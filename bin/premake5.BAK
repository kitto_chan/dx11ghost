﻿
require "setup"

--============================================================================
-- ソリューションファイル
--============================================================================
config_solution("Direct3D11")
    startproject "game"		-- スタートアッププロジェクト名

	-- プリプロセッサ #define
   	defines {
"_ITERATOR_DEBUG_LEVEL=0",
	}

	-- リンカー警告抑制
	linkoptions {
		"-IGNORE:4006",	-- warning LNK4006 : symbolはxxxx.libで定義されています。2 つ目以降の定義は無視されます。
	}
--============================================================================
-- 外部のプロジェクトファイル
--============================================================================
group "OpenSource"

-----------------------------------------------------------------
-- DirectXTex
-----------------------------------------------------------------
config_project("DirectXTex", "StaticLib")

	characterset      "UNICODE"		-- UNICODE文字セット

	local SOURCE_PATH = "../opensource/DirectXTex"

	-- 追加するソースコード
	-- *=フォルダ内 **=フォルダ内とその階層下サブフォルダ内
    	files {
		path.join(SOURCE_PATH, "DirectXTex/**.cpp"),
		path.join(SOURCE_PATH, "DirectXTex/**.h"),
		path.join(SOURCE_PATH, "DirectXTex/**.inl"),
	}
	
	-- "" インクルードパス
	includedirs {
		SOURCE_PATH,
	}

	-- プリプロセッサ #define
   	defines {
		"_WIN32_WINNT=0x0A00",						-- _WIN32_WINNT_WIN10 (0x0A00)
		"_CRT_STDIO_ARBITRARY_WIDE_SPECIFIERS",
	}

	-- フォルダ分け
	vpaths {
		["ヘッダー ファイル/*"] = {
			path.join(SOURCE_PATH, "**.h"),
			path.join(SOURCE_PATH, "**.hxx"),
			path.join(SOURCE_PATH, "**.hpp"),
			path.join(SOURCE_PATH, "**.inl")
		},
		["ソース ファイル/*"] = {
			path.join(SOURCE_PATH, "**.c"),
			path.join(SOURCE_PATH, "**.cxx"),
			path.join(SOURCE_PATH, "**.cpp")
		},
	}

-----------------------------------------------------------------
-- DirectXTK
-----------------------------------------------------------------
config_project("DirectXTK", "StaticLib")

	characterset      "UNICODE"		-- UNICODE文字セット

	local SOURCE_PATH = "../opensource/DirectXTK"

	-- 追加するソースコード
	-- *=フォルダ内 **=フォルダ内とその階層下サブフォルダ内
    	files {
		path.join(SOURCE_PATH, "Inc/**.cpp"),
		path.join(SOURCE_PATH, "Inc/**.h"),
		path.join(SOURCE_PATH, "Inc/**.inl"),
		path.join(SOURCE_PATH, "Src/**.cpp"),
		path.join(SOURCE_PATH, "Src/**.h"),
		path.join(SOURCE_PATH, "Src/**.inl"),
	}

	-- 除去するファイル
	removefiles {
		path.join(SOURCE_PATH, "Inc/XboxDDSTextureLoader.h"),
		path.join(SOURCE_PATH, "Src/XboxDDSTextureLoader.cpp"),
	}

	-- "" インクルードパス
	includedirs {
		path.join(SOURCE_PATH, "Inc"),
		path.join(SOURCE_PATH, "Src"),
	}

	-- プリプロセッサ #define
    	defines {
		"_WIN32_WINNT=0x0A00",						-- _WIN32_WINNT_WIN10 (0x0A00)
		"_CRT_STDIO_ARBITRARY_WIDE_SPECIFIERS",
	}

	-- プリコンパイル済ヘッダー
	pchheader( "pch.h" )
	pchsource( path.join(SOURCE_PATH, "Src/pch.cpp") )
	forceincludes ( "pch.h" )

-----------------------------------------------------------------
-- hlslpp
-----------------------------------------------------------------
config_project("hlsl++", "StaticLib")

	local SOURCE_PATH = "../opensource/hlslpp"

	files
	{
		path.join(SOURCE_PATH, "include/**.h"),
		path.join(SOURCE_PATH, "include/*.natvis"),
	}

-----------------------------------------------------------------
-- imgui
-----------------------------------------------------------------
config_project("imgui", "StaticLib")

	characterset      "UNICODE"		-- UNICODE文字セット
   	warnings "Default"

	local SOURCE_PATH = "../opensource/imgui"

	-- 追加するソースコード
	-- *=フォルダ内 **=フォルダ内とその階層下サブフォルダ内
    	files {
		path.join(SOURCE_PATH, "*.cpp"),
		path.join(SOURCE_PATH, "*.h"),
		path.join(SOURCE_PATH, "*.inl"),

		path.join(SOURCE_PATH, "backends/imgui_impl_dx11.*"),
		path.join(SOURCE_PATH, "backends/imgui_impl_win32.*"),
	}
	
	-- "" インクルードパス
	includedirs {
		SOURCE_PATH,
	}

	-- 除去するファイル
	removefiles {
	}

	-- プリプロセッサ #define
   	defines {
		"_WIN32_WINNT=0x0A00",						-- _WIN32_WINNT_WIN10 (0x0A00)
	}

	-- フォルダ分け
	vpaths {
		["ヘッダー ファイル/*"] = {
			path.join(SOURCE_PATH, "**.h"),
			path.join(SOURCE_PATH, "**.hxx"),
			path.join(SOURCE_PATH, "**.hpp"),
			path.join(SOURCE_PATH, "**.inl")
		},
		["ソース ファイル/*"] = {
			path.join(SOURCE_PATH, "**.c"),
			path.join(SOURCE_PATH, "**.cxx"),
			path.join(SOURCE_PATH, "**.cpp")
		},
	}

-----------------------------------------------------------------
-- OpenFBX
-----------------------------------------------------------------
config_project("OpenFBX", "StaticLib")

	local SOURCE_PATH = "../opensource/OpenFBX/src"

   	warnings "Default"

	-- 追加するソースコード
	-- *=フォルダ内 **=フォルダ内とその階層下サブフォルダ内
    	files {
		path.join(SOURCE_PATH, "*.c"),
		path.join(SOURCE_PATH, "*.cpp"),
		path.join(SOURCE_PATH, "*.h"),
	}
	
	-- "" インクルードパス
	includedirs {
		SOURCE_PATH,
	}

	-- 除去するファイル
	removefiles {
	}

	-- プリプロセッサ #define
    	defines {
	}

	-- フォルダ分け
	vpaths {
		["ヘッダー ファイル/*"] = {
			path.join(SOURCE_PATH, "**.h"),
			path.join(SOURCE_PATH, "**.hxx"),
			path.join(SOURCE_PATH, "**.hpp"),
			path.join(SOURCE_PATH, "**.inl")
		},
		["ソース ファイル/*"] = {
			path.join(SOURCE_PATH, "**.c"),
			path.join(SOURCE_PATH, "**.cxx"),
			path.join(SOURCE_PATH, "**.cpp")
		},
	}


-----------------------------------------------------------------
-- bullet
-----------------------------------------------------------------
config_project("bullet", "StaticLib")

	local SOURCE_PATH = "../opensource/bullet3/src"

   	warnings "Off"

	-- 追加するソースコード
	-- *=フォルダ内 **=フォルダ内とその階層下サブフォルダ内
    	files {
		path.join(SOURCE_PATH, "**.c"),
		path.join(SOURCE_PATH, "**.cpp"),
		path.join(SOURCE_PATH, "**.h"),
	}
	
	-- "" インクルードパス
	includedirs {
		SOURCE_PATH,
	}

	-- 除去するファイル
	removefiles {
	}

	-- プリプロセッサ #define
	defines {
		"BT_USE_SSE_IN_API",	-- [bullet] SSE命令の使用
		"BT_THREADSAFE",		-- [bullet] マルチスレッド時スレッドセーフ
	}

	-- フォルダ分け
	vpaths {
		["ヘッダー ファイル/*"] = {
			path.join(SOURCE_PATH, "**.h"),
			path.join(SOURCE_PATH, "*.hxx"),
			path.join(SOURCE_PATH, "**.hpp"),
			path.join(SOURCE_PATH, "**.inl")
		},
		["ソース ファイル/*"] = {
			path.join(SOURCE_PATH, "**.c"),
			path.join(SOURCE_PATH, "**.cxx"),
			path.join(SOURCE_PATH, "**.cpp")
		},
	}

-----------------------------------------------------------------
-- meshoptimizer
-----------------------------------------------------------------
config_project("meshoptimizer", "StaticLib")

	local SOURCE_PATH = "../opensource/meshoptimizer/src"

   	warnings "Off"

	-- 追加するソースコード
	-- *=フォルダ内 **=フォルダ内とその階層下サブフォルダ内
    	files {
		path.join(SOURCE_PATH, "**.c"),
		path.join(SOURCE_PATH, "**.cpp"),
		path.join(SOURCE_PATH, "**.h"),
	}
	
	-- "" インクルードパス
	includedirs {
		SOURCE_PATH,
	}

	-- 除去するファイル
	removefiles {
	}

	-- プリプロセッサ #define
	defines {
	}

	-- フォルダ分け
	vpaths {
		["ヘッダー ファイル/*"] = {
			path.join(SOURCE_PATH, "**.h"),
			path.join(SOURCE_PATH, "*.hxx"),
			path.join(SOURCE_PATH, "**.hpp"),
			path.join(SOURCE_PATH, "**.inl")
		},
		["ソース ファイル/*"] = {
			path.join(SOURCE_PATH, "**.c"),
			path.join(SOURCE_PATH, "**.cxx"),
			path.join(SOURCE_PATH, "**.cpp")
		},
	}

--============================================================================
-- プロジェクトファイル
--============================================================================
group ""
config_project("_framework_", "StaticLib")

	local SOURCE_PATH = "../source/framework"

	-- 追加するソースコード
	-- *=フォルダ内 **=フォルダ内とその階層下サブフォルダ内
    	files {
		path.join(SOURCE_PATH, "**.h"),
		path.join(SOURCE_PATH, "**.inl"),
		path.join(SOURCE_PATH, "**.cpp"),
		path.join("../resource", "**.fx"),	-- シェーダー
	}
	
	-- "" インクルードパス
	includedirs {
		SOURCE_PATH,
		"../opensource",		-- オープンソース
		"../opensource/bullet3/src"	-- Bullet物理シミュレーション
	}

	-- 依存ライブラリ・プロジェクト
	links {
		"d3dcompiler",
		"d3d11",
		"dxgi",
	--	"dxguid",
	}

	-- プリプロセッサ #define
   	defines {
	--	"_CRT_SECURE_NO_WARNINGS",
		"BT_USE_SSE_IN_API",	-- [bullet] SSE命令の使用
		"BT_THREADSAFE",		-- [bullet] マルチスレッド時スレッドセーフ
	}

	-- プリコンパイル済ヘッダー
	pchheader "precompile.h"
	pchsource (path.join(SOURCE_PATH, "precompile.cpp"))
	forceincludes "precompile.h"

	-- フォルダ分け
	vpaths {
		["ヘッダー ファイル/*"] = {
			path.join(SOURCE_PATH, "**.h"),
			path.join(SOURCE_PATH, "**.hxx"),
			path.join(SOURCE_PATH, "**.hpp"),
			path.join(SOURCE_PATH, "**.inl")
		},
		["ソース ファイル/*"] = {
			path.join(SOURCE_PATH, "**.c"),
			path.join(SOURCE_PATH, "**.cxx"),
			path.join(SOURCE_PATH, "**.cpp")
		},
	}

--============================================================================
-- プロジェクトファイル
--============================================================================
group ""
config_project("game", "WindowedApp")

	local SOURCE_PATH    = "../source/app"
	local FRAMEWORK_PATH = "../source/framework"

	debugdir  "../resource"			-- 実行開始時のカレントディレクトリ

	-- 追加するソースコード
	-- *=フォルダ内 **=フォルダ内とその階層下サブフォルダ内
    	files {
		path.join(SOURCE_PATH, "**.h"),
		path.join(SOURCE_PATH, "**.inl"),
		path.join(SOURCE_PATH, "**.cpp"),
	}
	
	-- "" インクルードパス
	includedirs {
		SOURCE_PATH,
		FRAMEWORK_PATH,
		"../opensource",						-- オープンソース
		"../opensource/bullet3/src"				-- Bullet物理シミュレーション
	}

	-- ライブラリディレクトリ
	libdirs {
	}

	-- 依存ライブラリ・プロジェクト
	links {
	}

	-- プリプロセッサ #define
   	defines {
	--	"_CRT_SECURE_NO_WARNINGS",
		"BT_USE_SSE_IN_API",	-- [bullet] SSE命令の使用
		"BT_THREADSAFE",		-- [bullet] マルチスレッド時スレッドセーフ
	}

	-- プリコンパイル済ヘッダー
	pchheader "precompile.h"
	pchsource (path.join(SOURCE_PATH, "precompile.cpp"))
	forceincludes "precompile.h"

	-- フォルダ分け
	vpaths {
		["ヘッダー ファイル/*"] = {
			path.join(SOURCE_PATH, "**.h"),
			path.join(SOURCE_PATH, "**.hxx"),
			path.join(SOURCE_PATH, "**.hpp"),
			path.join(SOURCE_PATH, "**.inl")
		},
		["ソース ファイル/*"] = {
			path.join(SOURCE_PATH, "**.c"),
			path.join(SOURCE_PATH, "**.cxx"),
			path.join(SOURCE_PATH, "**.cpp")
		},
	}

	links {
		"DirectXTex",
		"DirectXTK",
		"imgui",
		"OpenFBX",
		"bullet",
		"meshoptimizer",
		"_framework_",
	}

