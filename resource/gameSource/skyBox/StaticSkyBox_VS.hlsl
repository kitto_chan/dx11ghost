//----------------------------------------------------------------------------
//!	@file	StaticSkoBox.hlsl
//!	@brief	静的スカイボックス頂点シェーダ
//----------------------------------------------------------------------------
#include "StaticSkyBox.hlsli"

VertexPosHL VS(VertexPos vIn)
{
    VertexPosHL vOut;
    
    float4 posH = mul(g_WorldViewProj, float4(vIn.PosL, 1.0f));
    vOut.PosH = posH.xyww;
    vOut.PosL = vIn.PosL;
    return vOut;
}
