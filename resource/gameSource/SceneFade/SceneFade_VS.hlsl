//----------------------------------------------------------------------------
//!	@file	SceneFade.hlsl
//!	@brief	シーンフェード頂点シェーダ
//----------------------------------------------------------------------------
#include "SceneFade.hlsli"

VertexPosHTex VS(VertexPosTex vIn)
{
	VertexPosHTex vOut;
	vOut.posH = mul(matWVP_, float4(vIn.posL, 1.0f));
	vOut.tex = vIn.tex;
	return vOut;
}
