//----------------------------------------------------------------------------
//!	@file	BasicPosCol.hlsli
//!	@brief	PosとColor専用シェーダーヘーダ
//----------------------------------------------------------------------------

// 定数バッファ
cbuffer WorldCB : register(b0)
{
    matrix g_World; // ワールド行列
};

cbuffer CameraCB : register(b1)
{
    matrix g_View; // ビュー行列
    matrix g_Proj; // 投影行列
};

struct VertexIn
{
    float3 posL : POSITION;
    float4 color : COLOR;
};

struct VertexOut
{
    float4 posH : SV_POSITION;
    float4 color : COLOR;
};