//----------------------------------------------------------------------------
//!	@file	BasicPosCol.hlsl
//!	@brief	PosとColor専用頂点シェーダ
//----------------------------------------------------------------------------
#include "BasicPosCol.hlsli"

VertexOut VS(VertexIn vIn){
	VertexOut vOut;
	
    vOut.posH = mul(g_World, float4(vIn.posL, 1.0f) );
    vOut.posH = mul(g_View, vOut.posH);
    vOut.posH = mul(g_Proj,vOut.posH);
	
	
	vOut.color = vIn.color;
	return vOut;
}
