GL-Otomanopee: multibyte Japanese font

(C)2007-2009 Das Ende der Wildnis (http://heiden.daynight.jp/)
(C)2008-2020 Gutenberg Labo (http://gutenberg.osdn.jp/), All rights reserved.

- GL-Otomanopee License
  These fonts are free softwares.
  Unlimited permission is granted to use, copy, and distribute it, with or without modification, either commercially and noncommercially.
  THESE FONTS ARE PROVIDED "AS IS" WITHOUT WARRANTY.

- About GL-Otomanopee
  GL-Otomanopee は、天外エリカ（現・eunice）作成が作成した日本語デザインかなフォント「オとマのペ」を本プロジェクトにて改刻・再構築したものです。
  本フォントのライセンスは上記提示の通りです。

- Contained Glyphs in GL-Otomanopee
  このフォントに含まれる文字は以下の通りです。

  半角アルファベット
    ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz
  半角数字
    0123456789
  半角約物
    !"#$%&'()*+,-./:;<=>?@[\]^_`{|}~¢£¥¦¬¯
  半角カナ
    ｡｢｣､･ｦｧｨｩｪｫｬｭｮｯｰｱｲｳｴｵｶｷｸｹｺｻｼｽｾｿﾀﾁﾂﾃﾄﾅﾆﾇﾈﾉﾊﾋﾌﾍﾎﾏﾐﾑﾒﾓﾔﾕﾖﾗﾘﾙﾚﾛﾜﾝﾞﾟ
  全角アルファベット
    ＡＢＣＤＥＦＧＨＩＪＫＬＭＮＯＰＱＲＳＴＵＶＷＸＹＺ
    ａｂｃｄｅｆｇｈｉｊｋｌｍｎｏｐｑｒｓｔｕｖｗｘｙｚ
  全角数字
    ０１２３４５６７８９
  全角約物
    ―‖‥…※！＂＃＄％＆＇（）＊＋，－．／：；＜＝＞？＠［＼］＾＿｀｛｜｝～｟｠￠￡￥￤￢￣
    、。〃〄々〆〇〈〉《》「」『』【】〒〓〔〕〖〗〘〙〚〛〜〝〞〟〠〰〱〲〳〴〵〶〻〼〽・﹅﹆
  ひらがな/カタカナ
    ぁあぃいぅうぇえぉおかがきぎくぐけげこごさざしじすずせぜそぞただちぢつっづてでとどなにぬねのはばぱひびぴふぶぷへべぺほぼぽまみむめもゃやゅゆょよらりるれろゎわゐゑをんゔゕゖゟ゛゜ゝゞ
    ァアィイゥウェエォオカガキギクグケゲコゴサザシジスズセゼソゾタダチヂッツヅテデトドナニヌネノハバパヒビピフブプヘベペホボポマミムメモャヤュユョヨラリルレロヮワヰヱヲンヴヵヶヷヸヹヺヿーヽヾㇰㇱㇲㇳㇴㇵㇶㇷㇸㇹㇺㇻㇼㇽㇾㇿ

- Successor Font
  なお、GitHubよりGoogle Fonts提供版として、Adobe Western 2文字集合と教育漢字240文字を拡充した後継フォント「Otomanopee」を配布します。
  この後継フォント「Otomanopee」はライセンスが SIL Open Font Lisence 1.1 へ変更となりますのでご注意ください。
  https://github.com/Gutenberg-Labo/Otomanopee
  このフォントの当サイトでの更新は終了し、今後の更新はGitHub上の「Otomanopee」にて行います。

--------
オとマのペ
(C)2007-2009 Das Ende der Wildnis (http://heiden.daynight.jp/), All rights reserved.

- オとマのペ license（2009/08/21 Reversion）
  These fonts are free softwares.
  Unlimited permission is granted to use, copy, and distribute it, with or without modification, either commercially and noncommercially.
  THESE FONTS ARE PROVIDED "AS IS" WITHOUT WARRANTY.
