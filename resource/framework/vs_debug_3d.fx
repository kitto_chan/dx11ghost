//----------------------------------------------------------------------------
//!	@file	vs_debug_3d.fx
//!	@brief	3D頂点シェーダー
//----------------------------------------------------------------------------

// 定数バッファ
cbuffer CameraCB : register(b0)
{
	matrix	matView_;   // ビュー行列
	matrix	matProj_;   // 投影行列
};

// 頂点シェーダー入力
struct VS_INPUT
{
	float4	position_ : POSITION;
	float4	color_    : COLOR;
};

// 頂点シェーダー出力
struct VS_OUTPUT
{
	float4	position_ : SV_Position;
	float4	color_    : COLOR;
};

//----------------------------------------------------------------------------
// 頂点シェーダー
//----------------------------------------------------------------------------
VS_OUTPUT main(VS_INPUT input)
{
	VS_OUTPUT output = (VS_OUTPUT)0;

	output.position_ = input.position_;

	// 座標変換(ワールド座標を入力するためワールド行列は適用しない)
	output.position_ = mul(matView_, output.position_);
	output.position_ = mul(matProj_, output.position_);

	// パラメーター
	output.color_ = input.color_;

	return output;
}
