//----------------------------------------------------------------------------
//!	@file	vs_3d.fx
//!	@brief	3D頂点シェーダー
//----------------------------------------------------------------------------

// 定数バッファ
cbuffer WorldCB : register(b0)
{
	matrix	matWorld_;  // ワールド行列
};

cbuffer CameraCB : register(b1)
{
	matrix	matView_;   // ビュー行列
	matrix	matProj_;   // 投影行列
};

// 頂点シェーダー入力
struct VS_INPUT
{
	float4	position_ : POSITION;
	float2	uv_       : TEXCOORD;
};

// 頂点シェーダー出力
struct VS_OUTPUT
{
	float4	position_ : SV_Position;
	float4	color_    : COLOR;
	float2	uv_		  : TEXCOORD0;
};

//----------------------------------------------------------------------------
// 頂点シェーダー
//----------------------------------------------------------------------------
VS_OUTPUT main(VS_INPUT input)
{
	VS_OUTPUT output = (VS_OUTPUT)0;

	output.position_ = input.position_;

	// 座標変換
	output.position_ = mul(matWorld_, output.position_);
	output.position_ = mul(matView_, output.position_);
	output.position_ = mul(matProj_, output.position_);

	// パラメーター
	output.color_ = float4(1,1,1,1);
	output.uv_    = input.uv_;

	return output;
}
