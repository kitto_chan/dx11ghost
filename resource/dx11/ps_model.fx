//----------------------------------------------------------------------------
//!	@file	ps_model.fx
//!	@brief	モデル用ピクセルシェーダー
//----------------------------------------------------------------------------

Texture2D		Texture : register(t0);
SamplerState	Sampler : register(s0);

// 頂点シェーダー出力
struct VS_OUTPUT
{
	float4	position_ : SV_Position;
	float4	color_    : COLOR;
	float2	uv_		  : TEXCOORD0;
};

// ピクセルシェーダー出力
struct PS_OUTPUT
{
	float4	color0_ : SV_Target0;
};

cbuffer MeshColorCB : register(b0)
{
	float4	meshColor_ = float4(1, 1, 1, 1);	// メッシュの色
};

//----------------------------------------------------------------------------
// ピクセルシェーダー
//----------------------------------------------------------------------------
PS_OUTPUT main(VS_OUTPUT input)
{
	PS_OUTPUT	output = (PS_OUTPUT)0;

	output.color0_ = Texture.Sample(Sampler, input.uv_);
	
	// 透明度85%以下はピクセル棄却(くりぬき)
	if( output.color0_.a < 0.85) discard;
	
	output.color0_ *= input.color_ * meshColor_;

	//float gray = ( output.color0_.r + output.color0_.g + output.color0_.b ) / 3.0f;

	//output.color0_.rgb = gray;

	return output;
}
