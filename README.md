# Mobius Engine（メビウスエンジン）

## 概要
シェーダとゲーム開発を勉強ために書き始めたソースですが、今は就職作品として開発しています。  

ゲームエンジンのようにゲーム開発できると目指してます。  
ゲームエンジンとまだいろいろな不足ですが、いろんな機能追加して改善したいと思います。

DirectX11とC++17で書かれたソースです。  
Visual Studio 2019とC++17で作っています。  
プロジェクトはGitとPremake5で管理しています。  

GitHubでもアップロードしているけど、進行中のソースはBitbucketでメインで管理します。

## ビルド
`open.bat`を実行するとプロジェクトを生成します。自動的にVisual Studio 2019で開きます。（お勧め）

もしくは

`make.bat`を実行するとプロジェクトを生成します。

## 開発概要
2年生から(2021/05~)開発を始めました。  
先生のライブラリと自作フレームワークで構成しています。  
First Commitだけは先生から配布されたソースを流用しています。  
上記以外は自作したものです。

Unityのようなオブジェクト指向、コンポーネント指向を目指して設計開発しました。

主にSmart Pointerで管理してます

## 現在の機能
- ECS(Entity Component System)で構成するクラス
- ゲームシーン、ヒエラルキー、インスペクターなどのウィンドウでオブジェクトが操作できます
- オブジェクトは親子関係というコンセプトが使われています.（Transform Component）
- ゲームカメラ（第一人称、第三人称）
- 文字描画
- Fog　シェーダ
- Lighting (Direction Light / Point Light / Spot Light)
- スカイボックス

## Editor 画面
![Ghost2](https://user-images.githubusercontent.com/46988847/126074468-dc94e6e2-c06c-4d79-8080-cfdfa2b3ccb2.png)

## デモ動画
https://youtu.be/cWYIvkPmoeo?t=26


## 使ったライブラリー
 - DirectXTK       (いろんな便利の機能)
 - DirectXTex      (テクスチャー)
 - Bullet Physics3 (物理演算のため)
 - Imgui           (GUIのために)
 - Imguizmo        (GUIのために)
 - IconFonts       (Guiのために)   
 - OpenFBX         (Fbxファイルを読み込む)
 - Git             (ソース管理)
 - Premake5        (ソースコードから構成を自動的に構築するため)
 - hlslpp          (C++でシェーダと共通のシンタックス)
### 実装予定
 - cereal          (シリアライザ)
 - assimp          (モデル読み込む)

## 現在開発中
 - Serializer      (シーンの読み込む)
 - Assert Messager (アセットを管理)

## 開発/勉強予定
 - Motion Blur
 - Bloom
 - PBR
 - Particle
 - Toon Shader
 - Shadow

## 名前の由来
作品制作テーマである”メビウスの輪”から発想した際、その不思議な形状と特性はゲームエンジンと一緒ではないかと思いました。  
メビウスの輪の表をなぞっていくと裏につながったり、裏をなぞっていくと表になったり、どこ行っても結局は原点に戻って始めます。  
ゲーム制作のように1部ゲーム作るとまだこのエンジンの最初を戻って2部、3部... ∞部ゲームが作られます.無限の繰り返しの象徴でメビウスエンジンという名前をつけました。