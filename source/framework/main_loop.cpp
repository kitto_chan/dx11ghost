﻿//---------------------------------------------------------------------------
//!	@file	main_loop.cpp
//!	@brief	メインループ
//---------------------------------------------------------------------------
#include "graphics/graphics_render.h"
#include "graphics/graphics_imgui.h"

#include "imgui/imgui.h"

//namespace {
//HWND hwnd_          = nullptr;   //!< ウィンドウハンドル
//u32  mouseDragging_ = 0;         //!< ウィンドウ上でマウスをドラッグしているかどうか　bit0:左ボタン bit1:右ボタン bit2:中央ボタン
//bool isFullScreen_  = false;     //!< フルスクリーンモードかどうか
//bool isActive_      = false;     //!< アクティブかどうか
//RECT windowedRect_  = {};        //!< ウィンドウモード時のウィンドウ位置
//}   // namespace
//
////---------------------------------------------------------------------------
////! フルスクリーンとウィンドウモードの切り替え
////---------------------------------------------------------------------------
//bool toggleFullScreen()
//{
//    isFullScreen_ = !isFullScreen_;
//    auto hwnd     = application::hwnd();
//
//    constexpr LONG toggleStyle = WS_CAPTION | WS_SYSMENU | WS_THICKFRAME | WS_MINIMIZEBOX | WS_MAXIMIZEBOX;
//
//    auto style = GetWindowLong(hwnd, GWL_STYLE);
//    SetWindowLong(hwnd, GWL_STYLE, style ^ toggleStyle);
//
//    HWND windowOrder = HWND_NOTOPMOST;
//    s32  x, y, w, h;
//    if(isFullScreen_) {
//        //------------------------------------------------------
//        // フルスクリーンモード
//        //------------------------------------------------------
//        x = 0;
//        y = 0;
//        w = GetSystemMetrics(SM_CXSCREEN);   // 幅　 - 画面解像度
//        h = GetSystemMetrics(SM_CYSCREEN);   // 高さ - 画面解像度
//
//        windowOrder = HWND_TOP;   // 最前面
//
//        //------------------------------------------------------
//        // マルチモニターのフルスクリーン対応
//        // 利便性の向上。現在のウィンドウ位置のモニターでフルスクリーン化
//        //------------------------------------------------------
//        constexpr bool supportForMultiMonitor = true;
//
//        if constexpr(supportForMultiMonitor) {
//            // 現在のモニターを列挙
//            std::vector<RECT> monitorRects;
//
//            // ※Windowsコールバックにラムダ式を渡す場合はキャプチャ指定不可
//            MONITORENUMPROC MyInfoEnumProc = [](HMONITOR monitor, [[maybe_unused]] HDC hdcMonitor, RECT* rectMonitor, LPARAM args) -> BOOL {
//                MONITORINFOEX monitorInfo{};
//                monitorInfo.cbSize = sizeof(monitorInfo);
//                GetMonitorInfo(monitor, &monitorInfo);
//
//                auto* monitorRects = reinterpret_cast<std::vector<RECT>*>(args);
//
//                if(monitorInfo.dwFlags == DISPLAY_DEVICE_MIRRORING_DRIVER) {
//                    return true;
//                }
//                else {
//                    monitorRects->push_back(*rectMonitor);
//                };
//                return true;
//            };
//
//            // モニター列挙
//            EnumDisplayMonitors(nullptr, nullptr, MyInfoEnumProc, reinterpret_cast<LPARAM>(&monitorRects));
//
//            // 元に戻すときのために現在のウィンドウの位置とサイズを保存
//            GetWindowRect(hwnd, &windowedRect_);
//
//            // 中心座標を計算
//            s32 centerX = (windowedRect_.left + windowedRect_.right) >> 1;
//            s32 centerY = (windowedRect_.bottom + windowedRect_.top) >> 1;
//
//            // 現在のウィンドウ中心位置がどのモニター上にあるか判定してフルスクリーン化するモニターを選択
//            auto rect = std::find_if(monitorRects.begin(),
//                                     monitorRects.end(),
//                                     [&](RECT& r) { return r.left <= centerX && centerX < r.right &&
//                                                           r.top <= centerY && centerY < r.bottom; });
//            if(rect != monitorRects.end()) {
//                x = rect->left;
//                y = rect->top;
//                w = rect->right - rect->left;
//                h = rect->bottom - rect->top;
//            }
//        }
//    }
//    else {
//        //----------------------------------------------
//        // ウィンドウモード
//        //----------------------------------------------
//        // 元に戻す
//        x = windowedRect_.left;
//        y = windowedRect_.top;
//        w = windowedRect_.right - windowedRect_.left;
//        h = windowedRect_.bottom - windowedRect_.top;
//    }
//    // ウィンドウ位置と優先度を更新
//    SetWindowPos(hwnd,                                    // [in] ウィンドウハンドル
//                 windowOrder,                             // [in] ウィンドウの表示優先順位
//                 x, y,                                    // [in] 位置
//                 w, h,                                    // [in] 幅高さ
//                 SWP_FRAMECHANGED | SWP_NOOWNERZORDER);   // [in] 変更対象の選択フラグ (ウィンドウスタイルの変更)
//
//    return true;
//}
//
////---------------------------------------------------------------------------
////! ウィンドウプロシージャ
////!	@param	[in]	hwnd	ウィンドウハンドル
////!	@param	[in]	message	ウィンドウメッセージ
////!	@param	[in]	wparam	パラメーター1
////!	@param	[in]	lparam	パラメーター2
////!	@return ウィンドウプロシージャのそれぞれのメッセージ処理後の値
////---------------------------------------------------------------------------
//LRESULT CALLBACK windowProc(HWND hwnd, UINT message, WPARAM wparam, LPARAM lparam)
//{
//    //----------------------------------------------------------
//    // ImGuiのウィンドウプロシージャ処理
//    //----------------------------------------------------------
//    if(graphics::imgui::windowProc(hwnd, message, wparam, lparam)) {
//        return 0;
//    }
//    //----------------------------------------------------------
//    // ウィンドウメッセージ処理
//    //----------------------------------------------------------
//    switch(message) {
//        case WM_ACTIVATEAPP:
//            {
//                isActive_ = !!wparam;
//            }
//            break;
//        case WM_CREATE:   // ウィンドウ生成時(コンストラクタ相当)
//            {
//                // ここでGWLP_USERDATAにthisポインタを保存
//                auto* cs = reinterpret_cast<CREATESTRUCT*>(lparam);
//                SetWindowLongPtr(hwnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(cs->lpCreateParams));
//            }
//            return 0;
//
//        case WM_SYSKEYDOWN:
//            {
//                auto vk = wparam;          // 現在押されている仮想キーコード
//                if(lparam & (1 << 29)) {   // Altキーが押されているかどうか
//                    // Alt+Enter
//                    if(vk == VK_RETURN) {
//                        // フルスクリーンモードを切り替え
//                        toggleFullScreen();
//                        return 0;
//                    }
//                }
//            }
//            break;
//
//        // マウス左ボタンを押した
//        case WM_LBUTTONDOWN:
//            if(!ImGui::IsWindowHovered(ImGuiFocusedFlags_AnyWindow)) {
//                mouseDragging_ |= 1 << 0;
//                SetCapture(hwnd);
//            }
//            break;
//        // マウス右ボタンを押した
//        case WM_RBUTTONDOWN:
//            if(!ImGui::IsWindowHovered(ImGuiFocusedFlags_AnyWindow)) {
//                mouseDragging_ |= 1 << 1;
//                SetCapture(hwnd);
//            }
//            break;
//        // マウス中央ボタンを押した
//        case WM_MBUTTONDOWN:
//            if(!ImGui::IsWindowHovered(ImGuiFocusedFlags_AnyWindow)) {
//                mouseDragging_ |= 1 << 2;
//                SetCapture(hwnd);
//            }
//            break;
//        // マウス左ボタンを離した
//        case WM_LBUTTONUP:
//            mouseDragging_ &= ~(1 << 0);
//            if(mouseDragging_ == 0) {
//                ReleaseCapture();
//            }
//            break;
//        // マウス右ボタンを離した
//        case WM_RBUTTONUP:
//            mouseDragging_ &= ~(1 << 1);
//            if(mouseDragging_ == 0) {
//                ReleaseCapture();
//            }
//            break;
//        // マウス中央ボタンを離した
//        case WM_MBUTTONUP:
//            mouseDragging_ &= ~(1 << 2);
//            if(mouseDragging_ == 0) {
//                ReleaseCapture();
//            }
//            break;
//        // ウィンドウサイズ変更
//        case WM_SIZE:
//            {
//                auto width  = static_cast<u32>(lparam) & 0xffff;           // 幅
//                auto height = (static_cast<u32>(lparam) >> 16) & 0xffff;   // 高さ
//                dx11::swapChain()->resizeBuffers(width, height);           // バックバッファの解像度変更
//            }
//            break;
//        // ウィンドウ破棄時(デストラクタ相当)
//        case WM_DESTROY:
//            PostQuitMessage(0);
//            return 0;
//    }
//
//    //-------------------------------------------------------------
//    // デフォルトのウィンドウプロシージャ
//    // ※注意※ UNICODE版CreateWindowWを利用する場合はこの関数もUNICODE版にする必要あり
//    //-------------------------------------------------------------
//    return DefWindowProcW(hwnd, message, wparam, lparam);
//}
//
////---------------------------------------------------------------------------
//// メイン処理
////---------------------------------------------------------------------------
//int run(const std::wstring& titleName, u32 width, u32 height, bool isWindowed)
//{
//    // 高DPIモード対応
//    // ウィンドウUIのみDPIスケーリングされ、クライアント領域はdot-by-dot
//    SetProcessDpiAwarenessContext(DPI_AWARENESS_CONTEXT_PER_MONITOR_AWARE_V2);
//
//    // COMライブラリを初期化
//    CoInitializeEx(nullptr, COINIT_MULTITHREADED);
//
//    //----------------------------------------------------------
//    // ウィンドウクラスの登録
//    //----------------------------------------------------------
//    WNDCLASSEXW windowClass{};
//    windowClass.cbSize        = sizeof(windowClass);           // 構造体サイズ
//    windowClass.style         = CS_HREDRAW | CS_VREDRAW;       // クラススタイル
//    windowClass.lpfnWndProc   = &windowProc;                   // ウィンドウプロシージャ
//    windowClass.hInstance     = application::instance();       // アプリケーションインスタンスハンドル
//    windowClass.hCursor       = LoadCursor(NULL, IDC_CROSS);   // カーソルの種類
//    windowClass.lpszClassName = L"DX11SampleClass";            // クラス名
//
//    RegisterClassExW(&windowClass);
//
//    //----------------------------------------------------------
//    // ウィンドウを作成
//    //----------------------------------------------------------
//    u32 style   = WS_OVERLAPPEDWINDOW;   // ウィンドウスタイル
//    u32 styleEx = 0;                     // 拡張ウィンドウスタイル
//    s32 x       = CW_USEDEFAULT;
//    s32 y       = CW_USEDEFAULT;
//    s32 w       = CW_USEDEFAULT;
//    s32 h       = CW_USEDEFAULT;
//
//    // ウィンドウサイズをUIサイズを考慮して補正
//    {
//        RECT windowRect{ 0, 0, static_cast<LONG>(width), static_cast<LONG>(height) };
//
//        AdjustWindowRectEx(&windowRect, style, false, styleEx);
//        w = windowRect.right - windowRect.left;
//        h = windowRect.bottom - windowRect.top;
//    }
//
//    // ウィンドウを作成
//    hwnd_ = CreateWindowExW(
//        styleEx,                     // 拡張ウィンドウスタイル
//        windowClass.lpszClassName,   // ウィンドウクラス名
//        titleName.c_str(),           // タイトル名
//        style,                       // ウィンドウスタイル
//        x,                           // X座標
//        y,                           // Y座標
//        w,                           // ウィンドウの幅
//        h,                           // ウィンドウの高さ
//        nullptr,                     // 親ウィンドウ(なし)
//        nullptr,                     // メニューハンドル(なし)
//        application::instance(),     // アプリケーションインスタンスハンドル
//        nullptr);                    // WM_CREATEへの引数(任意)
//
//    //----------------------------------------------------------
//    // 初期化
//    //----------------------------------------------------------
//    if(!graphics::render()->initialize(width, height)) {
//        MessageBox(hwnd_, "起動に失敗しました.", "初期化エラー", MB_OK);
//        return 1;
//    }
//
//    // 初期状態がフルスクリーンモードの場合
//    if(!isWindowed) {
//        toggleFullScreen();
//    }
//
//    // ウィンドウを表示
//    {
//        // GetWindowPlacementからWinMainの引数のnCmdShowを取得可能。
//        WINDOWPLACEMENT placement;
//        GetWindowPlacement(application::hwnd(), &placement);
//        ShowWindow(application::hwnd(), static_cast<int>(placement.showCmd));
//    }
//
//    //=============================================================
//    // メインメッセージループ
//    //=============================================================
//    MSG msg{};
//
//    while(msg.message != WM_QUIT) {
//        if(PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE)) {
//            TranslateMessage(&msg);
//            DispatchMessage(&msg);
//        }
//        else {
//            graphics::render()->update();   // 更新
//            graphics::render()->render();   // 描画
//        }
//    }
//
//    //----------------------------------------------------------
//    // 解放
//    //----------------------------------------------------------
//    graphics::render()->finalize();
//
//    // COMライブラリの解放
//    CoUninitialize();
//
//    // WM_QUITメッセージの戻り値を終了コードとして返す
//    return static_cast<int>(msg.wParam);
//}
//
//namespace application {
//
////---------------------------------------------------------------------------
////! ウィンドウハンドルを取得
////---------------------------------------------------------------------------
//HWND hwnd()
//{
//    return hwnd_;
//}
//
////---------------------------------------------------------------------------
////! アプリケーションインスタンスハンドルを取得
////---------------------------------------------------------------------------
//HINSTANCE instance()
//{
//    return GetModuleHandle(nullptr);
//}
//
////---------------------------------------------------------------------------
////! マウスフォーカスを持っているかどうか
////---------------------------------------------------------------------------
//bool hasMouseFocus()
//{
//    return mouseDragging_ != 0;
//}
//
////---------------------------------------------------------------------------
////! アクティブかどうか
////---------------------------------------------------------------------------
//bool isActive()
//{
//    return isActive_;
//}
//
//}   // namespace application
