﻿//---------------------------------------------------------------------------
//!	@file	dx11.cpp
//!	@brief	DirectX11
//---------------------------------------------------------------------------
#include "dx11.h"
#include <DirectXTex/DirectXTex/DirectXTex.h>

namespace dx11::callback {

std::function<bool(u32, u32)>                                                                 onInitialize;
std::function<void(f32)>                                                                      onUpdate;
std::function<void(raw_ptr<dx11::Texture> colorTexture, raw_ptr<dx11::Texture> depthTexture)> onRender;
std::function<void()>                                                                         onFinalize;

}   // namespace dx11::callback

namespace dx11 {

//---------------------------------------------------------------------------
//! プリミティブの種類をD3D11_PRIMITIVE_TOPOLOGYに変換
//---------------------------------------------------------------------------
D3D11_PRIMITIVE_TOPOLOGY makePrimitiveTopology(dx11::Primitive type)
{
    static constexpr D3D11_PRIMITIVE_TOPOLOGY d3d11PrimitiveTopology[] = {
        D3D11_PRIMITIVE_TOPOLOGY_UNDEFINED,           // 未定義
        D3D11_PRIMITIVE_TOPOLOGY_POINTLIST,           // ポイントリスト
        D3D11_PRIMITIVE_TOPOLOGY_LINELIST,            // ラインリスト
        D3D11_PRIMITIVE_TOPOLOGY_LINESTRIP,           // ラインSTRIP
        D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST,        // 三角形リスト
        D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP,       // 三角形STRIP
        D3D11_PRIMITIVE_TOPOLOGY_LINELIST_ADJ,        // 隣接データを持つラインリスト
        D3D11_PRIMITIVE_TOPOLOGY_LINESTRIP_ADJ,       // 隣接データを持つラインSTRIP
        D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST_ADJ,    // 隣接データを持つ三角形リスト
        D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP_ADJ,   // 隣接データを持つ三角形STRIP
    };
    return d3d11PrimitiveTopology[static_cast<u32>(type)];
}

//---------------------------------------------------------------------------
//! D3D11_USAGEからCPUアクセス権フラグ取得
//---------------------------------------------------------------------------
u32 makeD3DCpuAccessFlags(D3D11_USAGE usage)
{
    std::array<u32, 4> d3dCpuAcceccFlags{
        0,                                                // D3D11_USAGE_DEFAULT
        0,                                                // D3D11_USAGE_IMMUTABLE
        D3D11_CPU_ACCESS_WRITE,                           // D3D11_USAGE_DYNAMIC
        D3D11_CPU_ACCESS_READ | D3D11_CPU_ACCESS_WRITE,   // D3D11_USAGE_STAGING
    };

    return d3dCpuAcceccFlags[usage];
}

//---------------------------------------------------------------------------
//! カラーターゲットに適したフォーマットか判定
//---------------------------------------------------------------------------
bool isColorTargetFormat(DXGI_FORMAT dxgiFormat)
{
    if(u32 formatSupport = 0; SUCCEEDED(dx11::d3dDevice()->CheckFormatSupport(dxgiFormat, &formatSupport))) {
        return formatSupport & (D3D11_FORMAT_SUPPORT_RENDER_TARGET);
    }
    return false;
}

//---------------------------------------------------------------------------
//! デプスバッファに適したフォーマットか判定
//---------------------------------------------------------------------------
bool isDepthTargetFormat(DXGI_FORMAT dxgiFormat)
{
    if(u32 formatSupport = 0; SUCCEEDED(dx11::d3dDevice()->CheckFormatSupport(dxgiFormat, &formatSupport))) {
        return formatSupport & (D3D11_FORMAT_SUPPORT_DEPTH_STENCIL);
    }
    return false;
}

//---------------------------------------------------------------------------
//! ステンシルバッファフォーマットか判定
//---------------------------------------------------------------------------
bool isStencilFormat(DXGI_FORMAT dxgiFormat)
{
    return dxgiFormat == DXGI_FORMAT_D32_FLOAT_S8X24_UINT ||
           dxgiFormat == DXGI_FORMAT_D24_UNORM_S8_UINT;
}

//---------------------------------------------------------------------------
//! TYPELESS形式のフォーマットに変換
//---------------------------------------------------------------------------
DXGI_FORMAT makeTypelessFormat(DXGI_FORMAT dxgiFormat)
{
    switch(dxgiFormat) {
        case DXGI_FORMAT_R32G32B32A32_FLOAT:
        case DXGI_FORMAT_R32G32B32A32_UINT:
        case DXGI_FORMAT_R32G32B32A32_SINT:

            dxgiFormat = DXGI_FORMAT_R32G32B32A32_TYPELESS;
            break;

        case DXGI_FORMAT_R32G32B32_FLOAT:
        case DXGI_FORMAT_R32G32B32_UINT:
        case DXGI_FORMAT_R32G32B32_SINT:
            dxgiFormat = DXGI_FORMAT_R32G32B32_TYPELESS;
            break;

        case DXGI_FORMAT_R16G16B16A16_FLOAT:
        case DXGI_FORMAT_R16G16B16A16_UNORM:
        case DXGI_FORMAT_R16G16B16A16_UINT:
        case DXGI_FORMAT_R16G16B16A16_SNORM:
        case DXGI_FORMAT_R16G16B16A16_SINT:
            dxgiFormat = DXGI_FORMAT_R16G16B16A16_TYPELESS;
            break;

        case DXGI_FORMAT_R32G32_FLOAT:
        case DXGI_FORMAT_R32G32_UINT:
        case DXGI_FORMAT_R32G32_SINT:
            dxgiFormat = DXGI_FORMAT_R32G32_TYPELESS;
            break;

        case DXGI_FORMAT_D32_FLOAT_S8X24_UINT:
            dxgiFormat = DXGI_FORMAT_R32G8X24_TYPELESS;
            break;

        case DXGI_FORMAT_R10G10B10A2_UNORM:
        case DXGI_FORMAT_R10G10B10A2_UINT:
            dxgiFormat = DXGI_FORMAT_R10G10B10A2_TYPELESS;
            break;

        case DXGI_FORMAT_R8G8B8A8_UNORM:
        case DXGI_FORMAT_R8G8B8A8_UNORM_SRGB:
        case DXGI_FORMAT_R8G8B8A8_UINT:
        case DXGI_FORMAT_R8G8B8A8_SNORM:
        case DXGI_FORMAT_R8G8B8A8_SINT:
            dxgiFormat = DXGI_FORMAT_R8G8B8A8_TYPELESS;
            break;

        case DXGI_FORMAT_R16G16_FLOAT:
        case DXGI_FORMAT_R16G16_UNORM:
        case DXGI_FORMAT_R16G16_UINT:
        case DXGI_FORMAT_R16G16_SNORM:
        case DXGI_FORMAT_R16G16_SINT:
            dxgiFormat = DXGI_FORMAT_R16G16_TYPELESS;
            break;

        case DXGI_FORMAT_D32_FLOAT:
        case DXGI_FORMAT_R32_FLOAT:
        case DXGI_FORMAT_R32_UINT:
        case DXGI_FORMAT_R32_SINT:
            dxgiFormat = DXGI_FORMAT_R32_TYPELESS;
            break;

        case DXGI_FORMAT_D24_UNORM_S8_UINT:
            dxgiFormat = DXGI_FORMAT_R24G8_TYPELESS;
            break;

        case DXGI_FORMAT_R8G8_UNORM:
        case DXGI_FORMAT_R8G8_UINT:
        case DXGI_FORMAT_R8G8_SNORM:
        case DXGI_FORMAT_R8G8_SINT:
            dxgiFormat = DXGI_FORMAT_R8G8_TYPELESS;
            break;

        case DXGI_FORMAT_R16_FLOAT:
        case DXGI_FORMAT_D16_UNORM:
        case DXGI_FORMAT_R16_UNORM:
        case DXGI_FORMAT_R16_UINT:
        case DXGI_FORMAT_R16_SNORM:
        case DXGI_FORMAT_R16_SINT:
            dxgiFormat = DXGI_FORMAT_R16_TYPELESS;
            break;

        case DXGI_FORMAT_R8_UNORM:
        case DXGI_FORMAT_R8_UINT:
        case DXGI_FORMAT_R8_SNORM:
        case DXGI_FORMAT_R8_SINT:
            dxgiFormat = DXGI_FORMAT_R8_TYPELESS;
            break;

        case DXGI_FORMAT_BC1_UNORM:
        case DXGI_FORMAT_BC1_UNORM_SRGB:
            dxgiFormat = DXGI_FORMAT_BC1_TYPELESS;
            break;

        case DXGI_FORMAT_BC2_UNORM:
        case DXGI_FORMAT_BC2_UNORM_SRGB:
            dxgiFormat = DXGI_FORMAT_BC2_TYPELESS;
            break;

        case DXGI_FORMAT_BC3_UNORM:
        case DXGI_FORMAT_BC3_UNORM_SRGB:
            dxgiFormat = DXGI_FORMAT_BC3_TYPELESS;
            break;

        case DXGI_FORMAT_BC4_UNORM:
        case DXGI_FORMAT_BC4_SNORM:
            dxgiFormat = DXGI_FORMAT_BC4_TYPELESS;
            break;

        case DXGI_FORMAT_BC5_UNORM:
        case DXGI_FORMAT_BC5_SNORM:
            dxgiFormat = DXGI_FORMAT_BC5_TYPELESS;
            break;

        case DXGI_FORMAT_B8G8R8A8_UNORM:
        case DXGI_FORMAT_B8G8R8A8_UNORM_SRGB:
            dxgiFormat = DXGI_FORMAT_B8G8R8A8_TYPELESS;
            break;

        case DXGI_FORMAT_B8G8R8X8_UNORM:
        case DXGI_FORMAT_B8G8R8X8_UNORM_SRGB:
            dxgiFormat = DXGI_FORMAT_B8G8R8X8_TYPELESS;
            break;

        case DXGI_FORMAT_BC6H_UF16:
        case DXGI_FORMAT_BC6H_SF16:
            dxgiFormat = DXGI_FORMAT_BC6H_TYPELESS;
            break;

        case DXGI_FORMAT_BC7_UNORM:
        case DXGI_FORMAT_BC7_UNORM_SRGB:
            dxgiFormat = DXGI_FORMAT_BC7_TYPELESS;
            break;

        default:
            break;
    }
    return dxgiFormat;
}

//---------------------------------------------------------------------------
//! カラー形式のフォーマットに変換
//---------------------------------------------------------------------------
DXGI_FORMAT makeColorFormat(DXGI_FORMAT dxgiFormat)
{
    switch(dxgiFormat) {
        case DXGI_FORMAT_D32_FLOAT_S8X24_UINT:
            dxgiFormat = DXGI_FORMAT_R32_FLOAT_X8X24_TYPELESS;
            dxgiFormat = DXGI_FORMAT_X32_TYPELESS_G8X24_UINT;
            break;

        case DXGI_FORMAT_D32_FLOAT:
            dxgiFormat = DXGI_FORMAT_R32_FLOAT;
            break;

        case DXGI_FORMAT_D24_UNORM_S8_UINT:
            dxgiFormat = DXGI_FORMAT_R24_UNORM_X8_TYPELESS;
            dxgiFormat = DXGI_FORMAT_X24_TYPELESS_G8_UINT;
            break;

        case DXGI_FORMAT_D16_UNORM:
            dxgiFormat = DXGI_FORMAT_R16_UNORM;
            break;

        default:
            break;
    }
    return dxgiFormat;
}

//---------------------------------------------------------------------------
//! 1ピクセルあたりのサイズを取得
//---------------------------------------------------------------------------
size_t bpp(DXGI_FORMAT dxgiFormat)
{
    return DirectX::BitsPerPixel(dxgiFormat);
}

//---------------------------------------------------------------------------
//! 解像度からミップ段数を計算
//---------------------------------------------------------------------------
size_t countMips(size_t width, size_t height)
{
    size_t mipLevels = 1;

    while(height > 1 || width > 1) {
        if(height > 1)
            height >>= 1;

        if(width > 1)
            width >>= 1;

        ++mipLevels;
    }

    return mipLevels;
}

}   // namespace dx11
