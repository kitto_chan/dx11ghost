﻿//---------------------------------------------------------------------------
//!	@file   dx11_constant_buffer_pool.cpp
//!	@brief	定数バッファプール
//! 
//! シェーダー内で参照される定数バッファを名前引き出来るようにしたプール
//---------------------------------------------------------------------------
#include "dx11_constant_buffer_pool.h"

namespace dx11 {

namespace {

// 定数バッファプール
// バッファの名前でバッファを取得可能。弱参照ポインタで管理
std::unordered_map<std::string, std::weak_ptr<dx11::Buffer>> constantBufferPool_;

}   // namespace

//---------------------------------------------------------------------------
//! シェーダー内で利用する定数バッファを取得
//! @param  [in]    name    名前
//---------------------------------------------------------------------------
std::shared_ptr<dx11::Buffer> getShaderConstantBuffer(const char* name)
{
    // 検索
    auto result = constantBufferPool_.find(name);
    if(result != std::end(constantBufferPool_)) {
        std::weak_ptr<dx11::Buffer> wp = result->second;
        // weak_ptrの監視対象のshared_ptrが有効なリソースを保持している状態なら処理する
        if(std::shared_ptr<dx11::Buffer> cb = wp.lock()) {
            return cb;
        }
    }
    return nullptr;
}

//---------------------------------------------------------------------------
//! シェーダー内で利用する定数バッファを作成
//! @param  [in]    name    名前
//! @param  [in]    size    バッファサイズ
//---------------------------------------------------------------------------
std::shared_ptr<dx11::Buffer> createShaderConstantBuffer(const char* name, u32 size)
{
    // 検索
    std::weak_ptr<dx11::Buffer> wp;
    {
        auto result = constantBufferPool_.find(name);
        if(result != std::end(constantBufferPool_)) {
            wp = result->second;
        }
    }

    auto cb = getShaderConstantBuffer(name);
    if(!cb) {
        // 既存のバッファが無ければ新規作成
        cb = dx11::createBuffer({ size,                         // [in] サイズ(単位:bytes)
                                  D3D11_BIND_CONSTANT_BUFFER,   // [in] 設定可能スロット
                                  D3D11_USAGE_DYNAMIC });       // [in] メモリ配置場所
        if(!cb) {
            return false;
        }
        constantBufferPool_[name] = cb;   // リストに登録
    }
    else {
        // 安全のためサイズの比較チェック
        ASSERT_MESSAGE(cb->desc().size_ == size, "サイズが異なる同名定数バッファが検出されました.");
    }
    return cb;
}

}   // namespace dx11
