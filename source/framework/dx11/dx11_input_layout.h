﻿//---------------------------------------------------------------------------
//!	@file	dx11_input_layout.h
//!	@brief	入力レイアウト
//---------------------------------------------------------------------------
#pragma once

namespace dx11 {

//===========================================================================
// 入力レイアウト
//===========================================================================
class InputLayout
{
public:
    //! 入力要素配列を取得
    virtual const std::vector<D3D11_INPUT_ELEMENT_DESC>* d3dInputElements() const = 0;

    //! ハッシュ値を取得
    virtual u64 hash() const = 0;

    //! 頂点シェーダーに対応するD3D入力レイアウトを取得
    //! @param  [in]    shaderVs    頂点シェーダー
    virtual ID3D11InputLayout* d3dInputLayout(raw_ptr<dx11::Shader> shaderVs) const = 0;

    //! 頂点サイズを取得
    virtual u32 vertexStride() const = 0;

protected:
    virtual ~InputLayout() = default;
};

//! 入力レイアウトを作成
//! @param  [in]    elements        入力要素の配列先頭
//! @param  [in]    elementCount    要素数
[[nodiscard]] std::shared_ptr<dx11::InputLayout> createInputLayout(const D3D11_INPUT_ELEMENT_DESC* elements, size_t elementCount);

}   // namespace dx11