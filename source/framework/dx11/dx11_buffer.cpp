﻿//---------------------------------------------------------------------------
//!	@file   dx11_buffer.cpp
//!	@brief	GPUバッファ
//---------------------------------------------------------------------------
namespace dx11 {

//===========================================================================
//! GPUバッファ実装部
//===========================================================================
class BufferImpl final : public dx11::Buffer
{
public:
    //! コンストラクタ
    BufferImpl(ID3D11Buffer* d3dResource);

    //! デストラクタ
    virtual ~BufferImpl() = default;

    //! メモリマッピング開始
    //! GPUメモリをCPU側から参照できるようにします。
    //! @param  [in]    mapType マップの動作タイプ
    virtual void* map(D3D11_MAP mapType) override;

    //! メモリマッピング終了
    //! GPU実行時に対象のバッファがマップされているとストールします
    //!必ずバッファのマッピングを解除してから発行します。
    virtual void unmap() override;

    //----------------------------------------------------------
    //! @name キャスト
    //----------------------------------------------------------
    //@{

    //! ID3D11Buffer参照
    virtual operator ID3D11Buffer*() const override { return d3dBuffer_.Get(); }

    //@}
    //----------------------------------------------------------
    //! @name 参照
    //----------------------------------------------------------
    //@{

    //! バッファ情報を取得
    virtual const Buffer::Desc& desc() const override { return desc_; }

    //! D3Dリソースを取得
    virtual ID3D11Resource* d3dResource() const override { return d3dBuffer_.Get(); }

private:
    // コピー禁止/move禁止
    BufferImpl(const BufferImpl&) = delete;
    BufferImpl(BufferImpl&&)      = delete;
    BufferImpl& operator=(const BufferImpl&) = delete;
    BufferImpl& operator=(BufferImpl&&) = delete;

private:
    Buffer::Desc          desc_ = {};
    com_ptr<ID3D11Buffer> d3dBuffer_;   //!< D3Dバッファリソース (GPUメモリに相当)
};

//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
BufferImpl::BufferImpl(ID3D11Buffer* d3dBuffer)
{
    d3dBuffer_ = d3dBuffer;

    // D3Dバッファからパラメーターを取得
    D3D11_BUFFER_DESC bufferDesc;
    d3dBuffer->GetDesc(&bufferDesc);

    desc_.size_         = bufferDesc.ByteWidth;
    desc_.d3dBindFlags_ = bufferDesc.BindFlags;
    desc_.d3dUsage_     = bufferDesc.Usage;
}

//---------------------------------------------------------------------------
//! メモリマッピング開始
//---------------------------------------------------------------------------
void* BufferImpl::map(D3D11_MAP mapType)
{
    D3D11_MAPPED_SUBRESOURCE data;
    dx11::immediateContext()->Map(d3dBuffer_.Get(), 0, mapType, 0, &data);
    return data.pData;
}

//---------------------------------------------------------------------------
//! メモリマッピング終了
//---------------------------------------------------------------------------
void BufferImpl::unmap()
{
    dx11::immediateContext()->Unmap(d3dBuffer_.Get(), 0);
}

//---------------------------------------------------------------------------
//! バッファを作成
//---------------------------------------------------------------------------
std::shared_ptr<dx11::Buffer> createBuffer(const Buffer::Desc& desc, const void* initializeData)
{
    // オプション設定
    D3D11_BUFFER_DESC bufferDesc{};
    bufferDesc.ByteWidth           = static_cast<u32>(desc.size_);                  // バッファのサイズ
    bufferDesc.Usage               = desc.d3dUsage_;                                // 配置場所(メインメモリ or ビデオメモリ)
    bufferDesc.BindFlags           = desc.d3dBindFlags_;                            // 設定可能スロット
    bufferDesc.CPUAccessFlags      = dx11::makeD3DCpuAccessFlags(desc.d3dUsage_);   // CPUアクセス権
    bufferDesc.MiscFlags           = 0;                                             // フラグ
    bufferDesc.StructureByteStride = 0;                                             // 構造化バッファの構造体あたりのサイズ(単位:bytes)

    // バッファの初期データを渡して頂点バッファを作成
    D3D11_SUBRESOURCE_DATA data{};
    data.pSysMem = initializeData;   // 初期データの先頭アドレス

    com_ptr<ID3D11Buffer> d3dBuffer;
    if(FAILED(dx11::d3dDevice()->CreateBuffer(
           &bufferDesc,                        // [in]  オプション設定
           initializeData ? &data : nullptr,   // [in]  初期データ情報構造体のアドレス(nullptrの場合は初期値なし)
           &d3dBuffer))) {                     // [out] 作成されたバッファ
        return nullptr;
    }

    return createBuffer(d3dBuffer.Get());
}

//---------------------------------------------------------------------------
//! D3Dリソースから作成
//---------------------------------------------------------------------------
std::shared_ptr<dx11::Buffer> createBuffer(ID3D11Resource* d3dResource)
{
    // GPUリソースがバッファかどうか判定
    D3D11_RESOURCE_DIMENSION resourceType;
    d3dResource->GetType(&resourceType);

    if(resourceType != D3D11_RESOURCE_DIMENSION_BUFFER) {
        return nullptr;
    }

    //---------------------------------------------------------------------
    // バッファクラスの初期化
    //---------------------------------------------------------------------
    return std::make_shared<dx11::BufferImpl>(static_cast<ID3D11Buffer*>(d3dResource));
}

}   // namespace dx11
