﻿//---------------------------------------------------------------------------
//!	@file	dx11_swap_chain.cpp
//!	@brief	スワップチェイン
//---------------------------------------------------------------------------

namespace dx11 {

//===========================================================================
//! スワップチェイン実装部
//===========================================================================
class SwapChainImpl final : public dx11::SwapChain
{
public:
    //! コンストラクタ
    SwapChainImpl() = default;

    //! デストラクタ
    virtual ~SwapChainImpl() = default;

    //! 初期化
    bool initialize(u32 width, u32 height, DXGI_FORMAT dxgiFormat, u32 bufferCount);

    //! 画面更新
    virtual bool present(u32 vsyncInterval) override;

    //! GPUが実行完了になるまで待つ
    //! 完全にアイドル状態になるまでブロックします
    virtual void waitForGpu() override;

    //! バックバッファのサイズ変更
    virtual void resizeBuffers(u32 width, u32 height) override;

    //----------------------------------------------------------
    //! @name 参照
    //----------------------------------------------------------
    //@{

    //! DXGIスワップチェインを取得
    virtual IDXGISwapChain3* dxgiSwapChain() const override
    {
        return dxgiSwapChain_.Get();
    }

    //! バックバッファを取得
    virtual dx11::Texture* backBuffer() const override
    {
        return backBuffer_.get();
    }

    //@}
private:
    // コピー禁止/move禁止
    SwapChainImpl(const SwapChainImpl&) = delete;
    SwapChainImpl(SwapChainImpl&&)      = delete;
    SwapChainImpl& operator=(const SwapChainImpl&) = delete;
    SwapChainImpl& operator=(SwapChainImpl&&) = delete;

private:
    u32 width_       = 0;   //!< 幅
    u32 height_      = 0;   //!< 高さ
    u32 bufferCount_ = 0;   //!< バッファ数

    com_ptr<IDXGISwapChain3>       dxgiSwapChain_;   //!< スワップチェイン
    std::shared_ptr<dx11::Texture> backBuffer_;      //!< バックバッファ
};

//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool SwapChainImpl::initialize(u32 width, u32 height, DXGI_FORMAT dxgiFormat, u32 bufferCount)
{
    width_       = width;
    height_      = height;
    bufferCount_ = bufferCount;

    //----------------------------------------------------------
    //! スワップチェインの作成
    //----------------------------------------------------------
    {
        // 全般設定
        DXGI_SWAP_CHAIN_DESC1 desc{};
        desc.Width              = width_;                            // 幅
        desc.Height             = height_;                           // 高さ
        desc.Format             = dxgiFormat;                        // ピクセル形式
        desc.Stereo             = false;                             // 立体視出力
        desc.SampleDesc.Count   = 1;                                 // マルチサンプル数
        desc.SampleDesc.Quality = 0;                                 // マルチサンプル品質
        desc.BufferUsage        = DXGI_USAGE_RENDER_TARGET_OUTPUT;   // バッファ用途
        desc.BufferCount        = bufferCount;                       // バックバッファ数
        desc.Scaling            = DXGI_SCALING_STRETCH;              // スケーリングモード(拡縮)
        desc.SwapEffect         = DXGI_SWAP_EFFECT_FLIP_DISCARD;     // Swap処理タイプ
        desc.AlphaMode          = DXGI_ALPHA_MODE_UNSPECIFIED;       // アルファの種類
        desc.Flags              = 0;                                 // フラグ

        // フルスクリーン設定
        DXGI_SWAP_CHAIN_FULLSCREEN_DESC fullscreenDesc{};
        fullscreenDesc.RefreshRate.Numerator   = 0;                                      // リフレッシュレート分子
        fullscreenDesc.RefreshRate.Denominator = 0;                                      // リフレッシュレート分母
        fullscreenDesc.ScanlineOrdering        = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;   // スキャンライン方式
        fullscreenDesc.Scaling                 = DXGI_MODE_SCALING_UNSPECIFIED;          // スケーリングモード
        fullscreenDesc.Windowed                = true;                                   // ウィンドウモード true:ウィンドウモード false:フルスクリーンモード

        com_ptr<IDXGISwapChain1> swapChain;
        if(FAILED(dx11::dxgiFactory()->CreateSwapChainForHwnd(
               dx11::d3dDevice(),     // [in]  D3Dデバイス
               application::hwnd(),   // [in]  ウィンドウハンドル
               &desc,                 // [in]  オプション
               &fullscreenDesc,       // [in]  フルスクリーン表示オプション
               nullptr,               // [in]  出力先(コンテンツを出力ターゲットに制限しない場合はnullptr)
               &swapChain))) {        // [out] 生成されたスワップチェイン
            return false;
        }
        // OS制御のAlt+Enterのウィンドウモード←→フルスクリーンモード遷移を禁止
        // 現在は従来の排他モード(exclusive mode)と言われるフルスクリーンモードは非推奨になっているため。
        // [注意点] CreateDeviceの引数のadapterにも同じDXGIFactoryから生成したアダプターの指定が必要。
        //         同じFactoryから生成されていない場合にこの設定は正常に機能しないため注意が必要。
        if(FAILED(dx11::dxgiFactory()->MakeWindowAssociation(application::hwnd(), DXGI_MWA_NO_ALT_ENTER))) {
            return false;
        }

        if(FAILED(swapChain.As(&dxgiSwapChain_))) {
            return false;
        }
    }

    //----------------------------------------------------------
    // バックバッファテクスチャの作成
    //----------------------------------------------------------
    // スワップチェインからバックバッファのResourceを取得
    // ここで参照カウンタが増えているため注意
    com_ptr<ID3D11Resource> backBuffer;
    if(FAILED(dxgiSwapChain_->GetBuffer(0, IID_PPV_ARGS(&backBuffer)))) {
        return false;
    }

    backBuffer_ = dx11::createTexture(backBuffer.Get());
    if(!backBuffer) {
        return false;
    }

    return true;
}

//---------------------------------------------------------------------------
//! 画面更新
//---------------------------------------------------------------------------
bool SwapChainImpl::present(u32 vsyncInterval)
{
    // 画面更新
    dxgiSwapChain_->Present(vsyncInterval, 0);

    //----------------------------------------------------------
    // 画面サイズ設定が変更されていたらバックバッファをリサイズ
    //----------------------------------------------------------
    auto* d3dTexture = static_cast<ID3D11Texture2D*>(backBuffer_->d3dResource());

    D3D11_TEXTURE2D_DESC desc;
    d3dTexture->GetDesc(&desc);

    if(desc.Width != width_ || desc.Height != height_) {
        // リサイズ前に一旦バックバッファを解放
        backBuffer_.reset();

        // リサイズ実行
        if(FAILED(dxgiSwapChain_->ResizeBuffers(bufferCount_, width_, height_, DXGI_FORMAT_UNKNOWN, 0))) {
            ASSERT_MESSAGE(false, "フレームバッファのサイズ変更に失敗しました。");
            return false;
        }

        //----------------------------------------------------------
        // バックバッファテクスチャの再作成
        //----------------------------------------------------------
        // スワップチェインからバックバッファのResourceを取得
        // ここで参照カウンタが増えているため注意
        com_ptr<ID3D11Resource> backBuffer;
        if(FAILED(dxgiSwapChain_->GetBuffer(0, IID_PPV_ARGS(&backBuffer)))) {
            return false;
        }
        backBuffer_ = dx11::createTexture(backBuffer.Get());
        if(!backBuffer_) {
            ASSERT_MESSAGE(backBuffer, "バックバッファの再確保に失敗しました。");
            return false;
        }
    }
    return true;
}

//---------------------------------------------------------------------------
//! バックバッファのサイズ変更
//! @note ここでは解像度変数のみ変更。present()内でバッファサイズ変更を実行。
//---------------------------------------------------------------------------
void SwapChainImpl::resizeBuffers(u32 width, u32 height)
{
    width_  = width;
    height_ = height;
}

//---------------------------------------------------------------------------
//! GPUが実行完了になるまで待つ
//---------------------------------------------------------------------------
void SwapChainImpl::waitForGpu()
{
    dx11::immediateContext()->Flush();
}

//---------------------------------------------------------------------------
//! スワップチェインの作成
//---------------------------------------------------------------------------
std::unique_ptr<dx11::SwapChain> createSwapChain(u32 width, u32 height, DXGI_FORMAT dxgiFormat, u32 bufferCount)
{
    auto p = std::make_unique<dx11::SwapChainImpl>();

    if(!p->initialize(width, height, dxgiFormat, bufferCount)) {
        p.reset();
    }
    return p;
}

}   // namespace dx11