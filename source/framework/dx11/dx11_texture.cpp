﻿//---------------------------------------------------------------------------
//!	@file   dx11_texture.cpp
//!	@brief	GPUテクスチャ
//---------------------------------------------------------------------------
#include <DirectXTex/DirectXTex/DirectXTex.h>
#include <iostream>
#include <fstream>
#include <filesystem>

namespace dx11 {

//===========================================================================
//! GPUテクスチャ実装部
//===========================================================================
class TextureImpl final : public dx11::Texture
{
public:
    //! コンストラクタ
    TextureImpl(ID3D11Resource* d3dResource, DXGI_FORMAT overrideFormat);

    //! デストラクタ
    virtual ~TextureImpl() = default;

    // 初期化
    bool initialize();

    //----------------------------------------------------------
    //! @name キャスト
    //----------------------------------------------------------
    //@{

    //! SRV参照
    virtual operator ID3D11ShaderResourceView*() const override { return d3dSRV_.Get(); }

    //! RTV参照
    virtual operator ID3D11RenderTargetView*() const override { return d3dRTV_.Get(); }

    //! DSV参照
    virtual operator ID3D11DepthStencilView*() const override { return d3dDSV_.Get(); }

    //! UAV参照
    virtual operator ID3D11UnorderedAccessView*() const override { return d3dUAV_.Get(); }

    //@}
    //----------------------------------------------------------
    //! @name 参照
    //----------------------------------------------------------
    //@{

    //! テクスチャ情報を取得
    virtual const dx11::TextureDesc& desc() const override { return desc_; }

    //! D3D12リソースを取得
    virtual ID3D11Resource* d3dResource() const override { return d3dResource_.Get(); }

    //@}
private:
    // コピー禁止/move禁止
    TextureImpl(const TextureImpl&) = delete;
    TextureImpl(TextureImpl&&)      = delete;
    TextureImpl& operator=(const TextureImpl&) = delete;
    TextureImpl& operator=(TextureImpl&&) = delete;

private:
    dx11::TextureDesc                  desc_ = {};
    com_ptr<ID3D11Resource>            d3dResource_;   //!< D3Dリソース (GPUメモリに相当)
    com_ptr<ID3D11ShaderResourceView>  d3dSRV_;        //!< ShaderResource View
    com_ptr<ID3D11RenderTargetView>    d3dRTV_;        //!< RenderTaret View
    com_ptr<ID3D11DepthStencilView>    d3dDSV_;        //!< DepthStencil View
    com_ptr<ID3D11UnorderedAccessView> d3dUAV_;        //!< UnorderedAccess View
};

//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
TextureImpl::TextureImpl(ID3D11Resource* d3dResource, DXGI_FORMAT overrideFormat)
{
    d3dResource_ = d3dResource;

    // D3Dリソースからテクスチャのパラメーターを取得
    D3D11_RESOURCE_DIMENSION dimension;
    d3dResource->GetType(&dimension);

    switch(dimension) {
        case D3D11_RESOURCE_DIMENSION_UNKNOWN:
            return;
        case D3D11_RESOURCE_DIMENSION_BUFFER:
            return;
        case D3D11_RESOURCE_DIMENSION_TEXTURE1D:
            {
                auto*                d3dTexture = static_cast<ID3D11Texture1D*>(d3dResource);
                D3D11_TEXTURE1D_DESC desc;
                d3dTexture->GetDesc(&desc);

                desc_.width_        = desc.Width;
                desc_.dxgiFormat_   = desc.Format;
                desc_.mipLevels_    = desc.MipLevels;
                desc_.arrayCount_   = desc.ArraySize;
                desc_.d3dBindFlags_ = desc.BindFlags;
                desc_.d3dUsage_     = desc.Usage;
            }
            break;
        case D3D11_RESOURCE_DIMENSION_TEXTURE2D:
            {
                auto*                d3dTexture = static_cast<ID3D11Texture2D*>(d3dResource);
                D3D11_TEXTURE2D_DESC desc;
                d3dTexture->GetDesc(&desc);

                desc_.width_        = desc.Width;
                desc_.height_       = desc.Height;
                desc_.dxgiFormat_   = desc.Format;
                desc_.mipLevels_    = desc.MipLevels;
                desc_.arrayCount_   = desc.ArraySize;
                desc_.d3dBindFlags_ = desc.BindFlags;
                desc_.d3dUsage_     = desc.Usage;
                desc_.isCubemap_    = (desc.MiscFlags & D3D11_RESOURCE_MISC_TEXTURECUBE) ? true : false;
            }
            break;
        case D3D11_RESOURCE_DIMENSION_TEXTURE3D:
            {
                auto*                d3dTexture = static_cast<ID3D11Texture3D*>(d3dResource);
                D3D11_TEXTURE3D_DESC desc;
                d3dTexture->GetDesc(&desc);

                desc_.width_        = desc.Width;
                desc_.height_       = desc.Height;
                desc_.depth_        = desc.Depth;
                desc_.dxgiFormat_   = desc.Format;
                desc_.mipLevels_    = desc.MipLevels;
                desc_.arrayCount_   = 1;
                desc_.d3dBindFlags_ = desc.BindFlags;
                desc_.d3dUsage_     = desc.Usage;
            }
            break;
    }

    // フォーマット型の上書き
    if(overrideFormat != DXGI_FORMAT_UNKNOWN) {
        desc_.dxgiFormat_ = overrideFormat;
    }
}

//---------------------------------------------------------------------------
// 初期化
//---------------------------------------------------------------------------
bool TextureImpl::initialize()
{
    ASSERT_MESSAGE(d3dResource_.Get(), "D3D11Resourceが初期化されていません.");

    //----------------------------------------------------------
    // ビューをバインドフラグから自動認識して初期化
    //----------------------------------------------------------
    if(desc_.d3dBindFlags_ & D3D11_BIND_SHADER_RESOURCE) {
        //---- ビューの種類を検出
        D3D11_SRV_DIMENSION dimension;

        if(desc_.height_ == 0) {
            dimension = (desc_.arrayCount_ > 1) ? D3D11_SRV_DIMENSION_TEXTURE1DARRAY : D3D11_SRV_DIMENSION_TEXTURE1D;
        }
        else if(desc_.depth_ == 0) {
            if(desc_.isCubemap_) {
                dimension = (desc_.arrayCount_ > 1) ? D3D11_SRV_DIMENSION_TEXTURECUBEARRAY : D3D11_SRV_DIMENSION_TEXTURECUBE;
            }
            else {
                dimension = (desc_.arrayCount_ > 1) ? D3D11_SRV_DIMENSION_TEXTURE2DARRAY : D3D11_SRV_DIMENSION_TEXTURE2D;
            }
        }
        else {
            dimension = D3D11_SRV_DIMENSION_TEXTURE3D;
        }

        CD3D11_SHADER_RESOURCE_VIEW_DESC desc(dimension, dx11::makeColorFormat(desc_.dxgiFormat_));
        if(FAILED(dx11::d3dDevice()->CreateShaderResourceView(d3dResource_.Get(), &desc, &d3dSRV_))) {
            return false;
        }
    }

    if(desc_.d3dBindFlags_ & D3D11_BIND_RENDER_TARGET) {
        //---- ビューの種類を検出
        D3D11_RTV_DIMENSION dimension;

        if(desc_.height_ == 0) {
            dimension = (desc_.arrayCount_ > 1) ? D3D11_RTV_DIMENSION_TEXTURE1DARRAY : D3D11_RTV_DIMENSION_TEXTURE1D;
        }
        else if(desc_.depth_ == 0) {
            dimension = (desc_.arrayCount_ > 1) ? D3D11_RTV_DIMENSION_TEXTURE2DARRAY : D3D11_RTV_DIMENSION_TEXTURE2D;
        }
        else {
            dimension = D3D11_RTV_DIMENSION_TEXTURE3D;
        }

        CD3D11_RENDER_TARGET_VIEW_DESC desc(dimension, desc_.dxgiFormat_);
        if(FAILED(dx11::d3dDevice()->CreateRenderTargetView(d3dResource_.Get(), &desc, &d3dRTV_))) {
            return false;
        }
    }

    if(desc_.d3dBindFlags_ & D3D11_BIND_DEPTH_STENCIL) {
        //---- ビューの種類を検出
        D3D11_DSV_DIMENSION dimension;

        if(desc_.height_ == 0) {
            dimension = (desc_.arrayCount_ > 1) ? D3D11_DSV_DIMENSION_TEXTURE1DARRAY : D3D11_DSV_DIMENSION_TEXTURE1D;
        }
        else if(desc_.depth_ == 0) {
            dimension = (desc_.arrayCount_ > 1) ? D3D11_DSV_DIMENSION_TEXTURE2DARRAY : D3D11_DSV_DIMENSION_TEXTURE2D;
        }
        else {
            return false;   // 3Dテクスチャは非サポート
        }

        CD3D11_DEPTH_STENCIL_VIEW_DESC desc(dimension, desc_.dxgiFormat_);
        if(FAILED(dx11::d3dDevice()->CreateDepthStencilView(d3dResource_.Get(), &desc, &d3dDSV_))) {
            return false;
        }
    }

    if(desc_.d3dBindFlags_ & D3D11_BIND_UNORDERED_ACCESS) {
        //---- ビューの種類を検出
        D3D11_UAV_DIMENSION dimension;

        if(desc_.height_ == 0) {
            dimension = (desc_.arrayCount_ > 1) ? D3D11_UAV_DIMENSION_TEXTURE1DARRAY : D3D11_UAV_DIMENSION_TEXTURE1D;
        }
        else if(desc_.depth_ == 0) {
            dimension = (desc_.arrayCount_ > 1) ? D3D11_UAV_DIMENSION_TEXTURE2DARRAY : D3D11_UAV_DIMENSION_TEXTURE2D;
        }
        else {
            dimension = D3D11_UAV_DIMENSION_TEXTURE3D;
        }

        CD3D11_UNORDERED_ACCESS_VIEW_DESC desc(dimension, dx11::makeColorFormat(desc_.dxgiFormat_));
        if(FAILED(dx11::d3dDevice()->CreateUnorderedAccessView(d3dResource_.Get(), &desc, &d3dUAV_))) {
            return false;
        }
    }

    return true;
}

//---------------------------------------------------------------------------
//! テクスチャを作成
//---------------------------------------------------------------------------
std::shared_ptr<dx11::Texture> createTexture(const dx11::TextureDesc& desc)
{
    //----------------------------------------------------------
    // D3D11テクスチャを作成
    //----------------------------------------------------------
    D3D11_TEXTURE2D_DESC textureDesc{};

    textureDesc.Width              = desc.width_;                                   // 幅
    textureDesc.Height             = desc.height_;                                  // 高さ
    textureDesc.MipLevels          = desc.mipLevels_;                               // ミップレベル
    textureDesc.ArraySize          = desc.arrayCount_;                              // 配列数
    textureDesc.Format             = dx11::makeTypelessFormat(desc.dxgiFormat_);    // デプスバッファの場合はTYPELESS形式でメモリ確保
    textureDesc.SampleDesc.Count   = 1;                                             // マルチサンプル数
    textureDesc.SampleDesc.Quality = 0;                                             // マルチサンプル品質
    textureDesc.Usage              = desc.d3dUsage_;                                // 用途(メモリ配置場所)
    textureDesc.BindFlags          = desc.d3dBindFlags_;                            // バインド設定(テクスチャの設定可能スロット)
    textureDesc.CPUAccessFlags     = dx11::makeD3DCpuAccessFlags(desc.d3dUsage_);   // CPUアクセス権
    textureDesc.MiscFlags          = 0;                                             // フラグ
    if(desc.isCubemap_) {
        textureDesc.MiscFlags |= D3D11_RESOURCE_MISC_TEXTURECUBE;   // 6枚のテクスチャ配列をキューブマップテクスチャとして扱う
    }

    com_ptr<ID3D11Texture2D> d3dResource;
    if(FAILED(dx11::d3dDevice()->CreateTexture2D(&textureDesc, nullptr, &d3dResource))) {
        return nullptr;
    }

    //----------------------------------------------------------
    // テクスチャクラスを作成
    //----------------------------------------------------------
    return createTexture(d3dResource.Get(), desc.dxgiFormat_);
}

//---------------------------------------------------------------------------
//! ターゲットテクスチャを作成
//---------------------------------------------------------------------------
std::shared_ptr<dx11::Texture> createTargetTexture(u32 width, u32 height, DXGI_FORMAT dxgiFormat)
{
    dx11::TextureDesc desc{};
    desc.width_        = width;                        // 幅
    desc.height_       = height;                       // 高さ
    desc.depth_        = 0;                            // 高さ(3Dテクスチャの場合)
    desc.dxgiFormat_   = dxgiFormat;                   // ピクセル形式
    desc.d3dBindFlags_ = D3D11_BIND_SHADER_RESOURCE;   // テクスチャとしてバインド

    //----------------------------------------------------------
    // ピクセルフォーマットからRenderTargetかDepthStencilか判定
    //----------------------------------------------------------
    if(dx11::isColorTargetFormat(dxgiFormat)) {
        desc.d3dBindFlags_ |= D3D11_BIND_RENDER_TARGET;   // 描画ターゲット
        return dx11::createTexture(desc);
    }
    else if(dx11::isDepthTargetFormat(dxgiFormat)) {
        desc.d3dBindFlags_ |= D3D11_BIND_DEPTH_STENCIL;   // デプスステンシル
        return dx11::createTexture(desc);
    }

    return nullptr;
}

//---------------------------------------------------------------------------
//! テクスチャをファイルから作成
//---------------------------------------------------------------------------
std::shared_ptr<dx11::Texture> createTextureFromFile(std::string_view path)
{
    //---------------------------------------------------------------------
    // ファイルからイメージを読み込み
    //---------------------------------------------------------------------
    std::vector<char> source;
    {
        // ファイルから読み込み
        std::ifstream file(std::string(path), std::ios::in | std::ios::binary | std::ios::ate);   // ateを指定すると最初からファイルポインタが末尾に移動
        if(!file.is_open()) {
            return nullptr;
        }
        auto size = file.tellg();
        source.resize(static_cast<size_t>(size));

        file.seekg(0, std::ios::beg);
        file.read(source.data(), size);
        file.close();
    }

    const std::filesystem::path filePath = path;                   // ファイルパス
    const std::filesystem::path ext      = filePath.extension();   // ファイル拡張子

    //----------------------------------------------------------
    // テクスチャイメージをファイルから生成
    // ここではメモリ上に展開するところまで
    //----------------------------------------------------------
    DirectX::TexMetadata  metadata;
    DirectX::ScratchImage scratchImage;

    // DDSファイル
    if(_wcsicmp(ext.c_str(), L".dds") == 0) {
        u32 ddsFlags = 0;
        if(FAILED(DirectX::LoadFromDDSMemory(source.data(),      // [in]  DDSファイルの先頭アドレス
                                             source.size(),      // [in]  DDSファイルのサイズ
                                             ddsFlags,           // [in]  フラグ(DirectX::DDS_FLAGS)
                                             &metadata,          // [out] テクスチャメタ情報
                                             scratchImage))) {   // [out] イメージ情報
            return nullptr;
        }
    }
    // TGAファイル
    else if(_wcsicmp(ext.c_str(), L".tga") == 0) {
        if(FAILED(DirectX::LoadFromTGAMemory(source.data(),      // [in]  TGAファイルの先頭アドレス
                                             source.size(),      // [in]  TGAファイルのサイズ
                                             &metadata,          // [out] テクスチャメタ情報
                                             scratchImage))) {   // [out] イメージ情報
            return nullptr;
        }
    }
    // その他 BMP,JPEG,PNG,TIFF,GIF,WMP,ICOファイル
    else {
        u32 flags = 0;
        if(FAILED(DirectX::LoadFromWICMemory(source.data(),      // [in]  TGAファイルの先頭アドレス
                                             source.size(),      // [in]  TGAファイルのサイズ
                                             flags,              // [in]  フラグ
                                             &metadata,          // [out] テクスチャメタ情報
                                             scratchImage))) {   // [out] イメージ情報
            return nullptr;
        }
    }

    //----------------------------------------------------------
    // ミップマップの自動生成
    //----------------------------------------------------------
    if(scratchImage.GetMetadata().mipLevels == 1) {
        DirectX::ScratchImage mippedImage;

        auto hr = DirectX::GenerateMipMaps(*scratchImage.GetImage(0, 0, 0),                    // [in]  変換元イメージ
                                           DirectX::TEX_FILTER_FANT,                           // [in]  フィルター
                                           dx11::countMips(metadata.width, metadata.height),   // [in]  生成するミップ段数
                                           mippedImage);                                       // [out] 生成されたミップマップありのイメージ
        if(SUCCEEDED(hr)) {
            scratchImage = std::move(mippedImage);   // イメージ更新
            metadata     = mippedImage.GetMetadata();
        }
    }

    source.clear();   // 画像イメージファイルのメモリを解放
    source.shrink_to_fit();

    //----------------------------------------------------------
    // パラメーターの保存
    //----------------------------------------------------------
    dx11::TextureDesc desc{};
    desc.width_        = static_cast<u32>(metadata.width);       // 幅
    desc.height_       = static_cast<u32>(metadata.height);      // 高さ
    desc.dxgiFormat_   = metadata.format;                        // ピクセルフォーマット
    desc.mipLevels_    = static_cast<u32>(metadata.mipLevels);   // ミップレベル
    desc.arrayCount_   = static_cast<u32>(metadata.arraySize);   // 配列数
    desc.d3dBindFlags_ = D3D11_BIND_SHADER_RESOURCE;             // テクスチャの設定可能スロット(テクスチャとして利用)
    desc.isCubemap_    = metadata.IsCubemap();                   // キューブマップテクスチャかどうか

    //----------------------------------------------------------
    // D3Dテクスチャを作成
    //----------------------------------------------------------
    com_ptr<ID3D11Resource> d3dResource;
    {
        u32  miscFlags = 0;
        bool forceSRGB = false;
        auto usage     = D3D11_USAGE_DEFAULT;   // ビデオメモリに配置

        // テクスチャに合わせたGPUメモリを確保(ここでは領域の確保のみ)
        // リソース状態はCOPY_DESTで作成される
        if(FAILED(DirectX::CreateTextureEx(dx11::d3dDevice(),                    // [in]  D3Dデバイス
                                           scratchImage.GetImages(),             // [in]  Image配列の先頭アドレス
                                           scratchImage.GetImageCount(),         // [in]  Imageの数
                                           metadata,                             // [in]  テクスチャメタ情報
                                           desc.d3dUsage_,                       // [in]  テクスチャの配置場所
                                           desc.d3dBindFlags_,                   // [in]  テクスチャの設定可能スロット
                                           dx11::makeD3DCpuAccessFlags(usage),   // [in]  CPUアクセス権フラグ
                                           miscFlags,                            // [in]  追加のフラグ (D3D11_RESOURCE_MISC_FLAG の組み合わせ)
                                           forceSRGB,                            // [in]  強制的にsRGBとして読み込むかどうか true:強制sRGB false:指定なし
                                           &d3dResource))) {                     // [out] 作成されたD3D11リソース
            return nullptr;
        }
    }

    //----------------------------------------------------------
    // テクスチャクラスを作成
    //----------------------------------------------------------
    return createTexture(d3dResource.Get());
}

//---------------------------------------------------------------------------
//! D3Dリソースから作成
//---------------------------------------------------------------------------
std::shared_ptr<dx11::Texture> createTexture(ID3D11Resource* d3dResource, DXGI_FORMAT overrideFormat)
{
    //---------------------------------------------------------------------
    // テクスチャクラスの初期化
    //---------------------------------------------------------------------
    auto p = std::make_shared<dx11::TextureImpl>(d3dResource, overrideFormat);

    if(!p->initialize()) {
        p.reset();
    }
    return p;
}

}   // namespace dx11
