﻿////---------------------------------------------------------------------------
////---------------------------------------------------------------------------
////!	@file	main_loop.h
////!	@brief	メインループ
////---------------------------------------------------------------------------
//#pragma once
//
//namespace application {
//
////! ウィンドウハンドルを取得
//HWND hwnd();
//
////! アプリケーションインスタンスハンドルを取得
//HINSTANCE instance();
//
////! マウスフォーカスを持っているかどうか
//bool hasMouseFocus();
//
////! アクティブかどうか
//bool isActive();
//
//}   // namespace application
//
//// 実行
//int run(const std::wstring& titleName, u32 width, u32 height, bool isWindowed);