﻿//---------------------------------------------------------------------------
//!	@file	math_bulletPhyics.h
//!	@brief	hlslpp <-> bullet physics ヘルパークラス
//---------------------------------------------------------------------------
#pragma once
namespace math {
btTransform castBtTrans(const matrix& f4);   //!< cast matrix -> btTransform
btVector3   castBtVec3(const float3& f3);   //!< cast float3 -> btVector3
}   // namespace math
