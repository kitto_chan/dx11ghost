﻿//---------------------------------------------------------------------------
//!	@file	math_bulletPhyics.h
//!	@brief	hlslpp <-> bullet physics ヘルパークラス
//---------------------------------------------------------------------------

#include "math_bulletPhysics.h"
namespace math {
//---------------------------------------------------------------------------
//! cast matrix -> btTransform
//---------------------------------------------------------------------------
btTransform castBtTrans(const matrix& mat)
{
    btTransform btTrans;
    btTrans.setFromOpenGLMatrix(math::CastF16(mat));
    return btTrans;
}
//---------------------------------------------------------------------------
//! cast float3 -> btVector3
//---------------------------------------------------------------------------
btVector3 castBtVec3(const float3& f3)
{
    return btVector3(f3.f32[0], f3.f32[1], f3.f32[2]);
}
}   // namespace math
