﻿//---------------------------------------------------------------------------
//!	@file	math_DirectX.cpp
//!	@brief	cast hlslpp <-> DirectX
//---------------------------------------------------------------------------
#include "math_DirectX.h"
namespace math {
//---------------------------------------------------------------------------
//! cast DirectX::XMFLOAT4 -> hlslpp::float4
//---------------------------------------------------------------------------
DirectX::XMFLOAT4 cast(const float4& f4)
{
    return DirectX::XMFLOAT4(f4.x, f4.y, f4.z, f4.w);
}
//---------------------------------------------------------------------------
//! cast DirectX::XMFLOAT3 -> hlslpp::float3
//---------------------------------------------------------------------------
DirectX::XMFLOAT3 cast(const float3& f3)
{
    return DirectX::XMFLOAT3(f3.x, f3.y, f3.z);
}
//---------------------------------------------------------------------------
//! cast hlslpp::float4 -> DirectX::XMFLOAT4
//---------------------------------------------------------------------------
float4 cast(const DirectX::XMFLOAT4& f4)
{
    return float4(f4.x, f4.y, f4.z, f4.w);
}
//---------------------------------------------------------------------------
//! cast hlslpp::float3 -> DirectX::XMFLOAT3
//---------------------------------------------------------------------------
float3 cast(const DirectX::XMFLOAT3& f3)
{
    return float3(f3.x, f3.y, f3.z);
}
//---------------------------------------------------------------------------
//! cast hlslpp::float4 -> DirectX::XMVECTOR
//---------------------------------------------------------------------------
DirectX::XMVECTOR castXMVector(const float4& f4)
{
    return DirectX::XMVECTOR({ f4.f32[0], f4.f32[1], f4.f32[2], f4.f32[3] });
}
}   // namespace math
