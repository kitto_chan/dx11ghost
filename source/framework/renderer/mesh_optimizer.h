﻿//---------------------------------------------------------------------------
//!	@file	mesh_optimizer.h
//!	@brief	メッシュ最適化
//---------------------------------------------------------------------------
#pragma once

//===========================================================================
//! メッシュ最適化
//===========================================================================
class MeshOptimizer
{
public:
    //! コンストラクタ
    MeshOptimizer() = default;

    //! デストラクタ
    virtual ~MeshOptimizer() = default;

    //! 最適化
    //! @param  [in]    vertices        頂点配列の先頭アドレス
    //! @param  [in]    vertexCount     頂点数
    //! @param  [in]    vertexStride    1頂点あたりのバイトサイズ
    //! @param  [in]    positionOffset  頂点構造体のxyz座標情報がある場所へのオフセット
    //! @note   頂点は三角形を構成するように3の倍数の個数が必要。
    //! @note   頂点の内部構成は問わない。バイナリイメージで識別する。
    bool optimize(const void* vertices, u32 vertexCount, size_t vertexStride, u32 positionOffset = 0);

    //! 最適化済み頂点を取得
    const std::vector<std::byte>& optimizedVertices() const { return optVertices_; }

    //! 最適化済みインデックスを取得
    const std::vector<u32>& optimizedIndices() const { return optIndices_; }

private:
    std::vector<std::byte> optVertices_;   //!< 最適化済み頂点配列
    std::vector<u32>       optIndices_;    //!< 最適化済みインデックス配列
};
