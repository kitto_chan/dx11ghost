﻿//---------------------------------------------------------------------------
//!	@file	animation.cpp
//!	@brief	3Dアニメーション
//---------------------------------------------------------------------------
#include "animation.h"
#include "animation_layer.h"
#include "impl/model_impl.h"
#include "impl/animation_impl.h"
#include "impl/animation_layer_impl.h"
#include "import_fbx.h"

//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void AnimationImpl::update(f32 t)
{
    time_ += t;

    //----------------------------------------------------------
    // アニメーション再生ループ判定
    //----------------------------------------------------------
    if(activeLayer_) {
        auto animationLength = activeLayer_->animationLength();
        if(time_ > animationLength) {
            // ループ再生時は先頭に戻す
            if(playType_ == Animation::PlayType::Loop) {
                time_ -= animationLength;
            }
            // 単発再生時は最終フレームでポーズ
            else {
                time_ = animationLength;
            }
        }
    }
}

//---------------------------------------------------------------------------
//! アニメーション再生リクエスト
//---------------------------------------------------------------------------
bool AnimationImpl::play(const char* name, Animation::PlayType playType)
{
    auto it = animationLayers_.find(name);
    if(it == std::end(animationLayers_)) {
        return false;
    }
    time_        = 0.0f;               // 現在の再生時間
    activeLayer_ = it->second.get();   // 現在再生中のアニメーションレイヤー
    playType_    = playType;           // アニメーション再生タイプ

    return true;
}

//---------------------------------------------------------------------------
//! アニメーションレイヤーを追加
//---------------------------------------------------------------------------
void AnimationImpl::appendLayer(const char* name, std::shared_ptr<AnimationLayer> layer)
{
    animationLayers_[name] = std::static_pointer_cast<AnimationLayerImpl>(layer);
}

//---------------------------------------------------------------------------
//! アニメーションレイヤーの数を取得
//---------------------------------------------------------------------------
size_t AnimationImpl::animationLayerCount() const
{
    return animationLayers_.size();
}

//---------------------------------------------------------------------------
//! アニメーションレイヤーを取得
//---------------------------------------------------------------------------
const AnimationLayer* AnimationImpl::animationLayer(const char* name) const
{
    auto it = animationLayers_.find(name);

    if(it == std::end(animationLayers_)) {
        return nullptr;
    }

    return it->second.get();
}

//---------------------------------------------------------------------------
//! ポーズ情報の作成
//---------------------------------------------------------------------------
void AnimationImpl::buildPose(Pose& pose, const ModelImpl* model)
{
    if(!activeLayer_)
        return;

    activeLayer_->updatePose(time_, pose, model, 1.0f);
}

bool AnimationImpl::IsLastFrame() const
{
    if(activeLayer_) {
        return time_ == activeLayer_->animationLength();
    }
    
    return false;
}

//---------------------------------------------------------------------------
//! アニメーション作成
//---------------------------------------------------------------------------
std::shared_ptr<Animation> createAnimation(const Animation::Desc* desc, size_t count)
{
    std::shared_ptr<AnimationImpl> p = std::make_shared<AnimationImpl>();

    if(p) {
        // Descに指定されているファイルをインポート
        for(u32 i = 0; i < count; ++i) {
            std::unique_ptr<importer::ImportFbx> importFbx = importer::createImportFbx(desc[i].path_, 1.0f, importer::ImportFbx::FLAG_IMPORT_ANIMATION);

            if(!importFbx) {
                continue;
            }

            auto animationLayer = importFbx->convertToAnimation(0);

            p->appendLayer(desc[i].name_, std::move(animationLayer));
        }
    }
    return p;
}
