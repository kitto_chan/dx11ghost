﻿//---------------------------------------------------------------------------
//!	@file	FontRenderer.h
//!	@brief	DirectXTKの文字描画
//! @note   TODO: 自分の文字描画シェーダを作る予定がある
//---------------------------------------------------------------------------
#include "FontRenderer.h"
#include <sstream>
#include <iostream>
namespace render {
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool FontRenderer::Init()
{
    _spriteBatch = std::make_unique<DirectX::SpriteBatch>(dx11::context());
    uni_ptr<DirectX::SpriteFont> _spriteFont;   //!< フォント
    _spriteFont = std::make_unique<DirectX::SpriteFont>(dx11::d3dDevice(), L"font/GLFont34.spritefont");
    _spriteFontList.insert(std::make_pair("GLFont", std::move(_spriteFont)));

    _spriteFont = std::make_unique<DirectX::SpriteFont>(dx11::d3dDevice(), L"font/Ms36B.spritefont");
    _spriteFontList.insert(std::make_pair("MsFont", std::move(_spriteFont)));
    return true;
}
//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------

void FontRenderer::Update()
{
}
//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void FontRenderer::Render()
{
    _spriteBatch->Begin();

    for(auto iter = _fontList.begin(); iter != _fontList.end(); ++iter) {
        FontDesc             fontDesc   = iter->second;
        DirectX::SpriteFont* spriteFont = _spriteFontList.at(fontDesc._fontName).get();   //!< 現在のフォント

        const wchar_t* msg = fontDesc._msg.c_str();

        // 中心点を計算
        DirectX::XMVECTOR centerPos = spriteFont->MeasureString(msg);
        centerPos.m128_f32[0] /= 2.0f;
        centerPos.m128_f32[1] /= 2.0f;

        // 描画座標
        DirectX::XMVECTOR renderPos = { fontDesc._pos.f32[0], fontDesc._pos.f32[1] };

        spriteFont->DrawString(_spriteBatch.get(),
                               msg,
                               renderPos,
                               math::castXMVector(fontDesc._color),
                               0.0f,
                               centerPos,
                               fontDesc._size);
    }

    _spriteBatch->End();
}
//---------------------------------------------------------------------------
//! 解放
//---------------------------------------------------------------------------

void FontRenderer::Finalize()
{
}
//---------------------------------------------------------------------------
//! 描画文字を設定
//---------------------------------------------------------------------------
void FontRenderer::SetFont(std::string_view key, FontDesc& desc)
{
    if(_spriteFontList.find(desc._fontName) == _spriteFontList.end()) {
        desc._fontName = DEFAULT_FONT;
        ASSERT_MESSAGE(false, "フォント存在しない ");
    }
    _fontList[key.data()] = desc;
}
//---------------------------------------------------------------------------
//! 特定文字を削除
//! @params [in] key
//---------------------------------------------------------------------------
void FontRenderer::RemoveFont(const std::string_view& key)
{
    _fontList.erase(key.data());
}
//---------------------------------------------------------------------------
//! フォントリストをクリアする
//---------------------------------------------------------------------------
void FontRenderer::Clear()
{
    _fontList.clear();
}
//============================================================================
// 実体を取得
//============================================================================
render::FontRenderer* FontRendererIns()
{
    return render::FontRenderer::Instance();
}

}   // namespace render
