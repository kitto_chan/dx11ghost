﻿//---------------------------------------------------------------------------
//!	@file	ImguiWidget.cpp
//!	@brief	自分の描画スタイル
//---------------------------------------------------------------------------
#include "imgui/imgui.h"
#include "imgui/imgui_internal.h"
#include "IconFont/IconsFontAwesome5.h"
#include "ImguiWidget.h"

namespace imgui {
namespace {
constexpr f32 LABEL_COL_SIZE = 100.0f;   //!< ラベルのColumnサイズ
}
//---------------------------------------------------------------------------
//! float3 を Imgui DragFloatの形で描画する
//---------------------------------------------------------------------------
void ImguiDragXYZ(const std::string& label, float3& xyz)
{
    constexpr f32 dragSpeed  = 0.1f;         // DragFloatの速さ
    const ImVec2  buttonSize = { 22, 22 };   // ボタンサイズ

    f32 x = xyz.x;
    f32 y = xyz.y;
    f32 z = xyz.z;

    ImGui::PushID(label.c_str());

    // 左のラベル表示
    ImGui::Columns(2);
    ImGui::SetColumnWidth(0, LABEL_COL_SIZE);
    ImGui::Text(label.c_str());

    // 右のDragFloat設定
    ImGui::NextColumn();
    ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2{ 0.0f, 5.0f });
    ImGui::PushMultiItemsWidths(3, ImGui::CalcItemWidth());

    //----------
    // X設定
    // ボタンスタイル
    ImGui::PushStyleColor(ImGuiCol_Button, ImVec4{ 0.8f, 0.15f, 0.15f, 1.0f });
    ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImVec4{ 0.9f, 0.2f, 0.2f, 1.0f });
    ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImVec4{ 0.8f, 0.15f, 0.15f, 1.0f });
    if(ImGui::Button("X", buttonSize)) x = 0.0f;   // Reset
    ImGui::PopStyleColor(3);

    // DragFloat
    ImGui::SameLine();
    ImGui::DragFloat("##X", &x, dragSpeed, 0.0f, 0.0f, "%.3f");
    ImGui::PopItemWidth();

    //----------
    // Y設定
    // ボタンスタイル
    ImGui::SameLine();
    ImGui::PushStyleColor(ImGuiCol_Button, ImVec4{ 0.15f, 0.8f, 0.15f, 1.0f });
    ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImVec4{ 0.2f, 0.9f, 0.2f, 1.0f });
    ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImVec4{ 0.15f, 0.8f, 0.15f, 1.0f });
    if(ImGui::Button("Y", buttonSize)) y = 0.0f;   // Reset
    ImGui::PopStyleColor(3);

    // DragFloat
    ImGui::SameLine();
    ImGui::DragFloat("##Y", &y, dragSpeed, 0.0f, 0.0f, "%.3f");
    ImGui::PopItemWidth();

    //----------
    // Z設定
    // ボタンスタイル
    ImGui::SameLine();
    ImGui::PushStyleColor(ImGuiCol_Button, ImVec4{ 0.15f, 0.15f, 0.8f, 1.0f });
    ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImVec4{ 0.2f, 0.2f, 0.9f, 1.0f });
    ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImVec4{ 0.15f, 0.15f, 0.8f, 1.0f });
    if(ImGui::Button("Z", buttonSize)) z = 0.0f;
    ImGui::PopStyleColor(3);

    // DragFloat
    ImGui::SameLine();
    ImGui::DragFloat("##Z", &z, dragSpeed, 0.0f, 0.0f, "%.3f");
    ImGui::PopItemWidth();

    ImGui::Columns(1);

    xyz.x = x;
    xyz.y = y;
    xyz.z = z;
    ImGui::PopStyleVar();
    ImGui::PopID();
}
void ImguiCheckBox(std::string_view label, bool& value)
{
    ImGui::Columns(2);
    ImGui::SetColumnWidth(0, LABEL_COL_SIZE);
    ImGui::Text(label.data());
    ImGui::NextColumn();
    std::string labelStr = label.data();
    labelStr             = "##" + labelStr;
    ImGui::Checkbox(labelStr.c_str(), &value);

    ImGui::Columns(1);
}
//---------------------------------------------------------------------------
//! メニューバー描画
//---------------------------------------------------------------------------
void RenderMenuBar()
{
    ImGui::Begin("MainDockspace");
    if(ImGui::BeginMenuBar()) {
        if(ImGui::BeginMenu("Debug")) {
            if(ImGui::MenuItem("Render Phy Debug", "", SystemSettingsMgr()->IsPhyDebug())) {
                SystemSettingsMgr()->SwapPhyDebug();
            }

            if(ImGui::MenuItem("Use Imgui Demo", "", SystemSettingsMgr()->IsUseImguiDemo())) {
                SystemSettingsMgr()->SwapImguiDemoFlag();
            }
            ImGui::EndMenu();
        }
        ImGui::EndMenuBar();
    }
    ImGui::End();
}
}   // namespace imgui
