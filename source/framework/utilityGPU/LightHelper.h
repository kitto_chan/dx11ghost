﻿//----------------------------------------------------------------------------
//!	@file   LightingHelper
//!	@brief  光源の関してヘルパークラス
//!	@note   方向光         (Direction Light)
//!	@note   点光源         (Point Light)
//! @note   スポットライト  (Spot Light)
//----------------------------------------------------------------------------
#pragma once

namespace render {
//! 方向光 (Direction Light)
struct DirectionalLight
{
    DirectionalLight() = default;

    DirectionalLight(const DirectionalLight&) = default;
    DirectionalLight& operator=(const DirectionalLight&) = default;

    DirectionalLight(DirectionalLight&&) = default;
    DirectionalLight& operator=(DirectionalLight&&) = default;

    DirectionalLight(const float4& _ambient, const float4& _diffuse, const float4& _specular,
                     const float3& _direction)
    : ambient(math::cast(_ambient))
    , diffuse(math::cast(_diffuse))
    , specular(math::cast(_specular))
    , direction(math::cast(_direction))
    , pad()
    {
    }

    DirectX::XMFLOAT4 ambient;
    DirectX::XMFLOAT4 diffuse;
    DirectX::XMFLOAT4 specular;
    DirectX::XMFLOAT3 direction;
    float             pad;
};

//! 点光源 ポイントライト
struct PointLight
{
    PointLight() = default;

    PointLight(const PointLight&) = default;
    PointLight& operator=(const PointLight&) = default;

    PointLight(PointLight&&) = default;
    PointLight& operator=(PointLight&&) = default;

    PointLight(const float4& _ambient, const float4& _diffuse, const float4& _specular,
               const float3& _position, float _range, const float3& _att)
    : ambient(math::cast(_ambient))
    , diffuse(math::cast(_diffuse))
    , specular(math::cast(_specular))
    , position(math::cast(_position))
    , range(_range)
    , att(math::cast(_att))
    , pad()
    {
    }

    DirectX::XMFLOAT4 ambient;
    DirectX::XMFLOAT4 diffuse;
    DirectX::XMFLOAT4 specular;

    // (position, range)
    DirectX::XMFLOAT3 position;
    float             range;

    // (A0, A1, A2, pad)
    DirectX::XMFLOAT3 att;
    float             pad;
};

//! スポットライト (Spot Light)
struct SpotLight
{
    SpotLight() = default;

    SpotLight(const SpotLight&) = default;
    SpotLight& operator=(const SpotLight&) = default;

    SpotLight(SpotLight&&) = default;
    SpotLight& operator=(SpotLight&&) = default;

    SpotLight(const float4& _ambient, const float4& _diffuse, const float4& _specular,
              const float3& _position, float _range, const float3& _direction,
              float _spot, const float3& _att)
    : ambient(math::cast(_ambient))
    , diffuse(math::cast(_diffuse))
    , specular(math::cast(_specular))
    , position(math::cast(_position))
    , range(_range)
    , direction(math::cast(_direction))
    , spot(_spot)
    , att(math::cast(_att))
    , pad()
    {
    }

    DirectX::XMFLOAT4 ambient;
    DirectX::XMFLOAT4 diffuse;
    DirectX::XMFLOAT4 specular;

    // (position, range)
    DirectX::XMFLOAT3 position;
    float             range;

    // (direction, spot)
    DirectX::XMFLOAT3 direction;
    float             spot;

    // 打包成4D向量: (att, pad)
    DirectX::XMFLOAT3 att;
    float             pad;
};

//! マテリアル (Material)
struct Material
{
    Material() = default;

    Material(const Material&) = default;
    Material& operator=(const Material&) = default;

    Material(Material&&) = default;
    Material& operator=(Material&&) = default;

    Material(const float4& _ambient, const float4& _diffuse, const float4& _specular,
             const float4& _reflect)
    : ambient(math::cast(_ambient))
    , diffuse(math::cast(_diffuse))
    , specular(math::cast(_specular))
    , reflect(math::cast(_reflect))
    {
    }

    DirectX::XMFLOAT4 ambient;
    DirectX::XMFLOAT4 diffuse;
    DirectX::XMFLOAT4 specular;   // w = 鏡面反射の強さ
    DirectX::XMFLOAT4 reflect;
};
}   // namespace render
