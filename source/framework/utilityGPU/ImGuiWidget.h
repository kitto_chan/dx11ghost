﻿//---------------------------------------------------------------------------
//!	@file	ImguiWidget.h
//!	@brief	自分の描画スタイル
//---------------------------------------------------------------------------
#pragma once
namespace imgui {
//! float3 を Imgui DragFloatの形で描画する
void ImguiDragXYZ(const std::string& label, float3& xyz);

//! ImGuiCheckBox
void ImguiCheckBox(std::string_view label, bool& value);

//! メニューバー描画
void RenderMenuBar();
}   // namespace ui
