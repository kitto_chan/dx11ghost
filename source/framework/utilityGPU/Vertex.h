﻿//===========================================================================
//! Vertex.h
//! 頂点データーを定義する
//===========================================================================
#pragma once
#include "UtilityGPU/LightHelper.h"
namespace vertex {
//---------------------------------------------------------------------------
//! @param pos    XYZ座標
//---------------------------------------------------------------------------
struct VertexPos
{
    DirectX::XMFLOAT3                     pos;
    static const D3D11_INPUT_ELEMENT_DESC inputLayout[1];
};
//---------------------------------------------------------------------------
//! @param pos     XYZ座標
//! @param color   色
//---------------------------------------------------------------------------
struct VertexPosColor
{
    VertexPosColor() = default;

    VertexPosColor(const VertexPosColor&) = default;
    VertexPosColor& operator=(const VertexPosColor&) = default;

    VertexPosColor(VertexPosColor&&) = default;
    VertexPosColor& operator=(VertexPosColor&&) = default;

    constexpr VertexPosColor(const DirectX::XMFLOAT3& _pos, const DirectX::XMFLOAT4& _color)
    : pos(_pos)
    , color(_color)
    {
    }

    DirectX::XMFLOAT3                     pos;
    DirectX::XMFLOAT4                     color;
    static const D3D11_INPUT_ELEMENT_DESC inputLayout[2];
};
//---------------------------------------------------------------------------
//! @param pos     XYZ座標
//! @param normal  ノーマル
//! @param color   色
//---------------------------------------------------------------------------
struct VertexPosNormalColor
{
    DirectX::XMFLOAT3                     pos;
    DirectX::XMFLOAT3                     normal;
    DirectX::XMFLOAT3                     color;
    static const D3D11_INPUT_ELEMENT_DESC inputLayout[3];
};
//---------------------------------------------------------------------------
//! @param pos     XYZ座標
//! @param normal  ノーマル
//! @param tex     テクスチャ
//---------------------------------------------------------------------------
struct VertexPosNormalTex
{
    VertexPosNormalTex() = default;

    VertexPosNormalTex(const VertexPosNormalTex&) = default;
    VertexPosNormalTex& operator=(const VertexPosNormalTex&) = default;

    VertexPosNormalTex(VertexPosNormalTex&&) = default;
    VertexPosNormalTex& operator=(VertexPosNormalTex&&) = default;

    constexpr VertexPosNormalTex(const DirectX::XMFLOAT3& _pos, const DirectX::XMFLOAT3& _normal,
                                 const DirectX::XMFLOAT2& _tex)
    : pos(_pos)
    , normal(_normal)
    , tex(_tex)
    {
    }

    DirectX::XMFLOAT3                     pos;
    DirectX::XMFLOAT3                     normal;
    DirectX::XMFLOAT2                     tex;
    static const D3D11_INPUT_ELEMENT_DESC inputLayout[3];
};

//---------------------------------------------------------------------------
//! @param pos     XYZ座標
//! @param normal  ノーマル
//! @param tex     テクスチャ
//! @param normal  ボーン
//! @param weight  ウェイト、メッシュの「影響度」
//---------------------------------------------------------------------------
struct VertexPosNorTexBonWei
{
    VertexPosNorTexBonWei() = default;

    VertexPosNorTexBonWei(const VertexPosNorTexBonWei&) = default;
    VertexPosNorTexBonWei& operator=(const VertexPosNorTexBonWei&) = default;

    VertexPosNorTexBonWei(VertexPosNorTexBonWei&&) = default;
    VertexPosNorTexBonWei& operator=(VertexPosNorTexBonWei&&) = default;

    constexpr VertexPosNorTexBonWei(const DirectX::XMFLOAT3& _pos,
                                    const DirectX::XMFLOAT3& _normal,
                                    const DirectX::XMFLOAT2& _tex,
                                    const DirectX::XMINT4&   _boneId,
                                    const DirectX::XMFLOAT4& _weight)
    : pos(_pos)
    , normal(_normal)
    , tex(_tex)
    , boneId(_boneId)
    , weight(_weight)
    {
    }

    DirectX::XMFLOAT3 pos;
    DirectX::XMFLOAT3 normal;
    DirectX::XMFLOAT2 tex;
    DirectX::XMINT4   boneId;
    DirectX::XMFLOAT4 weight;

    static const D3D11_INPUT_ELEMENT_DESC inputLayout[5];
};
//---------------------------------------------------------------------------
//! @param pos     XYZ座標
//! @param tex     テクスチャ
//---------------------------------------------------------------------------
struct VertexPosTex
{
    VertexPosTex() = default;

    VertexPosTex(const VertexPosTex&) = default;
    VertexPosTex& operator=(const VertexPosTex&) = default;

    VertexPosTex(VertexPosTex&&) = default;
    VertexPosTex& operator=(VertexPosTex&&) = default;

    constexpr VertexPosTex(const DirectX::XMFLOAT3& _pos, const DirectX::XMFLOAT2& _tex)
    : pos(_pos)
    , tex(_tex)
    {
    }

    DirectX::XMFLOAT3                     pos;
    DirectX::XMFLOAT2                     tex;
    static const D3D11_INPUT_ELEMENT_DESC inputLayout[2];
};
//---------------------------------------------------------------------------
//! @param pos     XYZ座標
//! @param nor     ノーマル
//! @param tangent 正接
//! @param tex     テクスチャ
//---------------------------------------------------------------------------
struct VertexPosNormalTangentex
{
    DirectX::XMFLOAT3 pos;
    DirectX::XMFLOAT3 nor;
    DirectX::XMFLOAT4 tangent;
    DirectX::XMFLOAT2 tex;

    static const D3D11_INPUT_ELEMENT_DESC inputLayout[4];
};

}   // namespace vertex
