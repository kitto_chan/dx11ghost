﻿//---------------------------------------------------------------------------
//!	@file	debug_draw_impl.h
//!	@brief	デバッグ描画(実装部)
//---------------------------------------------------------------------------
#pragma once

namespace debug {

//! デバッグ描画の初期化
bool initializeDebugDraw();

//! デバッグ描画の一括発行
void flushDebugDraw();

//! デバッグ描画の解放
void finalizeDebugDraw();

}   // namespace debug