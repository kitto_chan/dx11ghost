﻿//---------------------------------------------------------------------------
//!	@file	physics_character.cpp
//!	@brief	キャラクター
//---------------------------------------------------------------------------
#include "physics_character.h"

namespace physics {

namespace {

//--------------------------------------------------------------
// 内部変数
//--------------------------------------------------------------
btGhostPairCallback callback_;   // コールバック関数実体(常駐)

}   // namespace

//---------------------------------------------------------------------------
//! キャスト float3 → btVector3
//---------------------------------------------------------------------------
btVector3 cast(const float3& v)
{
    return btVector3(v.x, v.y, v.z);
}

//---------------------------------------------------------------------------
//! キャスト btVector3 → float3
//---------------------------------------------------------------------------
float3 cast(const btVector3& v)
{
    return float3(v.x(), v.y(), v.z());
}

//===========================================================================
//! キャラクター(実装部)
//===========================================================================
class CharacterImpl final : public physics::Character
{
public:
    //! コンストラクタ
    CharacterImpl() = default;

    //! デストラクタ
    virtual ~CharacterImpl();

    //! 初期化
    //! @param  [in]    matWorld    初期ワールド行列
    //! @param  [in]    radius      球の半径
    CharacterImpl(const matrix& matWorld, f32 radius);
    //! 初期化
    //! @param  [in]    matWorld    初期ワールド行列
    //! @param  [in]    radius      カプセルの半径
	//! @param  [in]    height      カプセルの高さ
    CharacterImpl(const matrix& matWorld, f32 radius, f32 height);

	//! 初期化(共通)
	void Init(const matrix& matWorld);
    //----------------------------------------------------------
    //! @name   更新
    //----------------------------------------------------------
    //@{

    //! 移動
    virtual void walk(const float3& walkDirection) override { characterController_->setWalkDirection(cast(walkDirection)); }

    //! ワープ(座標の設定)
    virtual void warp(const float3& position) override { characterController_->warp(cast(position)); }

    //! ジャンプ
    virtual void jump(const float3& v = float3(0.0f, 0.0f, 0.0f)) override { characterController_->jump(cast(v)); }

    //! 撃力を与える
    virtual void applyImpulse(const float3& v) override { characterController_->applyImpulse(cast(v)); }

    //@}
    //----------------------------------------------------------
    //! @name   設定
    //----------------------------------------------------------
    //@{

    //! 角速度の設定
    virtual void setAngularVelocity(const float3& velocity) override { characterController_->setAngularVelocity(cast(velocity)); }

    //! 速度の設定
    virtual void setLinearVelocity(const float3& velocity) override { characterController_->setLinearVelocity(cast(velocity)); }

    //! 角速度のダンパー設定
    virtual void setAngularDamping(f32 d) override { characterController_->setAngularDamping(d); }

    //! 速度のダンパー設定
    virtual void setLinearDamping(f32 d) override { characterController_->setLinearDamping(d); }

    //! 落下速度制限設定
    virtual void setFallSpeed(f32 fallSpeed) override { characterController_->setFallSpeed(fallSpeed); }

    //! ジャンプ速度(上昇速度)制限設定
    virtual void setJumpSpeed(f32 jumpSpeed) override { characterController_->setJumpSpeed(jumpSpeed); }

    //! 重力設定
    virtual void setGravity(const float3& gravity) override { characterController_->setGravity(cast(gravity)); }

    //! 斜面の最大傾斜角設定
    //! @note この指定角度以上の斜面は滑り落ちるようになります (初期値:45°)
    virtual void setMaxSlope(f32 slopeDegree = 45.0f) override { characterController_->setMaxSlope(btRadians(slopeDegree)); }

    //@}
    //----------------------------------------------------------
    //! @name   参照
    //----------------------------------------------------------
    //@{

    //! 角速度取得
    virtual const float3 getAngularVelocity() const override { return cast(characterController_->getAngularVelocity()); }

    //! 速度取得
    virtual float3 getLinearVelocity() const override { return cast(characterController_->getLinearVelocity()); }

    //! 角速度のダンパー取得
    virtual f32 getAngularDamping() const override { return characterController_->getAngularDamping(); }

    //! 速度のダンパー取得
    virtual f32 getLinearDamping() const override { return characterController_->getLinearDamping(); }

    //! 落下速度制限取得
    virtual f32 getFallSpeed() const override { return characterController_->getFallSpeed(); }

    //! ジャンプ速度(上昇速度)制限取得
    virtual f32 getJumpSpeed() const override { return characterController_->getJumpSpeed(); }

    //! 重力取得
    virtual float3 getGravity() const override { return cast(characterController_->getGravity()); }

    //! 斜面の最大傾斜角取得
    virtual f32 getMaxSlope() const override { return characterController_->getMaxSlope(); }

    //! ジャンプ可能かどうか
    virtual bool canJump() const override { return characterController_->canJump(); }

    //! 地面に接地しているかどうか
    virtual bool onGround() const override { return characterController_->onGround(); }

    //! ワールド行列を取得
    virtual matrix worldMatrix() const override;

    //! Bullet ゴーストオブジェクトを取得
    //! @note キネマティックキャラクターに対応するコリジョンオブジェクト
    virtual btPairCachingGhostObject* ghostObject() const override { return btGhostObject_.get(); }

    //! Bullet キャラクターコントローラーを取得
    virtual btKinematicCharacterController* characterController() const override { return characterController_.get(); }

    //@}

private:
    // コピー禁止/move禁止
    CharacterImpl(const CharacterImpl&) = delete;
    CharacterImpl(CharacterImpl&&)      = delete;
    CharacterImpl& operator=(const CharacterImpl&) = delete;
    CharacterImpl& operator=(CharacterImpl&&) = delete;

private:
    std::unique_ptr<btConvexShape>                  btCollisionShape_;      //!< Bullet 凸コリジョンシェイプ
    std::unique_ptr<btPairCachingGhostObject>       btGhostObject_;         //!< Bullet ゴーストオブジェクト(キネマティックキャラクターに対応するコリジョンオブジェクト)
    std::unique_ptr<btKinematicCharacterController> characterController_;   //!< Bullet キャラクターコントローラー
};

//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
CharacterImpl::CharacterImpl(const matrix& matWorld, f32 radius)
{
    //----------------------------------------------------------
    // コリジョンシェイプ(球)を作成
    //----------------------------------------------------------
    btCollisionShape_ = std::make_unique<btSphereShape>(radius);

	Init(matWorld);
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
CharacterImpl::CharacterImpl(const matrix& matWorld, f32 radius, f32 height)
{
    //----------------------------------------------------------
    // コリジョンシェイプ(カプセル)を作成
    //----------------------------------------------------------
    btCollisionShape_ = std::make_unique<btCapsuleShapeZ>(radius,height);

	Init(matWorld);
}
//---------------------------------------------------------------------------
//! 初期化（共通）
//---------------------------------------------------------------------------
void CharacterImpl::Init(const matrix& matWorld)
{
    //----------------------------------------------------------
    // 初期位置を作成
    //----------------------------------------------------------
    f32 m[16];   // 4x4行列
    m[0]  = matWorld.f32_0[0];
    m[1]  = matWorld.f32_0[1];
    m[2]  = matWorld.f32_0[2];
    m[3]  = matWorld.f32_0[3];
    m[4]  = matWorld.f32_1[0];
    m[5]  = matWorld.f32_1[1];
    m[6]  = matWorld.f32_1[2];
    m[7]  = matWorld.f32_1[3];
    m[8]  = matWorld.f32_2[0];
    m[9]  = matWorld.f32_2[1];
    m[10] = matWorld.f32_2[2];
    m[11] = matWorld.f32_2[3];
    m[12] = matWorld.f32_3[0];
    m[13] = matWorld.f32_3[1];
    m[14] = matWorld.f32_3[2];
    m[15] = matWorld.f32_3[3];

    btTransform transform;
    transform.setFromOpenGLMatrix(m);

    //----------------------------------------------------------
    // コリジョンオブジェクトを生成
    //----------------------------------------------------------
    auto* dynamicsWorld = physics::PhysicsEngine::instance()->dynamicsWorld();

    // btPairCachingGhostObjectの衝突検出を登録
    dynamicsWorld->getBroadphase()->getOverlappingPairCache()->setInternalGhostPairCallback(&callback_);

    // コリジョンオブジェクト作成
    btGhostObject_ = std::make_unique<btPairCachingGhostObject>();
    btGhostObject_->setWorldTransform(transform);                                                                         // 初期姿勢
    btGhostObject_->setCollisionShape(btCollisionShape_.get());                                                           // シェイプを設定
    btGhostObject_->setCollisionFlags(btCollisionObject::CF_CHARACTER_OBJECT | btCollisionObject::CF_KINEMATIC_OBJECT);   // キャラクターオブジェクト

    // CCD (Continuous Collision Detection) 有効
    // 当たり抜け発生を抑制
    btGhostObject_->setCcdMotionThreshold(1.0f);
    btGhostObject_->setCcdSweptSphereRadius(0.2f);

    // ワールドに登録
    dynamicsWorld->addCollisionObject(btGhostObject_.get(), btBroadphaseProxy::CharacterFilter, btBroadphaseProxy::StaticFilter | btBroadphaseProxy::DefaultFilter);

    //----------------------------------------------------------
    // キネマティックキャラクター作成
    //----------------------------------------------------------
    constexpr f32 stepHeight = 0.1f;   // 地面に張り付く判定になる高さ 0.1m以内

    characterController_ = std::make_unique<btKinematicCharacterController>(btGhostObject_.get(), btCollisionShape_.get(), stepHeight);

    // パラメーター設定
    characterController_->setGravity(dynamicsWorld->getGravity());   // 重力
    characterController_->setMaxPenetrationDepth(0.0f);

    // キャラクターをワールドに登録
    dynamicsWorld->addCharacter(characterController_.get());
}

//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
CharacterImpl::~CharacterImpl()
{
    auto* dynamicsWorld = physics::PhysicsEngine::instance()->dynamicsWorld();

    // ワールドから登録解除
    dynamicsWorld->removeCharacter(characterController_.get());
    dynamicsWorld->removeCollisionObject(btGhostObject_.get());
}

//---------------------------------------------------------------------------
//! ワールド行列を取得
//---------------------------------------------------------------------------
matrix CharacterImpl::worldMatrix() const
{
    const auto& transform = btGhostObject_->getWorldTransform();

    f32 m[16];
    transform.getOpenGLMatrix(m);

    return matrix(m[0], m[1], m[2], m[3],
                  m[4], m[5], m[6], m[7],
                  m[8], m[9], m[10], m[11],
                  m[12], m[13], m[14], m[15]);
}

//---------------------------------------------------------------------------
//! キャラクターを作成(球体)
//---------------------------------------------------------------------------
std::unique_ptr<physics::Character> createCharacter(const matrix& matWorld, f32 radius)
{
    std::unique_ptr<physics::CharacterImpl> p = std::make_unique<physics::CharacterImpl>(matWorld, radius);
    return p;
}
//---------------------------------------------------------------------------
//! キャラクターを作成(カプセル)
//---------------------------------------------------------------------------
std::unique_ptr<physics::Character> createCharacter(const matrix& matWorld, f32 radius, f32 height)
{
    std::unique_ptr<physics::CharacterImpl> p = std::make_unique<physics::CharacterImpl>(matWorld, radius, height);
    return p;
}

}   // namespace physics
