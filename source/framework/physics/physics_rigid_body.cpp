﻿//---------------------------------------------------------------------------
//!	@file	physics_rigid_body.cpp
//!	@brief	剛体
//---------------------------------------------------------------------------
#include "physics_rigid_body.h"

class btRigidBody;

namespace physics {

//===========================================================================
//! 剛体(実装部)
//===========================================================================
class RigidBodyImpl final : public physics::RigidBody
{
public:
    //! コンストラクタ
    RigidBodyImpl() = default;

    //! デストラクタ
    virtual ~RigidBodyImpl();

    //! 初期化
    //! @param  [in]    mass        質量
    //! @param  [in]    matWorld    初期ワールド行列
    //! @param  [in]    shape       Bulletコリジョンシェイプ
    RigidBodyImpl(f32 mass, const matrix& matWorld, std::unique_ptr<btCollisionShape> shape);

    //! モデルメッシュで初期化
    //! @param  [in]    mass        質量
    //! @param  [in]    matWorld    初期ワールド行列
    //! @param  [in]    model       モデル
    RigidBodyImpl(f32 mass, const matrix& matWorld, raw_ptr<Model> model);

    //! ワールド行列を取得
    virtual matrix worldMatrix() const override;

    //! Bullet剛体を取得
    virtual btRigidBody* nativeRigidBody() const override { return btRigidBody_.get(); }

    //! Bulletシェイプ形状を取得
    virtual BroadphaseNativeTypes shapeType() const override { return static_cast<BroadphaseNativeTypes>(btCollisionShape_->getShapeType()); };

    //! Bullet物体位置を取得
    virtual btMotionState* motionState() const override { return btMotionState_.get(); }

	//! Bulletワールド行列を設定
    virtual void setWorldMatrix(const matrix& worldMat) override { btMotionState_->setWorldTransform(math::castBtTrans(worldMat)); }

	//! Bullet衝突用フラッグを取得
	virtual s32 getCollisionFlags() const override { return btRigidBody_->getCollisionFlags(); };

private:
    //! 初期化
    void initialize(f32 mass, const matrix& matWorld, std::unique_ptr<btCollisionShape> shape);

private:
    // コピー禁止/move禁止
    RigidBodyImpl(const RigidBodyImpl&) = delete;
    RigidBodyImpl(RigidBodyImpl&&)      = delete;
    RigidBodyImpl& operator=(const RigidBodyImpl&) = delete;
    RigidBodyImpl& operator=(RigidBodyImpl&&) = delete;

private:
    std::unique_ptr<btRigidBody>      btRigidBody_;        //!< Bullet 剛体
    std::unique_ptr<btCollisionShape> btCollisionShape_;   //!< Bullet コリジョンシェイプ
    std::unique_ptr<btMotionState>    btMotionState_;      //!< Bullet モーションステート

    //----
    // 三角形メッシュのために使用中はメモリを保持しておく必要がある
    std::unique_ptr<btTriangleIndexVertexArray> triangles_;         //!< 三角形リスト
    std::vector<u32>                            triangleIndices_;   //!< TriangleMesh用インデックス配列
};

//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
RigidBodyImpl::RigidBodyImpl(f32 mass, const matrix& matWorld, std::unique_ptr<btCollisionShape> shape)
{
    // 初期化
    initialize(mass, matWorld, std::move(shape));
}

//---------------------------------------------------------------------------
//! モデルメッシュで初期化
//---------------------------------------------------------------------------
RigidBodyImpl::RigidBodyImpl(f32 mass, const matrix& matWorld, raw_ptr<Model> model)
{
    triangles_ = std::make_unique<btTriangleIndexVertexArray>();

    //-----------------------------------------------------------------------
    // インデックス配列を生成
    //-----------------------------------------------------------------------
    {
        const u32 triangleCount = model->vertexCount() / 3;

        triangleIndices_.reserve(triangleCount * 3);
        for(u32 i = 0; i < triangleCount; ++i) {
            triangleIndices_.push_back(i * 3 + 0);
            triangleIndices_.push_back(i * 3 + 1);
            triangleIndices_.push_back(i * 3 + 2);
        }

        // メッシュ情報を登録
        btIndexedMesh part;

        part.m_vertexBase          = reinterpret_cast<const unsigned char*>(model->vertices());   // 頂点配列の先頭
        part.m_vertexStride        = sizeof(float3);                                              // 1頂点あたりの間隔
        part.m_numVertices         = model->vertexCount();                                        // 頂点数
        part.m_triangleIndexBase   = reinterpret_cast<const unsigned char*>(model->indices());    // 三角形インデックス配列の先頭
        part.m_triangleIndexStride = sizeof(u32) * 3;                                             // 三角形インデックス1個あたりの間隔
        part.m_numTriangles        = model->indexCount() / 3;                                     // 三角形数
        part.m_indexType           = PHY_INTEGER;                                                 // インデックスはu32

        triangles_->addIndexedMesh(part, PHY_INTEGER);
    }

    //-----------------------------------------------------------------------
    // シェイプの生成
    //-----------------------------------------------------------------------
    constexpr bool useQuantizedAabbCompression = true;

    std::unique_ptr<btBvhTriangleMeshShape> collisionShape = std::make_unique<btBvhTriangleMeshShape>(triangles_.get(), useQuantizedAabbCompression);
	
    // 初期化
    initialize(mass, matWorld, std::move(collisionShape));
}

//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
void RigidBodyImpl::initialize(f32 mass, const matrix& matWorld, std::unique_ptr<btCollisionShape> shape)
{
    btAssert((!shape || shape->getShapeType() != INVALID_SHAPE_PROXYTYPE));

    // シェイプと質量から慣性モーメントを自動計算
    bool      isDynamic = (mass != 0.0f);       // 動的オブジェクトかどうか
    btVector3 localInertia(0.0f, 0.0f, 0.0f);   // 慣性モーメント
    if(isDynamic) {
        shape->calculateLocalInertia(mass, localInertia);
    }

    //----------------------------------------------------------
    // コリジョンシェイプを登録
    //----------------------------------------------------------
    btCollisionShape_ = std::move(shape);   // メモリ上に持っておく必要がある

    //----------------------------------------------------------
    // 初期位置を作成
    //----------------------------------------------------------
    f32 m[16];   // 4x4行列
    m[0]  = matWorld.f32_0[0];
    m[1]  = matWorld.f32_0[1];
    m[2]  = matWorld.f32_0[2];
    m[3]  = matWorld.f32_0[3];
    m[4]  = matWorld.f32_1[0];
    m[5]  = matWorld.f32_1[1];
    m[6]  = matWorld.f32_1[2];
    m[7]  = matWorld.f32_1[3];
    m[8]  = matWorld.f32_2[0];
    m[9]  = matWorld.f32_2[1];
    m[10] = matWorld.f32_2[2];
    m[11] = matWorld.f32_2[3];
    m[12] = matWorld.f32_3[0];
    m[13] = matWorld.f32_3[1];
    m[14] = matWorld.f32_3[2];
    m[15] = matWorld.f32_3[3];

    btTransform transform;
    transform.setFromOpenGLMatrix(m);

    //----------------------------------------------------------
    // MotionState作成
    //----------------------------------------------------------
    // オブジェクトのワールド変換行列をプログラムに受け渡すインターフェース
    btMotionState_ = std::make_unique<btDefaultMotionState>(transform);

    //----------------------------------------------------------
    // 剛体の作成
    //----------------------------------------------------------
    btRigidBody::btRigidBodyConstructionInfo info(mass, btMotionState_.get(), btCollisionShape_.get(), localInertia);
    btRigidBody_ = std::make_unique<btRigidBody>(info);

    // CCD (Continuous Collision Detection) 有効
    // 当たり抜け発生を抑制
    btRigidBody_->setCcdMotionThreshold(1.0f);
    btRigidBody_->setCcdSweptSphereRadius(0.2f);

    btRigidBody_->setFriction(0.5f);      // 摩擦係数
    btRigidBody_->setRestitution(0.6f);   // 跳ね返り係数

    btRigidBody_->setUserIndex(-1);

    // 剛体をワールドに登録
    physics::PhysicsEngine::instance()->registerRigidBody(btRigidBody_.get());
}

//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
RigidBodyImpl::~RigidBodyImpl()
{
    // ワールドから剛体を登録解除
    physics::PhysicsEngine::instance()->unregisterRigidBody(btRigidBody_.get());
}

//---------------------------------------------------------------------------
//! ワールド行列を取得
//---------------------------------------------------------------------------
matrix RigidBodyImpl::worldMatrix() const
{
    const auto& transform = btRigidBody_->getWorldTransform();

    f32 m[16];
    transform.getOpenGLMatrix(m);

    return matrix(m[0], m[1], m[2], m[3],
                  m[4], m[5], m[6], m[7],
                  m[8], m[9], m[10], m[11],
                  m[12], m[13], m[14], m[15]);
}

//---------------------------------------------------------------------------
//! Bulletコリジョンシェイプを指定して剛体を作成
//---------------------------------------------------------------------------
std::unique_ptr<physics::RigidBody> createRigidBody(f32 mass, const matrix& matWorld, std::unique_ptr<btCollisionShape> collisionShape)
{
    std::unique_ptr<physics::RigidBodyImpl> p = std::make_unique<physics::RigidBodyImpl>(mass, matWorld, std::move(collisionShape));

    return p;
}

//---------------------------------------------------------------------------
//! 箱(ボックス)剛体を作成
//---------------------------------------------------------------------------
std::unique_ptr<physics::RigidBody> createRigidBody(f32 mass, const matrix& matWorld, const shape::Box& shape)
{
    // シェイプの生成
    auto collisionShape = std::make_unique<btBoxShape>(btVector3(shape.halfExtents_.x, shape.halfExtents_.y, shape.halfExtents_.z));

    std::unique_ptr<physics::RigidBodyImpl> p = std::make_unique<physics::RigidBodyImpl>(mass, matWorld, std::move(collisionShape));

    return p;
}

//---------------------------------------------------------------------------
//! 球剛体を作成
//---------------------------------------------------------------------------
std::unique_ptr<physics::RigidBody> createRigidBody(f32 mass, const matrix& matWorld, const shape::Sphere& shape)
{
    // シェイプの生成
    auto collisionShape = std::make_unique<btSphereShape>(shape.radius_);

    std::unique_ptr<physics::RigidBodyImpl> p = std::make_unique<physics::RigidBodyImpl>(mass, matWorld, std::move(collisionShape));

    return p;
}

//---------------------------------------------------------------------------
//! 円筒(シリンダー)剛体を作成
//---------------------------------------------------------------------------
std::unique_ptr<physics::RigidBody> createRigidBody(f32 mass, const matrix& matWorld, const shape::Cylinder& shape)
{
    // シェイプの生成
    auto collisionShape = std::make_unique<btCylinderShape>(btVector3(shape.radius_, shape.heightHalfExtent_, shape.radius_));

    std::unique_ptr<physics::RigidBodyImpl> p = std::make_unique<physics::RigidBodyImpl>(mass, matWorld, std::move(collisionShape));

    return p;
}

//---------------------------------------------------------------------------
//! モデルメッシュで剛体を作成
//---------------------------------------------------------------------------
std::unique_ptr<physics::RigidBody> createRigidBody(f32 mass, const matrix& matWorld, const raw_ptr<Model> model)
{
    if(!model) {
        return nullptr;
    }

    std::unique_ptr<physics::RigidBodyImpl> p = std::make_unique<physics::RigidBodyImpl>(mass, matWorld, model);
    return p;
}

}   // namespace physics
