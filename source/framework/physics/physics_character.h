﻿//---------------------------------------------------------------------------
//!	@file	physics_character.h
//!	@brief	キャラクター
//---------------------------------------------------------------------------
#pragma once

//--------------------------------------------------------------
// Bullet Physics SDK
//--------------------------------------------------------------
#pragma warning(push)
#pragma warning(disable : 26495)
#include <BulletDynamics/Character/btKinematicCharacterController.h>
#include <BulletCollision/CollisionDispatch/btGhostObject.h>
#pragma warning(pop)

namespace physics {

//===========================================================================
//! キャラクター
//===========================================================================
class Character
{
public:
    //! デストラクタ
    virtual ~Character() = default;

    //----------------------------------------------------------
    //! @name   更新
    //----------------------------------------------------------
    //@{

    //! 移動
    virtual void walk(const float3& walkDirection) = 0;

    //! ワープ(座標の設定)
    virtual void warp(const float3& position) = 0;

    //! ジャンプ
    virtual void jump(const float3& v = float3(0.0f, 0.0f, 0.0f)) = 0;

    //! 撃力を与える
    virtual void applyImpulse(const float3& v) = 0;

    //@}
    //----------------------------------------------------------
    //! @name   設定
    //----------------------------------------------------------
    //@{

    //! 角速度の設定
    virtual void setAngularVelocity(const float3& velocity) = 0;

    //! 速度の設定
    virtual void setLinearVelocity(const float3& velocity) = 0;

    //! 角速度のダンパー設定
    virtual void setAngularDamping(f32 d) = 0;

    //! 速度のダンパー設定
    virtual void setLinearDamping(f32 d) = 0;

    //! 落下速度制限設定
    virtual void setFallSpeed(f32 fallSpeed) = 0;

    //! ジャンプ速度(上昇速度)制限設定
    virtual void setJumpSpeed(f32 jumpSpeed) = 0;

    //! 重力設定
    virtual void setGravity(const float3& gravity) = 0;

    //! 斜面の最大傾斜角設定
    //! @note この指定角度以上の斜面は滑り落ちるようになります (初期値:45°)
    virtual void setMaxSlope(f32 slopeDegree = 45.0f) = 0;

    //@}
    //----------------------------------------------------------
    //! @name   参照
    //----------------------------------------------------------
    //@{

    //! 角速度取得
    virtual const float3 getAngularVelocity() const = 0;

    //! 速度取得
    virtual float3 getLinearVelocity() const = 0;

    //! 角速度のダンパー取得
    virtual f32 getAngularDamping() const = 0;

    //! 速度のダンパー取得
    virtual f32 getLinearDamping() const = 0;

    //! 落下速度制限取得
    virtual f32 getFallSpeed() const = 0;

    //! ジャンプ速度(上昇速度)制限取得
    virtual f32 getJumpSpeed() const = 0;

    //! 重力取得
    virtual float3 getGravity() const = 0;

    //! 斜面の最大傾斜角取得
    virtual f32 getMaxSlope() const = 0;

    //! ジャンプ可能かどうか
    virtual bool canJump() const = 0;

    //! 地面に接地しているかどうか
    virtual bool onGround() const = 0;

    //! ワールド行列を取得
    virtual matrix worldMatrix() const = 0;

    //! Bullet ゴーストオブジェクトを取得
    //! @note キネマティックキャラクターに対応するコリジョンオブジェクト
    virtual btPairCachingGhostObject* ghostObject() const = 0;

    //! Bullet キャラクターコントローラーを取得
    virtual btKinematicCharacterController* characterController() const = 0;

    //@}
};

//===========================================================================
//! @defgroup   インスタンス生成
//===========================================================================
//@{

//! キャラクターを作成
//! @param  [in]    matWorld    初期ワールド行列
//! @param  [in]    radius      球の半径
[[nodiscard]] std::unique_ptr<physics::Character> createCharacter(const matrix& matWorld, f32 radius);
//! @param  [in]    matWorld    初期ワールド行列
//! @param  [in]    radius      カプセルの半径
//! @param  [in]    height      カプセルの高さ
[[nodiscard]] std::unique_ptr<physics::Character> createCharacter(const matrix& matWorld, f32 radius, f32 height);
//@}

}   // namespace physics
