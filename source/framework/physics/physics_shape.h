﻿//---------------------------------------------------------------------------
//!	@file	physics_shape.h
//!	@brief	形状定義
//---------------------------------------------------------------------------
#pragma once

namespace shape {

//===========================================================================
//! ボックス
//===========================================================================
struct Box
{
public:
    //! デフォルトコンストラクタ
    Box() = default;

    //! 初期化
    Box(const float3& halfExtents)
    : halfExtents_(halfExtents)
    {
    }

public:
    float3 halfExtents_ = float3(0.0f, 0.0f, 0.0f);   //!< 幅の範囲の半分
};

//===========================================================================
//! 球
//===========================================================================
struct Sphere
{
public:
    //! デフォルトコンストラクタ
    Sphere() = default;

    //! 初期化
    Sphere(f32 radius)
    : radius_(radius)
    {
    }

public:
    f32 radius_ = 0.0f;   //!< 半径
};

//===========================================================================
//! 円筒
//===========================================================================
struct Cylinder
{
public:
    //! デフォルトコンストラクタ
    Cylinder() = default;

    //! 初期化
    Cylinder(f32 radius, f32 heightHalfExtent)
    : radius_(radius)
    , heightHalfExtent_(heightHalfExtent)
    {
    }

public:
    f32 radius_           = 0.0f;   //!< 半径
    f32 heightHalfExtent_ = 0.0f;   //!< 高さの半分の幅
};

}   // namespace shape
