﻿//---------------------------------------------------------------------------
//!	@file	physics_rigid_body.h
//!	@brief	剛体
//---------------------------------------------------------------------------
#pragma once

class btRigidBody;
class Model;

namespace physics {

//===========================================================================
//! 剛体
//===========================================================================
class RigidBody
{
public:
    virtual ~RigidBody() = default;

    //! ワールド行列を取得
    virtual matrix worldMatrix() const = 0;

    //! Bullet剛体を取得
    virtual btRigidBody* nativeRigidBody() const = 0;

    //! Bulletシェイプ形状を取得
    virtual BroadphaseNativeTypes shapeType() const = 0;

	//! Bullet物体の変換行列を取得
	virtual btMotionState* motionState() const = 0;

    //! Bullet物体位置を設定
	virtual void setWorldMatrix(const matrix& worldMat) = 0;

	//! Bullet衝突用フラッグを取得
	virtual s32 getCollisionFlags() const = 0;
};

//===========================================================================
//! @defgroup   インスタンス生成
//===========================================================================
//@{

//! Bulletコリジョンシェイプを指定して剛体を作成
//! @param  [in]    mass            質量(単位:kg)
//! @param  [in]    matWorld        初期姿勢ワールド行列
//! @param  [in]    collisionShape  形状
[[nodiscard]] std::unique_ptr<physics::RigidBody> createRigidBody(f32 mass, const matrix& matWorld, std::unique_ptr<btCollisionShape> collisionShape);

//! 箱(ボックス)剛体を作成
//! @param  [in]    mass        質量(単位:kg)
//! @param  [in]    matWorld    初期姿勢ワールド行列
//! @param  [in]    shape       形状
[[nodiscard]] std::unique_ptr<physics::RigidBody> createRigidBody(f32 mass, const matrix& matWorld, const shape::Box& shape);

//! 球剛体を作成
//! @param  [in]    mass        質量(単位:kg)
//! @param  [in]    matWorld    初期姿勢ワールド行列
//! @param  [in]    shape       形状
[[nodiscard]] std::unique_ptr<physics::RigidBody> createRigidBody(f32 mass, const matrix& matWorld, const shape::Sphere& shape);

//! 円筒(シリンダー)剛体を作成
//! @param  [in]    mass        質量(単位:kg)
//! @param  [in]    matWorld    初期姿勢ワールド行列
//! @param  [in]    shape       形状
[[nodiscard]] std::unique_ptr<physics::RigidBody> createRigidBody(f32 mass, const matrix& matWorld, const shape::Cylinder& shape);

//! モデルメッシュで剛体を作成
//! @param  [in]    mass        質量(単位:kg)
//! @param  [in]    matWorld    初期姿勢ワールド行列
//! @param  [in]    model       モデルデーター
[[nodiscard]] std::unique_ptr<physics::RigidBody> createRigidBody(f32 mass, const matrix& matWorld, const raw_ptr<Model> model);

//@}

}   // namespace physics
