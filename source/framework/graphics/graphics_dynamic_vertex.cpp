﻿//---------------------------------------------------------------------------
//!	@file	graphics_dynamic_vertex.cpp
//!	@brief	動的な頂点バッファ管理
//---------------------------------------------------------------------------
#include "graphics_dynamic_vertex.h"

namespace graphics {

//===========================================================================
//! 動的な頂点バッファ管理
//===========================================================================
class DynamicVertexImpl final : public graphics::DynamicVertex
{
public:
    DynamicVertexImpl();

    virtual ~DynamicVertexImpl() = default;

    //! ユーザープリミティブ描画 (DirectX9のDrawPrimitiveUP相当)
    //! @param  [in]    type    プリミティブの種類
    //! @param  [in]    vertexCount 頂点数
    //! @param  [in]    vertices    頂点配列の先頭アドレス
    //! @param  [in]    stride      1頂点あたりのサイズ(0指定で入力レイアウトから自動設定)
    virtual void drawUserPrimitive(dx11::Primitive type, u32 vertexCount, const void* vertices, u32 stride) override;

    //! インデックスありユーザープリミティブ描画 (DirectX9のDrawIndexedPrimitiveUP相当)
    //! @param  [in]    type            プリミティブの種類
    //! @param  [in]    numVertices     頂点数
    //! @param  [in]    indexCount      インデックス数
    //! @param  [in]    indices         インデックス配列の先頭
    //! @param  [in]    vertices        頂点配列の先頭
    //! @param  [in]    stride          1頂点あたりのサイズ(0指定で入力レイアウトから自動設定)
    virtual void drawIndexedUserPrimitive(dx11::Primitive type, u32 numVertices, u32 indexCount, const void* indices, const void* vertices, u32 stride) override;

    //! 実体を取得
    static graphics::DynamicVertexImpl& instance()
    {
        static graphics::DynamicVertexImpl instance;   // シングルトン実体
        return instance;
    }

    //! 頂点の書き込み開始
    //! @param  [in]    size    確保したいサイズ
    //! @retval [0] 確保された先頭アドレス
    //! @retval [1] 先頭オフセット
    //! @retval [2] 確保されたサイズ
    std::tuple<void*, u32, size_t> beginVertices(size_t size)
    {
        ASSERT_MESSAGE(size <= bufferVb_->desc().size_, "頂点バッファ最大確保サイズを超えました。");

        D3D11_MAP mapType = D3D11_MAP_WRITE_NO_OVERWRITE;   // 追記モード

        // メモリ終端まで使用した場合は新しいメモリを再確保して先頭から書き込むように変更
        auto remainSizeVb = bufferVb_->desc().size_ - usedSizeVb_;   // 残りサイズ
        if(remainSizeVb < size) {
            mapType     = D3D11_MAP_WRITE_DISCARD;   // 再確保モード
            usedSizeVb_ = 0;                         // 先頭から
        }

        auto* p      = bufferVb_->map(mapType);         // バッファ先頭
        u32   offset = static_cast<u32>(usedSizeVb_);   // 確保されたオフセット位置

        // 使用中サイズを更新してオフセット位置を移動
        p = reinterpret_cast<void*>(reinterpret_cast<uintptr>(p) + offset);
        usedSizeVb_ += size;

        return std::make_tuple(p, offset, size);
    }

    //! 頂点の書き込み終了
    void endVertices()
    {
        bufferVb_->unmap();
    }

    //! インデックスの書き込み開始
    //! @param  [in]    size    確保したいサイズ
    //! @retval [0] 確保された先頭アドレス
    //! @retval [1] 先頭オフセット
    //! @retval [2] 確保されたサイズ
    std::tuple<void*, u32, size_t> beginIndices(size_t size)
    {
        ASSERT_MESSAGE(size <= bufferIb_->desc().size_, "インデックスバッファ最大確保サイズを超えました。");

        D3D11_MAP mapType = D3D11_MAP_WRITE_NO_OVERWRITE;   // 追記モード

        // メモリ終端まで使用した場合は新しいメモリを再確保して先頭から書き込むように変更
        auto remainSizeIb = bufferIb_->desc().size_ - usedSizeIb_;   // 残りサイズ
        if(remainSizeIb < size) {
            mapType     = D3D11_MAP_WRITE_DISCARD;   // 再確保モード
            usedSizeIb_ = 0;                         // 先頭から
        }

        auto* p      = bufferIb_->map(mapType);         // バッファ先頭
        u32   offset = static_cast<u32>(usedSizeIb_);   // 確保されたオフセット位置

        // 使用中サイズを更新してオフセット位置を移動
        p = reinterpret_cast<void*>(reinterpret_cast<uintptr>(p) + offset);
        usedSizeIb_ += size;

        return std::make_tuple(p, offset, std::min(size, size));
    }

    //! インデックスの書き込み終了
    void endIndices()
    {
        bufferIb_->unmap();
    }

private:
    // コピー禁止/move禁止
    DynamicVertexImpl(const DynamicVertexImpl&) = delete;
    DynamicVertexImpl(DynamicVertexImpl&&)      = delete;
    DynamicVertexImpl& operator=(const DynamicVertexImpl&) = delete;
    DynamicVertexImpl& operator=(DynamicVertexImpl&&) = delete;

private:
    std::shared_ptr<dx11::Buffer> bufferVb_;         //!< 頂点バッファ
    std::shared_ptr<dx11::Buffer> bufferIb_;         //!< インデックスバッファ
    uintptr                       usedSizeVb_ = 0;   //!< 使用中のバッファサイズ
    uintptr                       usedSizeIb_ = 0;   //!< 使用中のバッファサイズ
};

//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
DynamicVertexImpl::DynamicVertexImpl()
{
    constexpr u32 size = 16 * 1024 * 1024;   // 16MB

    // 動的バッファを作成(D3D11_USAGE_DYNAMIC)
    bufferVb_ = dx11::createBuffer({ size, D3D11_BIND_VERTEX_BUFFER, D3D11_USAGE_DYNAMIC });   // 頂点バッファ
    bufferIb_ = dx11::createBuffer({ size, D3D11_BIND_INDEX_BUFFER, D3D11_USAGE_DYNAMIC });    // インデックスバッファ
    if(!bufferVb_ && !bufferIb_) {
        return;
    }
}

//---------------------------------------------------------------------------
//! 頂点バッファに書き込み
//---------------------------------------------------------------------------
u32 writeToVertexBuffer(u32 vertexCount, const void* vertices, u32 stride)
{
    //------------------------------------------------------
    // メモリ確保して書き込み
    //------------------------------------------------------
    auto result        = DynamicVertexImpl::instance().beginVertices(vertexCount * stride);
    auto p             = std::get<0>(result);   // 確保された先頭アドレス
    auto offset        = std::get<1>(result);   // バッファ先頭からのオフセット位置
    auto allocatedSize = std::get<2>(result);   // 実際に確保されたサイズ

    memcpy_s(p, allocatedSize, vertices, allocatedSize);
    DynamicVertexImpl::instance().endVertices();

    return offset;
}

//---------------------------------------------------------------------------
//! インデックスバッファに書き込み
//---------------------------------------------------------------------------
u32 writeToIndexBuffer(u32 indexCount, const void* indices)
{
    //------------------------------------------------------
    // メモリ確保して書き込み
    //------------------------------------------------------
    auto result        = DynamicVertexImpl::instance().beginIndices(indexCount * sizeof(u32));
    auto p             = std::get<0>(result);   // 確保された先頭アドレス
    auto offset        = std::get<1>(result);   // バッファ先頭からのオフセット位置
    auto allocatedSize = std::get<2>(result);   // 実際に確保されたサイズ

    memcpy_s(p, allocatedSize, indices, allocatedSize);
    DynamicVertexImpl::instance().endIndices();

    return offset;
}

//---------------------------------------------------------------------------
//! ユーザープリミティブ描画 (DirectX9のDrawPrimitiveUP相当)
//---------------------------------------------------------------------------
void DynamicVertexImpl::drawUserPrimitive(dx11::Primitive type, u32 vertexCount, const void* vertices, u32 vertexStride)
{
    if(vertexStride == 0) {
        vertexStride = dx11::getInputLayout()->vertexStride();   // 1頂点あたりのサイズ
    }

    //----------------------------------------------------------
    // 設定の退避
    //----------------------------------------------------------
    com_ptr<ID3D11Buffer>    d3dBufferLastVb;
    u32                      strideLastVb;
    u32                      offsetLastVb;
    D3D11_PRIMITIVE_TOPOLOGY d3dTopologyLast;
    dx11::context()->IAGetVertexBuffers(0, 1, &d3dBufferLastVb, &strideLastVb, &offsetLastVb);
    dx11::context()->IAGetPrimitiveTopology(&d3dTopologyLast);

    //------------------------------------------------------
    // メモリ確保して書き込み
    //------------------------------------------------------
    auto offsetVb = writeToVertexBuffer(vertexCount, vertices, vertexStride);

    //------------------------------------------------------
    // 描画発行
    //------------------------------------------------------
    ID3D11Buffer* d3dBuffersVb[] = { *bufferVb_ };

    dx11::context()->IASetVertexBuffers(0, 1, d3dBuffersVb, &vertexStride, &offsetVb);
    dx11::context()->IASetPrimitiveTopology(dx11::makePrimitiveTopology(type));
    dx11::context()->Draw(vertexCount, 0);

    //----------------------------------------------------------
    // 退避した設定を元に戻す
    //----------------------------------------------------------
    ID3D11Buffer* d3dBuffer = d3dBufferLastVb.Get();
    dx11::context()->IASetVertexBuffers(0, 1, &d3dBuffer, &strideLastVb, &offsetLastVb);
    dx11::context()->IASetPrimitiveTopology(d3dTopologyLast);
}

//---------------------------------------------------------------------------
//! インデックスありユーザープリミティブ描画 (DirectX9のDrawIndexedPrimitiveUP相当)
//---------------------------------------------------------------------------
void DynamicVertexImpl::drawIndexedUserPrimitive(dx11::Primitive type, u32 vertexCount, u32 indexCount, const void* indices, const void* vertices, u32 vertexStride)
{
    if(vertexStride == 0) {
        vertexStride = dx11::getInputLayout()->vertexStride();   // 1頂点あたりのサイズ
    }

    //----------------------------------------------------------
    // 設定の退避
    //----------------------------------------------------------
    com_ptr<ID3D11Buffer>    d3dBufferLastVb;
    u32                      strideLastVb;
    u32                      offsetLastVb;
    com_ptr<ID3D11Buffer>    d3dBufferLastIb;
    DXGI_FORMAT              formatLastIb;
    u32                      offsetLastIb;
    D3D11_PRIMITIVE_TOPOLOGY d3dTopologyLast;
    dx11::context()->IAGetVertexBuffers(0, 1, &d3dBufferLastVb, &strideLastVb, &offsetLastVb);
    dx11::context()->IAGetIndexBuffer(&d3dBufferLastIb, &formatLastIb, &offsetLastIb);
    dx11::context()->IAGetPrimitiveTopology(&d3dTopologyLast);

    //------------------------------------------------------
    // メモリ確保して書き込み
    //------------------------------------------------------
    auto offsetVb = writeToVertexBuffer(vertexCount, vertices, vertexStride);
    auto offsetIb = writeToIndexBuffer(indexCount, indices);

    //------------------------------------------------------
    // 描画発行
    //------------------------------------------------------
    ID3D11Buffer* d3dBuffersVb[] = { *bufferVb_ };

    dx11::context()->IASetVertexBuffers(0, 1, d3dBuffersVb, &vertexStride, &offsetVb);
    dx11::context()->IASetIndexBuffer(*bufferIb_, DXGI_FORMAT_R32_UINT, offsetIb);
    dx11::context()->IASetPrimitiveTopology(dx11::makePrimitiveTopology(type));
    dx11::context()->DrawIndexed(indexCount, 0, 0);

    //----------------------------------------------------------
    // 退避した設定を元に戻す
    //----------------------------------------------------------
    ID3D11Buffer* d3dBuffer = d3dBufferLastVb.Get();
    dx11::context()->IASetVertexBuffers(0, 1, &d3dBuffer, &strideLastVb, &offsetLastVb);
    dx11::context()->IASetIndexBuffer(d3dBufferLastIb.Get(), formatLastIb, offsetLastIb);
    dx11::context()->IASetPrimitiveTopology(d3dTopologyLast);
}

//---------------------------------------------------------------------------
//! シングルトンオブジェクトを取得
//---------------------------------------------------------------------------
DynamicVertex& DynamicVertex::instance()
{
    return DynamicVertexImpl::instance();
}

}   // namespace graphics
