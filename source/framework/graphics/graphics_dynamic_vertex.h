﻿//---------------------------------------------------------------------------
//!	@file	graphics_dynamic_vertex.h
//!	@brief	動的な頂点バッファ管理
//---------------------------------------------------------------------------
#pragma once

namespace graphics {

//===========================================================================
//! 動的な頂点バッファ管理
//! シングルトン
//===========================================================================
class DynamicVertex
{
public:
    //! ユーザープリミティブ描画 (DirectX9のDrawPrimitiveUP相当)
    //! @param  [in]    type        プリミティブの種類
    //! @param  [in]    vertexCount     頂点数
    //! @param  [in]    vertices        頂点配列の先頭アドレス
    //! @param  [in]    vertexStride    1頂点あたりのサイズ(0指定で入力レイアウトから自動設定)
    virtual void drawUserPrimitive(dx11::Primitive type, u32 vertexCount, const void* vertices, u32 vertexStride = 0) = 0;

    //! インデックスありユーザープリミティブ描画 (DirectX9のDrawIndexedPrimitiveUP相当)
    //! @param  [in]    type            プリミティブの種類
    //! @param  [in]    vertexCount     頂点数
    //! @param  [in]    indexCount      インデックス数
    //! @param  [in]    indices         インデックス配列の先頭
    //! @param  [in]    vertices        頂点配列の先頭
    //! @param  [in]    vertexStride    1頂点あたりのサイズ(0指定で入力レイアウトから自動設定)
    virtual void drawIndexedUserPrimitive(dx11::Primitive type, u32 vertexCount, u32 indexCount, const void* indices, const void* vertices, u32 vertexStride = 0) = 0;

    //! シングルトンオブジェクトを取得
    static DynamicVertex& instance();

protected:
    virtual ~DynamicVertex() = default;
};

}   // namespace graphics
