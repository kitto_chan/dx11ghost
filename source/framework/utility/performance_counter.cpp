﻿//---------------------------------------------------------------------------
//!	@file	performance_counter.cpp
//!	@brief	パフォーマンスカウンター
//---------------------------------------------------------------------------
#include "performance_counter.h"

namespace performance_counter {

//---------------------------------------------------------------------------
//! 現在のパフォーマンスカウンター値を取得
//---------------------------------------------------------------------------
u64 now()
{
    LARGE_INTEGER integer;
    QueryPerformanceCounter(&integer);
    return integer.QuadPart;
}

//---------------------------------------------------------------------------
//! パフォーマンスカウンターの1秒あたりの値を取得
//---------------------------------------------------------------------------
u64 frequency()
{
    LARGE_INTEGER integer;
    QueryPerformanceFrequency(&integer);
    return integer.QuadPart;
}

}   // namespace performance_counter