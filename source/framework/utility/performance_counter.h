﻿//---------------------------------------------------------------------------
//!	@file	performance_counter.h
//!	@brief	パフォーマンスカウンター
//---------------------------------------------------------------------------
#pragma once

namespace performance_counter {

//! 現在のパフォーマンスカウンター値を取得
u64 now();

//! パフォーマンスカウンターの1秒あたりの値を取得
//! @note 単位を秒に変換する場合は now()÷frequency() で計算可能
u64 frequency();

}   // namespace performance_counter