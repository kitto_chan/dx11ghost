﻿//---------------------------------------------------------------------------
//!	@file	SystemSettings.h
//!	@brief	ゲーム設定
//---------------------------------------------------------------------------
#pragma once
namespace manager {
using namespace sys;
class SystemSettings : public Singleton<SystemSettings>
{
public:
    SystemSettings()  = default;   //!< コンストラクタ
    ~SystemSettings() = default;   //!< デストラクタ

    //---------------------------------------------------------------------------
    // 関数
    //---------------------------------------------------------------------------

    //---------
    //! Imguimoのアクション
    ImGuizmoAction GetImGuizmoAction() const;                      //!< Imguimoのアクションを取得
    void           SetImGuizmoAction(const ImGuizmoAction& set);   //!< Imguimoのアクションを設定

    //---------
    // システムのモード
    SystemMode GetSysMode() const;     //! SystemModeを取得
    void       SwapMode();             //! SystemModeを切り替えする EditorMode <-> GameMode
    bool       IsEditorMode() const;   //!<  EditorModeかどうか

    //----------
    // システムのステート
    SystemState GetSystemState() const;   //!< システムステート
    void        SwapSysState();           //!< システムステートを切り替える Play <->Pause
    bool        IsPlayState() const;      //!<  再生ステートかどうか
    bool        IsPauseState() const;     //!<  ポーズステートかどうか

    //----------
    // ブレットエンジン関して
    bool IsPhyDebug() const;   //!< 物理デバッグ表示すかどうか
    void SwapPhyDebug();       //!< 物理デバッグ描画フラッグ true <-> false swap

    //----------
    // Imgui
    bool IsUseImguiDemo() const;   //!< _isUseImguiDemoフラッグ
    void SwapImguiDemoFlag();      //!< Imguiデモ表示フラッグ true <-> false swap
private:
    //---------------------------------------------------------------------------
    //! 内部変数
    //---------------------------------------------------------------------------
    ImGuizmoAction _imguizmoAction = ImGuizmoAction::Transform;   //!< Imguimoのアクション
    SystemMode     _sysMode        = SystemMode::GameMode;        //!< システムのモード
    SystemState    _sysState       = SystemState::Play;           //!< システムのステート

    bool _isPhyDebug   = false;   //<! ブレットエンジンデバッグ描画
    bool _useImguiDemo = false;   //<! ImGuiDemo表示かどうか
};

}   // namespace manager
manager::SystemSettings* SystemSettingsMgr();
