﻿//---------------------------------------------------------------------------
//!	@file	StructDef.h
//!	@brief	通用構造体定義
//---------------------------------------------------------------------------
#pragma once
namespace cb {
//! カメラ用定数バッファ
struct CameraCB
{
    matrix matView = math::identity();   //!< ビュー行列
    matrix matProj = math::identity();   //!< 投影行列
};

struct MeshColorCB
{
    float4 color = math::ONE;
};

}   // namespace def
