﻿//---------------------------------------------------------------------------
//!	@file	enumDef.h
//!	@brief	型定義
//---------------------------------------------------------------------------

// システム関して
namespace sys {
// モード
enum class SystemMode
{
    EditorMode,   //!< クリエイトモード（開発用）
    GameMode      //!< ゲームモード    　(クライアント側)
};

// システムステート
enum class SystemState
{
    Play,   //!< 更新を行う
    Pause   //!< 更新しない
};

// ImGuizmoのアクション
enum class ImGuizmoAction
{
    Transform,   //!< 変換座標
    Rotate,      //!< 回転
    Scale        //!< スケール
};

}   // namespace sys
