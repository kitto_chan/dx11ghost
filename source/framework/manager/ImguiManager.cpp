﻿//---------------------------------------------------------------------------
//!	@file	ImguiManager.cpp
//!	@brief	Imguiマネジャー
//---------------------------------------------------------------------------
#include <imgui/imgui_impl_win32.h>
#include <imgui/imgui_impl_dx11.h>
#include "ImguiManager.h"

namespace manager {
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
ImguiManager::ImguiManager()
{
}

//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
ImguiManager::~ImguiManager()
{
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool ImguiManager::Init()
{
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO();
    io.Fonts->AddFontFromFileTTF("Font/Kosugi_Maru/KosugiMaru-Regular.ttf", 16.0f, nullptr, io.Fonts->GetGlyphRangesJapanese());

    ImGui::StyleColorsDark();
    ImGui_ImplWin32_Init(application::hwnd());
    ImGui_ImplDX11_Init(dx11::d3dDevice(), dx11::immediateContext());

    InitSettings();
    return true;
}

//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void ImguiManager::Update()
{

    ImGui_ImplDX11_NewFrame();
    ImGui_ImplWin32_NewFrame();
    ImGui::NewFrame();
}

//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void ImguiManager::Render()
{
    ImGui::EndFrame();
    ImGui::Render();
    ImGui_ImplDX11_RenderDrawData(ImGui::GetDrawData());
}

//---------------------------------------------------------------------------
//! 解放
//---------------------------------------------------------------------------
void ImguiManager::Finalize()
{
    ImGui_ImplDX11_Shutdown();
    ImGui_ImplWin32_Shutdown();
    ImGui::DestroyContext();
}

void ImguiManager::SetComponentWindow()
{
    _componentWindowSettings.Apply();
}

void ImguiManager::InitSettings()
{
    _componentWindowSettings._size.x = sysst::WND_W * 0.25f;
    _componentWindowSettings._size.y = sysst::WND_H;
    
    _componentWindowSettings._pos.x = sysst::WND_W * 0.75f;
    _componentWindowSettings._pos.y = 0.0f;
}

//---------------------------------------------------------------------------
//! ゲーム管理クラスを取得
//---------------------------------------------------------------------------
manager::ImguiManager* ImguiMgr()
{
    return ImguiManager::Instance();
}

}   // namespace manager