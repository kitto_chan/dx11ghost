﻿//---------------------------------------------------------------------------
//!	@file	ImguiManager.h
//!	@brief	Imguiマネジャー
//---------------------------------------------------------------------------
#pragma once
namespace manager {

class ImguiManager : public Singleton<ImguiManager>
{
    struct ImguiWindowSetting
    {
        ImVec2 _size;
        ImVec2 _pos;

        void Apply() {
            ImGui::SetWindowSize(_size);
            ImGui::SetWindowPos(_pos);
        }
    };

public:
    ImguiManager();
    ~ImguiManager();

    //---------------------------------------------------------------------------
    //! 関数
    //---------------------------------------------------------------------------
    bool Init();       //! 初期化
    void Update();     //! 更新
    void Render();     //! 描画
    void Finalize();   //! 解放

    void SetComponentWindow();
private:
    void InitSettings(); //! 初期設定
    //---------------------------------------------------------------------------
    //! 内部変数
    //---------------------------------------------------------------------------
    ImguiWindowSetting _componentWindowSettings;
};

//! ゲーム管理クラスを取得
manager::ImguiManager* ImguiMgr();

}   // namespace manager