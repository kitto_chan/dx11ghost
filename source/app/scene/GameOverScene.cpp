﻿//---------------------------------------------------------------------------
//!	@file	GameOverScene.h
//!	@brief	ゲームチュートリアル
//---------------------------------------------------------------------------

#include "GameOverScene.h"
#include "manager/SceneManager.h"
#include "manager/CameraManager.h"

#include "entity/gameObject/character/Ghost.h"
#include "component/Transform.h"

#include "effect/Effect.h"
namespace scene {
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
GameOverScene::GameOverScene()
{
}

//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
GameOverScene::~GameOverScene()
{
    // TODO: リファクタリング
    // 文字クリア
    render::FontRendererIns()->Clear();
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool GameOverScene::OnInit()
{
    auto* backBuffer   = gpu::swapChain()->backBuffer();
    f32   screenWidth  = static_cast<f32>(backBuffer->desc().width_);
    f32   screenHeight = static_cast<f32>(backBuffer->desc().height_);
    f32   centerWidth  = static_cast<f32>(backBuffer->desc().width_) / 2.0f;   //　画面中心点を求める
    {
        std::wstring     word = L"Game Over";
        render::FontDesc fontDesc(word.c_str(), float2(centerWidth, 200), float4(1.0f, 0.0f, 0.0f, 1.0f), 3.0f);
        render::FontRendererIns()->SetFont("Over", fontDesc);
    }

    {
        std::wstringstream wss;
        wss << L"のこり: " << manager::GameMgr()->score;
        std::wstring     gameOverMsg = L"LastScore";
        render::FontDesc overFont    = render::FontDesc(wss.str(),
                                                     float2(screenWidth * 0.5f, screenHeight * 0.5f),
                                                     float4(1.0f, 1.0f, 1.0f, 1.0f),
                                                     1.0f);
        render::FontRendererIns()->SetFont("LastScore", overFont);
    }

    return true;
}
//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void GameOverScene::OnUpdate()
{
    // Push Push Space Key の文字　アニメション
    {
        auto*            backBuffer = gpu::swapChain()->backBuffer();
        f32              width      = static_cast<f32>(backBuffer->desc().width_) / 2.0f;
        std::wstring     push       = L"Press 'R' To Try Again";
        f32              scale      = 0.4f + abs(sin(timer::TimerIns()->TotalTime())) / 3.0f;
        render::FontDesc fontDesc(push.c_str(), float2(width, 700), WHITE, scale);
        render::FontRendererIns()->SetFont("Push", fontDesc);
    }

    // スペースキーを押したら、ゲームシーンに切り替えます
    if(KeyboardMgr()->IsKeyPress(DirectX::Keyboard::R)) {
        manager::SetNextScene(manager::eScene::Game);
    }
}
//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void GameOverScene::OnRender(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture)
{
    gpu::setRenderTarget(0, colorTexture);
    gpu::setDepthStencil(depthTexture);

    // カラーとデプスバッファをクリア
    gpu::clearColor(colorTexture, float4(0.0f, 0.0f, 0.0f, 1.0f));   // カラーバッファ
    gpu::clearDepth(depthTexture, 1.0f);                             // デプスバッファ

    // ビューポート設定
    {
        auto*                   backBuffer = gpu::swapChain()->backBuffer();
        auto*                   context    = gpu::context();
        ID3D11RenderTargetView* rtv        = *colorTexture;
        context->OMSetRenderTargets(1, &rtv, nullptr);

        D3D11_VIEWPORT viewport;
        viewport.TopLeftX = 0;
        viewport.TopLeftY = 0;
        viewport.Width    = static_cast<f32>(backBuffer->desc().width_);
        viewport.Height   = static_cast<f32>(backBuffer->desc().height_);
        viewport.MinDepth = 0.0f;
        viewport.MaxDepth = 1.0f;
        context->RSSetViewports(1, &viewport);
    }
}
//---------------------------------------------------------------------------
//! Imgui描画
//---------------------------------------------------------------------------
void GameOverScene::OnRenderImgui()
{
}
//---------------------------------------------------------------------------
//! 解放
//---------------------------------------------------------------------------
void GameOverScene::OnFinalize()
{
}

//---------------------------------------------------------------------------
//! シーンの生成
//! @note テンプレートの特殊化で実装します
//---------------------------------------------------------------------------
template<>
std::unique_ptr<BaseScene> createNewScene<GameOverScene>()
{
    return std::make_unique<GameOverScene>();
}

}   // namespace scene
