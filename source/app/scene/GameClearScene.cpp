﻿//---------------------------------------------------------------------------
//!	@file	TutorialScene.h
//!	@brief	ゲームチュートリアル
//---------------------------------------------------------------------------

#include "GameClearScene.h"
#include "manager/SceneManager.h"
#include "manager/CameraManager.h"

#include "entity/gameObject/character/Ghost.h"
#include "component/Transform.h"

#include "effect/Effect.h"
namespace scene {
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
GameClearScene::GameClearScene()
{
}

//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
GameClearScene::~GameClearScene()
{
    // TODO: リファクタリング
    // 文字クリア
    render::FontRendererIns()->Clear();
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool GameClearScene::OnInit()
{
    auto* backBuffer  = gpu::swapChain()->backBuffer();
    f32   centerWidth = static_cast<f32>(backBuffer->desc().width_) / 2.0f;   //　画面中心点を求める

    {
        std::wstring     word = L"Game Clear";
        render::FontDesc fontDesc(word.c_str(), float2(centerWidth, 200), float4(0.0f,1.0f,0.0f,1.0f), 2.0f);
        render::FontRendererIns()->SetFont("Controll", fontDesc);
    }

    {
        std::wstring     word = L"おめでとう！";
        render::FontDesc fontDesc(word.c_str(), float2(centerWidth, 400), WHITE, 1.3f);
        render::FontRendererIns()->SetFont("Message", fontDesc);
    }

    return true;
}
//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void GameClearScene::OnUpdate()
{
    // Push Push Space Key の文字　アニメション
    {
        auto*            backBuffer = gpu::swapChain()->backBuffer();
        f32              width      = static_cast<f32>(backBuffer->desc().width_) / 2.0f;
        std::wstring     push       = L"Press 'R' To Try Again";
        f32              scale      = 0.4f + abs(sin(timer::TimerIns()->TotalTime())) / 3.0f;
        render::FontDesc fontDesc(push.c_str(), float2(width, 700), WHITE, scale);
        render::FontRendererIns()->SetFont("Push", fontDesc);
    }

    // スペースキーを押したら、ゲームシーンに切り替えます
    if(KeyboardMgr()->IsKeyPress(DirectX::Keyboard::R)) {
        manager::SetNextScene(manager::eScene::Game);
    }
}
//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void GameClearScene::OnRender(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture)
{
    gpu::setRenderTarget(0, colorTexture);
    gpu::setDepthStencil(depthTexture);

    // カラーとデプスバッファをクリア
    gpu::clearColor(colorTexture, float4(0.0f, 0.0f, 0.0f, 1.0f));   // カラーバッファ
    gpu::clearDepth(depthTexture, 1.0f);                             // デプスバッファ

    // ビューポート設定
    {
        auto*                   backBuffer = gpu::swapChain()->backBuffer();
        auto*                   context    = gpu::context();
        ID3D11RenderTargetView* rtv        = *colorTexture;
        context->OMSetRenderTargets(1, &rtv, nullptr);

        D3D11_VIEWPORT viewport;
        viewport.TopLeftX = 0;
        viewport.TopLeftY = 0;
        viewport.Width    = static_cast<f32>(backBuffer->desc().width_);
        viewport.Height   = static_cast<f32>(backBuffer->desc().height_);
        viewport.MinDepth = 0.0f;
        viewport.MaxDepth = 1.0f;
        context->RSSetViewports(1, &viewport);
    }
}
//---------------------------------------------------------------------------
//! Imgui描画
//---------------------------------------------------------------------------
void GameClearScene::OnRenderImgui()
{
}
//---------------------------------------------------------------------------
//! 解放
//---------------------------------------------------------------------------
void GameClearScene::OnFinalize()
{
}

//---------------------------------------------------------------------------
//! シーンの生成
//! @note テンプレートの特殊化で実装します
//---------------------------------------------------------------------------
template<>
std::unique_ptr<BaseScene> createNewScene<GameClearScene>()
{
    return std::make_unique<GameClearScene>();
}

}   // namespace scene
