﻿//---------------------------------------------------------------------------
//!	@file	BaseScene.h
//!	@brief	シーンの基底クラス
//---------------------------------------------------------------------------
#pragma once
#include "IScene.h"
#include "Entity/Entity.h"

namespace entity::go {
};
namespace gameobject = entity::go;

namespace manager {
class EntityManager;
class CameraManager;
}

namespace scene {
class BaseScene : IScene
{
public:
    BaseScene();
    virtual ~BaseScene();

    //---------------------------------------------------------------------------
    //! 関数
    //---------------------------------------------------------------------------

    //! 初期化
    virtual bool Init() override;

    //! 更新
    virtual void Update(f32 deltaTime) override;

    //! 描画
    virtual void Render(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture) override;

    //! Imgui描画
    virtual void RenderImgui(raw_ptr<gpu::Texture> colorTexture) override;

    //! 解放
    virtual void Finalize() override;

    raw_ptr<manager::EntityManager> GetEntityMgr();

protected:
    uni_ptr<manager::EntityManager> _pEntityMgr; //!< ゲームシーンのアクター（役者）たち
    uni_ptr<manager::CameraManager> _pCameraMgr; //!< カメラマネージャー
    //===========================================================================
    //!	@defgroup	継承用関数
    //===========================================================================
    //@{

    //!　初期化
    virtual bool OnInit() = 0;

    //! アップデート
    virtual void OnUpdate() = 0;

    //! 描画
    virtual void OnRender(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture) = 0;

    //! Imgui描画
    virtual void OnRenderImgui() = 0;

    //! 解放
    virtual void OnFinalize() = 0;

    //@}
private:
};
}   // namespace scene
