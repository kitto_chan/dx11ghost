﻿//---------------------------------------------------------------------------
//!	@file	TitleScene.h
//!	@brief	ゲームタイトル画面
//---------------------------------------------------------------------------
#pragma once
#include "BaseScene.h"

namespace scene {
class TitleScene final : public BaseScene
{
public:
    TitleScene();    //! コンストラクター
    ~TitleScene();   //! デストラクター
    //---------------------------------------------------------------------------
    // public 関数
    //---------------------------------------------------------------------------
protected:
private:
    //---------------------------------------------------------------------------
    // private 関数
    //---------------------------------------------------------------------------

    //----------
    // 継承
    bool OnInit() override;                                                                           //!< 初期化
    void OnUpdate() override;                                                                         //!< 更新
    void OnRender(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture) override;   //!< 描画
    void OnRenderImgui() override;                                                                    //!< Imgui描画
    void OnFinalize() override;                                                                       //!< 解放
};
}   // namespace scene
