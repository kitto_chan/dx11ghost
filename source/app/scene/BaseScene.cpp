﻿//---------------------------------------------------------------------------
//!	@file	BaseScene.cpp
//!	@brief	ゲームマネージャー
//---------------------------------------------------------------------------
#include "BaseScene.h"
#include "manager/EntityManager.h"
#include "manager/CameraManager.h"

#include "entity/Entity.h"
#include "entity/gameObject/camera/FirstPersonCamera.h"

#include "component/Transform.h"
namespace scene {
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
BaseScene::BaseScene()
{
    _pEntityMgr = std::make_unique<manager::EntityManager>();
    _pCameraMgr = std::make_unique<manager::CameraManager>();
}

//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
BaseScene::~BaseScene()
{
    Finalize();
    _pEntityMgr.reset();
    _pCameraMgr.reset();
    // 文字クリア
    render::FontRendererIns()->Clear();
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool BaseScene::Init()
{
    // 文字レンダー初期化
    render::FontRendererIns()->Init();

	// カメラの初期化
    _pCameraMgr->Init();

    // 各シーンの初期化
    OnInit();

    // エンティティ　初期化
    if(!_pEntityMgr->Init()) {
        return false;
    };

    return true;
}

//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void BaseScene::Update([[maybe_unused]] f32 deltaTime)
{
    if(KeyboardMgr()->IsKeyPress(DirectX::Keyboard::F1)) {
        SystemSettingsMgr()->SwapMode();
        _pCameraMgr->SyncDebugCameraSceneCamera();
    }

    if(SystemSettingsMgr()->IsPlayState()) {
        _pEntityMgr->Update();
        OnUpdate();
    }

    _pCameraMgr->Update();
}

//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void BaseScene::Render(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture)
{
    _pCameraMgr->Render();

    OnRender(colorTexture, depthTexture);

    _pEntityMgr->Render();

    render::FontRendererIns()->Render();
}

//---------------------------------------------------------------------------
//! Imguiの描画
//---------------------------------------------------------------------------
void BaseScene::RenderImgui(raw_ptr<gpu::Texture> colorTexture)
{
    // メニューバー描画
    imgui::RenderMenuBar();

    if(_pEntityMgr->GetSelectedEntities()) {
        imgui::ImguiGuizmoView(_pCameraMgr->GetCurrentCamera()->GetViewMatrix(),
                               _pCameraMgr->GetCurrentCamera()->GetProjMatrix(),
                               _pEntityMgr->GetSelectedEntities()->GetComponent<component::Transform>());
    }

    OnRenderImgui();

    _pEntityMgr->RenderImgui();

    imgui::ImguiActionBar();

    if(SystemSettingsMgr()->IsEditorMode()) {
        ImGui::Begin("Scene");
        ImVec2                    contentSize = ImGui::GetContentRegionAvail();   // ウインドウズの"中身"のサイズ
        ID3D11ShaderResourceView* srv         = *colorTexture;                    // テクスチャへのレンダー（Render-To-Texture）
        ImGui::Image((void*)srv, contentSize);                                    // 今画面のテクスチャーImgui DockSpaceに設置
        ImGui::End();
    }
}

//---------------------------------------------------------------------------
//! 解放
//---------------------------------------------------------------------------
void BaseScene::Finalize()
{
    //OnFinalize();

    _pEntityMgr->Finalize();
}
//---------------------------------------------------------------------------
//! entity managerを取得
//---------------------------------------------------------------------------
raw_ptr<manager::EntityManager> BaseScene::GetEntityMgr()
{
    return _pEntityMgr.get();
}
}   // namespace scene
