﻿//---------------------------------------------------------------------------
//!	@file	TitleScene.h
//!	@brief	ゲームタイトル画面
//---------------------------------------------------------------------------

#include "TitleScene.h"
#include "manager/SceneManager.h"
#include "manager/CameraManager.h"

#include "entity/gameObject/character/Ghost.h"
#include "component/Transform.h"

#include "effect/Effect.h"
namespace scene {
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
TitleScene::TitleScene()
{
}

//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
TitleScene::~TitleScene()
{
    // TODO: リファクタリング
    // 文字クリア
    render::FontRendererIns()->Clear();
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool TitleScene::OnInit()
{
    // エフェクトの初期化
    effect::BasicEffectIns()->InitAll();
    effect::BasicEffectIns()->SetFogState(false);

	_pCameraMgr->GetCurrentCamera()->GetComponent<component::Transform>()->SetPosition(float3(0.0f,1.5f, -3.0f));

    {
        gameobject::Ghost* ghost = new gameobject::Ghost(float3(0.0f, 0.0f, 0.0f));
        ghost->SetGhostColor(float4(0.8, 0.6f, 0.6f, 0.8f));
        ghost->SetToCleanness();
        ghost->GetComponent<component::Transform>()->SetScale(float3(0.2f, 0.2f, 0.2f));
        ghost->GetComponent<component::Transform>()->LookAt(float3(0.0f, 1.0f, -3.0f));
        _pEntityMgr->Regist(ghost);
    }
    {
        gameobject::Ghost* ghost = new gameobject::Ghost(float3(1.5f, 0.0f, 0.0f));
        ghost->SetToCleanness();
        ghost->SetGhostColor(float4(0.6, 0.8f, 0.6f, 0.8f));
        ghost->GetComponent<component::Transform>()->SetScale(float3(0.2f, 0.2f, 0.2f));
        ghost->GetComponent<component::Transform>()->LookAt(float3(0.0f, 1.0f, -3.0f));
        _pEntityMgr->Regist(ghost);
    }
    {
        gameobject::Ghost* ghost = new gameobject::Ghost(float3(-1.5f, 0.0f, 0.0f));
        ghost->SetToCleanness();
        ghost->SetGhostColor(float4(0.6, 0.6f, 0.8f, 0.8f));
        ghost->GetComponent<component::Transform>()->SetScale(float3(0.2f, 0.2f, 0.2f));
        ghost->GetComponent<component::Transform>()->LookAt(float3(0.0f, 1.0f, -3.0f));
        _pEntityMgr->Regist(ghost);
    }

    auto* backBuffer  = gpu::swapChain()->backBuffer();
    f32   centerWidth = static_cast<f32>(backBuffer->desc().width_) / 2.0f;   //　画面中心点を求める

    // タイトル文字描画設定
    {
        std::wstring     title = L"ゴースト";
        render::FontDesc fontDesc(title.c_str(), float2(centerWidth, 200), WHITE, 3.0f);
        render::FontRendererIns()->SetFont("Title", fontDesc);
    }

    return true;
}
//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void TitleScene::OnUpdate()
{
    // Push Push Space Key の文字　アニメション
    {
        auto*            backBuffer = gpu::swapChain()->backBuffer();
        f32              width      = static_cast<f32>(backBuffer->desc().width_) / 2.0f;
        std::wstring     push       = L"Press Space Key To Continue";
        f32              scale      = 0.4f + abs(sin(timer::TimerIns()->TotalTime())) / 3.0f;
        render::FontDesc fontDesc(push.c_str(), float2(width, 700), WHITE, scale);
        render::FontRendererIns()->SetFont("Push", fontDesc);
    }

    // スペースキーを押したら、ゲームシーンに切り替えます
    if(KeyboardMgr()->IsKeyPress(DirectX::Keyboard::Space)) {
        manager::SetNextScene(manager::eScene::Tutorial);
    }
}
//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void TitleScene::OnRender(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture)
{
    gpu::setRenderTarget(0, colorTexture);
    gpu::setDepthStencil(depthTexture);

    // カラーとデプスバッファをクリア
    gpu::clearColor(colorTexture, float4(0.0f, 0.0f, 0.0f, 1.0f));   // カラーバッファ
    gpu::clearDepth(depthTexture, 1.0f);                             // デプスバッファ
}
//---------------------------------------------------------------------------
//! Imgui描画
//---------------------------------------------------------------------------
void TitleScene::OnRenderImgui()
{
}
//---------------------------------------------------------------------------
//! 解放
//---------------------------------------------------------------------------
void TitleScene::OnFinalize()
{
}

//---------------------------------------------------------------------------
//! シーンの生成
//! @note テンプレートの特殊化で実装します
//---------------------------------------------------------------------------
template<>
std::unique_ptr<BaseScene> createNewScene<TitleScene>()
{
    return std::make_unique<TitleScene>();
}

}   // namespace scene
