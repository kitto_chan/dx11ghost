﻿//---------------------------------------------------------------------------
//!	@file	GameScene.h
//!	@brief	ゲームシーン
//---------------------------------------------------------------------------
#include "GameScene.h"
#include <iomanip>

#include "manager/SceneManager.h"
#include "manager/EntityManager.h"
#include "manager/CameraManager.h"
#include "manager/CubeManager.h"

// エフェクト
#include "effect/Effect.h"
#include "effect/skyBox/StaticSkyBoxEffect.h"

// エンティティ
#include "entity/skybox/StaticSkyBox.h"
#include "entity/gameObject/Camera/ThirdPersonCamera.h"
#include "entity/gameObject/Character/Player.h"
#include "entity/gameObject/Cube.h"

#include "component/Transform.h"
namespace scene {
namespace {
cb::CameraCB _cameraCB{};   //!< カメラ用定数バッファ

uni_ptr<entity::StaticSkyBox> _pCubeMap;   //!< スカイボックス

uni_ptr<manager::CubeManager> cubeMgr;   //!< スカイボックス

// ゲームオブジェクト
gameobject::ThirdPersonCamera* TPCamera;   //!< カメラ
gameobject::Player*            player;     //!< プレイヤー
}   // namespace
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
GameScene::GameScene()
{
}

//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
GameScene::~GameScene()
{
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool GameScene::OnInit()
{
    if(SystemSettingsMgr()->GetSysMode() == sys::SystemMode::GameMode) {
        MouseMgr()->SetRelativeMode();
	}
    // エフェクトの初期化
    effect::BasicEffectIns()->InitAll();
    effect::StaticSkyBoxEffectIns()->InitAll();
    effect::BasicEffectIns()->SetFogState(false);

    // スカイボックス
    _pCubeMap = std::make_unique<entity::StaticSkyBox>();
    _pCubeMap->ReadFile("gameSource/skyBox/Space.dds");
    _pCubeMap->Init();

    // プレイヤー
    player = new gameobject::Player();
    _pEntityMgr->Regist(player);

    // TPカメラ
    TPCamera = new gameobject::ThirdPersonCamera();
    TPCamera->SetTarget(player);
    _pCameraMgr->AddCamera(TPCamera, true);
    _pEntityMgr->Regist(TPCamera);

    // Game Logic
    cubeMgr = std::make_unique<manager::CubeManager>();
    cubeMgr->Init(_pEntityMgr);

    return true;
}
//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void GameScene::OnUpdate()
{
    // 勝利判定
    if(cubeMgr->GetScore() <= 0) {
        manager::SetNextScene(manager::eScene::Clear);
        return;
    }

    // 敗北判定
    if(player->IsDead()) {
        manager::GameMgr()->score = cubeMgr->GetScore();
        manager::SetNextScene(manager::eScene::Over);
        return;
    }

    // 画面の右上スコア
    {
        auto* backBuffer  = gpu::swapChain()->backBuffer();
        f32   screenWidth = static_cast<f32>(backBuffer->desc().width_);
        f32   drawXPos    = screenWidth * 0.9f;   //　画面中心点を求める

        std::wstringstream wss;
        wss << L"のこり: " << std::setw(2) << cubeMgr->GetScore();
        render::FontDesc fontDesc = render::FontDesc(wss.str(), float2(drawXPos, 50.0f), WHITE, 0.7f);
        render::FontRendererIns()->SetFont("Remaining", fontDesc);
    }

    // 表裏カリングOFF
    gpu::setRasterizerState(gpu::commonStates().CullNone());

    // カメラバッファ更新
    _cameraCB.matView = _pCameraMgr->GetCurrentCamera()->GetViewMatrix();
    _cameraCB.matProj = _pCameraMgr->GetCurrentCamera()->GetProjMatrix();

    cubeMgr->Update(_pEntityMgr);

    // 時間がないのでちょっと汚い
    // TODO: 直し
    player->GetComponent<component::Transform>()->SetRotationY(TPCamera->GetComponent<component::Transform>()->GetRotation().y + math::PI);
}
//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void GameScene::OnRender(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture)
{
    gpu::setRenderTarget(0, colorTexture);
    gpu::setDepthStencil(depthTexture);

    // カラーとデプスバッファをクリア
    gpu::clearColor(colorTexture, float4(0.0f, 0.0f, 0.0f, 1.0f));   // カラーバッファ
    gpu::clearDepth(depthTexture, 1.0f);                             // デプスバッファ

    effect::BasicEffectIns()->SetEyePos(player->GetComponent<component::Transform>()->GetPosition());

    _pCubeMap->Render(_cameraCB);

    cubeMgr->Render();
}
//---------------------------------------------------------------------------
//! Imgui描画
//---------------------------------------------------------------------------
void GameScene::OnRenderImgui()
{
}
//---------------------------------------------------------------------------
//! 解放
//---------------------------------------------------------------------------
void GameScene::OnFinalize()
{
}

//---------------------------------------------------------------------------
//! シーンの生成
//! @note テンプレートの特殊化で実装します
//---------------------------------------------------------------------------
template<>
std::unique_ptr<BaseScene> createNewScene<GameScene>()
{
    return std::make_unique<GameScene>();
}

}   // namespace scene
