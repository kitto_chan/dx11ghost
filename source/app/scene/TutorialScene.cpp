﻿//---------------------------------------------------------------------------
//!	@file	TutorialScene.h
//!	@brief	ゲームチュートリアル
//---------------------------------------------------------------------------

#include "TutorialScene.h"
#include "manager/SceneManager.h"
#include "manager/CameraManager.h"

#include "entity/gameObject/character/Ghost.h"
#include "component/Transform.h"

#include "effect/Effect.h"
namespace scene {
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
TutorialScene::TutorialScene()
{
}

//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
TutorialScene::~TutorialScene()
{
    // TODO: リファクタリング
    // 文字クリア
    render::FontRendererIns()->Clear();
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool TutorialScene::OnInit()
{
    auto* backBuffer  = gpu::swapChain()->backBuffer();
    f32   centerWidth = static_cast<f32>(backBuffer->desc().width_) / 2.0f;   //　画面中心点を求める

    {
        std::wstring     word = L"そうさ";
        render::FontDesc fontDesc(word.c_str(), float2(centerWidth, 200), WHITE, 2.0f);
        render::FontRendererIns()->SetFont("Controll", fontDesc);
    }

    {
        std::wstring     word = L"マウス　   　 : 視点変更\nＷＳキー      : 移動\nＳＰＡＣＥキー: ジャンプ";
        render::FontDesc fontDesc(word.c_str(), float2(centerWidth, 350), WHITE, 0.8f, "MsFont");
        render::FontRendererIns()->SetFont("Control", fontDesc);
    }

    {
        std::wstring     word = L"赤ゴースト: 常にプレイヤーを追跡(地形を超えない)";
        render::FontDesc fontDesc(word.c_str(), float2(centerWidth, 450), float4(1.0f, 0.0f, 0.0f, 1.0f), 0.5f, "MsFont");
        render::FontRendererIns()->SetFont("RedGhost", fontDesc);
    }

    {
        std::wstring     word = L"緑ゴースト: ランダムに移動する(地形を超えない)";
        render::FontDesc fontDesc(word.c_str(), float2(centerWidth, 500), float4(0.0f, 1.0f, 0.0f, 1.0f), 0.5f, "MsFont");
        render::FontRendererIns()->SetFont("GreenGhost", fontDesc);
    }

    {
        std::wstring     word = L"青ゴースト: 常にプレイヤーを追跡(地形を超える)";
        render::FontDesc fontDesc(word.c_str(), float2(centerWidth, 550), float4(0.3f, 0.3f, 1.0f, 1.0f), 0.5f, "MsFont");
        render::FontRendererIns()->SetFont("BlueGhost", fontDesc);
    }

    {
        std::wstring     word = L"目標： 地図上のドットを全部収集する";
        render::FontDesc fontDesc(word.c_str(), float2(centerWidth, 630), float4(1.0f, 0.8f, 0.4f, 1.0f), 0.9f, "MsFont");
        render::FontRendererIns()->SetFont("Goal", fontDesc);
    }

    return true;
}
//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void TutorialScene::OnUpdate()
{
    // Push Push Space Key の文字　アニメション
    {
        auto*            backBuffer = gpu::swapChain()->backBuffer();
        f32              width      = static_cast<f32>(backBuffer->desc().width_) / 2.0f;
        std::wstring     push       = L"Push Space Key To Game Start";
        f32              scale      = 0.4f + abs(sin(timer::TimerIns()->TotalTime())) / 3.0f;
        render::FontDesc fontDesc(push.c_str(), float2(width, 700), WHITE, scale);
        render::FontRendererIns()->SetFont("Push", fontDesc);
    }

    // スペースキーを押したら、ゲームシーンに切り替えます
    if(KeyboardMgr()->IsKeyPress(DirectX::Keyboard::Space)) {
        manager::SetNextScene(manager::eScene::Game);
    }
}
//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void TutorialScene::OnRender(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture)
{
    gpu::setRenderTarget(0, colorTexture);
    gpu::setDepthStencil(depthTexture);

    // カラーとデプスバッファをクリア
    gpu::clearColor(colorTexture, float4(0.0f, 0.0f, 0.0f, 1.0f));   // カラーバッファ
    gpu::clearDepth(depthTexture, 1.0f);                             // デプスバッファ

    // ビューポート設定
    {
        auto*                   backBuffer = gpu::swapChain()->backBuffer();
        auto*                   context    = gpu::context();
        ID3D11RenderTargetView* rtv        = *colorTexture;
        context->OMSetRenderTargets(1, &rtv, nullptr);

        D3D11_VIEWPORT viewport;
        viewport.TopLeftX = 0;
        viewport.TopLeftY = 0;
        viewport.Width    = static_cast<f32>(backBuffer->desc().width_);
        viewport.Height   = static_cast<f32>(backBuffer->desc().height_);
        viewport.MinDepth = 0.0f;
        viewport.MaxDepth = 1.0f;
        context->RSSetViewports(1, &viewport);
    }
}
//---------------------------------------------------------------------------
//! Imgui描画
//---------------------------------------------------------------------------
void TutorialScene::OnRenderImgui()
{
}
//---------------------------------------------------------------------------
//! 解放
//---------------------------------------------------------------------------
void TutorialScene::OnFinalize()
{
}

//---------------------------------------------------------------------------
//! シーンの生成
//! @note テンプレートの特殊化で実装します
//---------------------------------------------------------------------------
template<>
std::unique_ptr<BaseScene> createNewScene<TutorialScene>()
{
    return std::make_unique<TutorialScene>();
}

}   // namespace scene
