﻿//---------------------------------------------------------------------------
//!	@file	ImguiWidget.h
//!	@brief	自分の描画スタイル
//---------------------------------------------------------------------------
#pragma once

namespace component {
class Transform;
}

namespace entity {
class Entity;
}

namespace manager {
class EntityManager;
}

namespace imgui {
//! ImguiGuizmoViewを描画する
void ImguiGuizmoView(const matrix& view, const matrix& proj, const raw_ptr<component::Transform>& transCom);

//! ImGuiのAction描画
void ImguiActionBar();

//! ImGuiのHierarchyを描画
void ImGuiHierarchy(const std::vector<raw_ptr<entity::Entity>>& entts, manager::EntityManager& enttMgr,  bool isChild);
}   // namespace ui
