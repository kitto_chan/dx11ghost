﻿//---------------------------------------------------------------------------
//!	@file	ImguiWidget.cpp
//!	@brief	自分の描画スタイル
//---------------------------------------------------------------------------
#include "ImGuiGameView.h"
#include "imgui/imgui.h"
#include "imgui/imgui_internal.h"
#include "IconFont/IconsFontAwesome5.h"

#include "manager/EntityManager.h"
#include "entity/Entity.h"

#include "entity/gameObject/GameObject.h"
#include "component/Transform.h"

namespace imgui {
//----------
// Hierarchy用
namespace hierarchy {
ImGuiTreeNodeFlags baseNodeFlags = ImGuiTreeNodeFlags_None | ImGuiTreeNodeFlags_SpanAvailWidth;
s32                selectionMask = (1 << 2);
s32                selectedId    = 0;
}   // namespace hierarchy

//---------------------------------------------------------------------------
//! ImguiGuizmoViewを描画する
//---------------------------------------------------------------------------
void ImguiGuizmoView(const matrix& viewMat, const matrix& projMat, const raw_ptr<component::Transform>& transCom)
{
    if(!transCom) return;

    ImGui::Begin("Scene");
    ImGuizmo::SetOrthographic(false);
    ImGuizmo::SetDrawlist(ImGui::GetForegroundDrawList());
    ImGuizmo::SetRect(ImGui::GetWindowPos().x, ImGui::GetWindowPos().y,
                      ImGui::GetContentRegionAvail().x, ImGui::GetContentRegionAvail().y);

    matrix worldMat = transCom->GetLocalToWorldMatrix();   // 子行列->親行列

    // cast matrix -> float[16]
    float* viewF16  = math::CastF16(viewMat);
    float* projF16  = math::CastF16(projMat);
    float* worldF16 = math::CastF16(worldMat);

    //  GuizmoのManipulate
    switch(SystemSettingsMgr()->GetImGuizmoAction()) {
        case sys::ImGuizmoAction::Transform:
            ImGuizmo::Manipulate(viewF16, projF16, ImGuizmo::OPERATION::TRANSLATE,
                                 ImGuizmo::LOCAL, worldF16);
            break;
        case sys::ImGuizmoAction::Rotate:
            ImGuizmo::Manipulate(viewF16, projF16, ImGuizmo::OPERATION::ROTATE,
                                 ImGuizmo::LOCAL, worldF16);
            break;
        case sys::ImGuizmoAction::Scale:
            ImGuizmo::Manipulate(viewF16, projF16, ImGuizmo::OPERATION::SCALE,
                                 ImGuizmo::LOCAL, worldF16);
            break;
        default:
            ASSERT_MESSAGE(false, "不可能なEnum");
            break;
    }

    // Guizmo の判定
    if(ImGuizmo::IsUsing()) {
        float matrixTranslation[3], matrixRotation[3], matrixScale[3];

        // もし対象は子物件は子行列計算必要がある
        if(transCom->GetParent()) {
            matrix movedMat = math::CastFromF16(worldF16);

            matrix childMat = mul(worldMat, hlslpp::inverse(movedMat));                     // 移動量計算
            childMat        = mul(transCom->GetLocalMatrix(), hlslpp::inverse(childMat));   // 子行列に変換

            float* childF16 = math::CastF16(childMat);

            //　行列を分解
            ImGuizmo::DecomposeMatrixToComponents(childF16, matrixTranslation, matrixRotation, matrixScale);
        }
        // 親対象(そのまま)
        else {
            //　行列を分解
            ImGuizmo::DecomposeMatrixToComponents(worldF16, matrixTranslation, matrixRotation, matrixScale);
        }

        // 回転はオイラー角保存してるので
        float3 rot = transCom->GetRotation();
        rot.x      = math::D2R(matrixRotation[0]);
        rot.y      = math::D2R(matrixRotation[1]);
        rot.z      = math::D2R(matrixRotation[2]);

        transCom->SetPosition(float3(matrixTranslation[0], matrixTranslation[1], matrixTranslation[2]));
        transCom->SetScale(float3(matrixScale[0], matrixScale[1], matrixScale[2]));
        transCom->SetRotation(rot);
    }
    ImGui::End();
}
//---------------------------------------------------------------------------
//! ImGuiのAction描画
//---------------------------------------------------------------------------
void ImguiActionBar()
{
    // Padding
    ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, { 0.5f, 0.5f });
    ImGui::Begin("Actions");

    // アクションアイコンは真ん中で描画
    {
        ImGui::SameLine(ImGui::GetWindowSize().x * 0.5f + 15.0f);
        std::string actionIcon;
        if(SystemSettingsMgr()->GetSystemState() == sys::SystemState::Play) {
            actionIcon = ICON_FA_PLAY;
        }
        else {
            actionIcon = ICON_FA_PAUSE;
        }

        if(ImGui::Button(actionIcon.c_str(), { 30.0f, 30.0f })) {
            SystemSettingsMgr()->SwapSysState();
        };
    }

    // imguizmo 描画
    {
        static std::string type[3]    = { "Transform", "Rotation", "Scale" };
        s32                selected   = static_cast<s32>(SystemSettingsMgr()->GetImGuizmoAction());
        f32                alignRight = ImGui::GetWindowSize().x * 0.8f;
        ImGui::SameLine(alignRight);
        ImGui::Text("");
        for(int n = 0; n < 3; n++) {
            ImGui::SameLine();
            if(ImGui::Selectable(type[n].c_str(), selected == n, ImGuiSelectableFlags_None, ImVec2(80, 10)))
                SystemSettingsMgr()->SetImGuizmoAction(sys::ImGuizmoAction(n));
        }
    }

    ImGui::PopStyleVar();
    ImGui::End();
}
//---------------------------------------------------------------------------
//! ImGuiのHierarchyを描画
//! Recursive(リカーシブ)関数
//---------------------------------------------------------------------------
void ImGuiHierarchy(const std::vector<raw_ptr<entity::Entity>>& entts, manager::EntityManager& enttMgr, bool isChild)
{
    using namespace hierarchy;
    ImGui::Begin("Hierarchy");
    s32 nodeClicked = -1;

    for(s32 i = 0; i < entts.size(); i++) {
        // entity -> gameObject
        gameobject::GameObject* parentObj = dynamic_cast<gameobject::GameObject*>(entts[i].get());

        //　(リカーシブ)関数なのでChildの2次描画しないように
        if(!isChild && parentObj->HasParent()) continue;

        ImGuiTreeNodeFlags nodeFlags  = baseNodeFlags;                                        //!<　デフォルトフラッグ
        const bool         isSelected = (selectionMask & (1 << i)) != 0 && selectedId == i;   //!< SelectMask
        std::string        itemId     = entts[i]->GetName();                                  //!< ネームはIDとして

        // 子物件が選ばないように
        // 子物件を選んだときは色変わるだけ
        if(!isChild && isSelected) {
            nodeFlags |= ImGuiTreeNodeFlags_Selected;
        }

        // もし子物件あるなら、子物件のTreeNodeも作ります
        if(!parentObj->HasChild()) {
            if(enttMgr.GetSelectedEntities() == parentObj) ImGui::PushStyleColor(ImGuiCol_Text, { 1, 0, 0, 1 });
            bool node_open = ImGui::TreeNodeEx((void*)(intptr_t)i, nodeFlags, itemId.c_str());
            if(enttMgr.GetSelectedEntities() == parentObj) ImGui::PopStyleColor();
            if(ImGui::IsItemClicked()) {
                if(!isChild) {
                    selectedId  = i;
                    nodeClicked = i;
                }
                enttMgr.SetSelectedEntity(parentObj);
            }

            // 開いたら子のTreeNode生成する
            if(node_open) {
                ImGuiHierarchy(parentObj->GetChildEntts(), enttMgr, true);
                ImGui::TreePop();
            }
        }
        else {
            // The only reason we use TreeNode at all is to allow selection of the leaf. Otherwise we can
            // use BulletText() or advance the cursor by GetTreeNodeToLabelSpacing() and call Text().
            // もしも子物件ないなら現在のTreeNodeで描画する
            nodeFlags |= ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_NoTreePushOnOpen;   // ImGuiTreeNodeFlags_Bullet

            if(enttMgr.GetSelectedEntities() == parentObj) ImGui::PushStyleColor(ImGuiCol_Text, { 1, 0, 0, 1 });
            ImGui::TreeNodeEx((void*)(intptr_t)i, nodeFlags, itemId.c_str());
            if(enttMgr.GetSelectedEntities() == parentObj) ImGui::PopStyleColor();

            if(ImGui::IsItemClicked()) {
                if(!isChild) {
                    selectedId  = i;
                    nodeClicked = i;
                }
                enttMgr.SetSelectedEntity(parentObj);
            }
        }
    }

    // セレクション更新
    if(!isChild && nodeClicked != -1) {
        // Update selection state
        // (process outside of tree loop to avoid visual inconsistencies during the clicking frame)
        selectionMask = (1 << nodeClicked);   // Click to single-select
    }
    ImGui::End();
}
}   // namespace ui
