﻿//---------------------------------------------------------------------------
//!	@file	GameManager.h
//!	@brief	ゲームの状態管理
//! @note 　このクラスのデザインちょっとおかしいかも、TODO: Better Design
//---------------------------------------------------------------------------
#pragma once

namespace manager {
using namespace gameApp;
class GameManager : public Singleton<GameManager>
{
public:
    GameManager();
    ~GameManager();

    //---------------------------------------------------------------------------
    // 関数
    //---------------------------------------------------------------------------
    GameLevel GetCurrentLevel() const;
    void      SetCurrentLevel(const GameLevel& nextLevel);

	s32 score = 0;

private:
    GameLevel _currentLevel = GameLevel::Level_1;
    //---------------------------------------------------------------------------
    // 内部変数
    //---------------------------------------------------------------------------
};
//! カメラ管理クラスを取得
manager::GameManager* GameMgr();

}   // namespace manager
