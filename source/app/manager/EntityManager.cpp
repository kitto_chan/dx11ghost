﻿//---------------------------------------------------------------------------
//!	@file	EntityManager.cpp
//!	@brief	エンティティマネージャー
//---------------------------------------------------------------------------
#include "EntityManager.h"
#include "Entity/Entity.h"
#include "entity/gameObject/GameObject.h"
#include "component/Transform.h"
namespace manager {
namespace {
raw_ptr<manager::EntityManager> _pCurrentWorld;   // 現在のワールド

ImGuiTreeNodeFlags baseNodeFlags = ImGuiTreeNodeFlags_None | ImGuiTreeNodeFlags_SpanAvailWidth;
s32                selectionMask = (1 << 2);
}   // namespace
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
EntityManager::EntityManager()
{
    _pCurrentWorld = this;
}
//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
EntityManager::~EntityManager()
{
    UnregistAll();
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool EntityManager::Init()
{
    for(auto& _entity : _entities) {
        if(!_entity->_isInited) {
            _entity->Init();
        }
    }

    return true;
}

//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void EntityManager::Update()
{
    /*   for(int i = (int)m_entities.size() - 1; i >= 0; --i) {
        auto _entity = m_entities[i];
        if(_entity->m_is_release) {
            Unregist(_entity);
            continue;
        }
    }*/

    //for(auto entity : _entities) {
    //    if(!entity->_isInited) {
    //        entity->Init();
    //    }

    //    entity->Update();
    //}

    for(auto& entity : _entities) {
        if(!entity->_isInited) {
            entity->Init();
        }

        if(entity->_isRelease) {
            Unregist(entity);
            continue;
        }

        entity->Update();
    }
}

//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void EntityManager::Render()
{
    for(auto entity : _entities) {
        entity->Render();
    }
}

//---------------------------------------------------------------------------
//! Imgui描画
//---------------------------------------------------------------------------
void EntityManager::RenderImgui()
{
    // Hierarchy Ui
    imgui::ImGuiHierarchy(_entities, *this, false);

    // Inspector Ui
    ImGui::Begin("Inspector");
    if(_selectedEntity != nullptr) {
        GetSelectedEntities()->RenderImgui();
    }
    ImGui::End();
}

//---------------------------------------------------------------------------
//! 解放
//---------------------------------------------------------------------------
void EntityManager::Finalize()
{
    for(auto entity : _entities) {
        if(!entity->_isRelease)
            entity->Finalize();
    }
}

//---------------------------------------------------------------------------
//! エンティティを登録
//---------------------------------------------------------------------------
void EntityManager::Regist(raw_ptr<Entity> entity)
{
    _entities.push_back(entity);
}
//---------------------------------------------------------------------------
//! 特定エンティティの登録を解除
//---------------------------------------------------------------------------
void EntityManager::Unregist(raw_ptr<Entity> entity)
{
    for(auto itr = _entities.begin(); itr != _entities.end(); ++itr) {
        if(*itr == entity) {
            raw_ptr<Entity> _entity = *itr;

            if(_entity->_isRelease) {
                _entity->Finalize();
                delete(_entity.get());
            }
            _entities.erase(itr);
            break;
        }
    }
}
//---------------------------------------------------------------------------
//! 既存のエンティティの登録を解除
//---------------------------------------------------------------------------
void EntityManager::UnregistAll()
{
    for(auto itr = _entities.rbegin(); itr != _entities.rend(); ++itr) {
        raw_ptr<Entity> _entity = *itr;

        if(!_entity->_isRemoved) {
            _entity->_isRemoved = true;
            delete _entity.get();
        }
    }

    _entities.clear();
}
//---------------------------------------------------------------------------
//! エンティティのリストを取得
//---------------------------------------------------------------------------
std::vector<raw_ptr<Entity>> EntityManager::GetEntities() const
{
    return _entities;
}
//---------------------------------------------------------------------------
//! 選択しているエンティティのリストを取得
//---------------------------------------------------------------------------
raw_ptr<Entity> EntityManager::GetSelectedEntities() const
{
    return _selectedEntity;
}
//---------------------------------------------------------------------------
//! 選択しているエンティティのリストを設定
//---------------------------------------------------------------------------
void EntityManager::SetSelectedEntity(const raw_ptr<Entity>& selected)
{
    _selectedEntity = selected;
}
//---------------------------------------------------------------------------
//! エンティティを検索する
//---------------------------------------------------------------------------
raw_ptr<Entity> FindEntityByName(std::string_view name)
{
    if(!_pCurrentWorld) return nullptr;
    for(auto entity : _pCurrentWorld->GetEntities()) {
        if(entity->GetName() == name) {
            return entity;
		}
	}

	return nullptr;
}
}   // namespace manager
