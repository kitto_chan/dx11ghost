﻿//---------------------------------------------------------------------------
//!	@file	CubeManager.h
//!	@brief	ゲームキューブ管理
//---------------------------------------------------------------------------
#pragma once
#include "entity/gameObject/character/Ghost.h"

namespace manager {
class EntityManager;

class CubeManager
{
public:
    CubeManager();    //<! コンストラクタ
    ~CubeManager();   //<! デストラクタ

    // コピー禁止/代入禁止
    CubeManager(const CubeManager&) = delete;
    CubeManager(CubeManager&&)      = delete;
    CubeManager& operator=(const CubeManager&) = delete;
    CubeManager& operator=(CubeManager&&) = delete;

public:
    //---------------------------------------------------------------------------
    //public 関数
    //---------------------------------------------------------------------------
    bool Init(raw_ptr<EntityManager> enttMgr);                      //!< 初期化
    void Update([[maybe_unused]] raw_ptr<EntityManager> enttMgr);   //!< 更新
    void Render();                                                  //!< 描画
    void Finalize();                                                //!< 解放

    s32 GetScore() const { return static_cast<s32>(_dotList.size()); }

private:
    //---------------------------------------------------------------------------
    // private 関数
    //---------------------------------------------------------------------------

    //---------------------------------------------------------------------------
    // 内部変数
    //---------------------------------------------------------------------------
    std::vector<raw_ptr<gameobject::GameObject>> _dotList;     //!< ドットたち保存用リスト
    std::vector<raw_ptr<gameobject::Ghost>>      _ghostList;   //!< ゴースト達保存用リスト
};

}   // namespace manager
