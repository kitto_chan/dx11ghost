﻿//---------------------------------------------------------------------------
//!	@file	scene.cpp
//!	@brief	シーン管理
//---------------------------------------------------------------------------
#include "../manager/SceneManager.h"

namespace scene {
class TitleScene;
class TutorialScene;
class GameScene;
class GameClearScene;
class GameOverScene;
}   // namespace scene

extern template std::unique_ptr<scene::BaseScene> scene::createNewScene<scene::GameScene>();
extern template std::unique_ptr<scene::BaseScene> scene::createNewScene<scene::TitleScene>();
extern template std::unique_ptr<scene::BaseScene> scene::createNewScene<scene::TutorialScene>();
extern template std::unique_ptr<scene::BaseScene> scene::createNewScene<scene::GameClearScene>();
extern template std::unique_ptr<scene::BaseScene> scene::createNewScene<scene::GameOverScene>();
namespace manager {

namespace {

std::unique_ptr<scene::BaseScene> scene_;       //!<　今のシーン
std::unique_ptr<scene::BaseScene> sceneNext_;   //!<　次のシーン

}   // namespace

//--------------------------------------------------------------------------
// 初期化
//--------------------------------------------------------------------------
bool onInitialize([[maybe_unused]] u32 width, [[maybe_unused]] u32 height)
{
    //----------------------------------------------------------
    // 初期シーンの作成
    //----------------------------------------------------------
    sceneNext_ = scene::createNewScene<scene::TitleScene>();
    if(!sceneNext_)
        return false;

    return true;
}

//--------------------------------------------------------------------------
//! 更新
//! @attention ここでは描画発行は行わないこと
//--------------------------------------------------------------------------
void onUpdate(f32 deltaTime)
{
    static bool s = false;
    if(sceneNext_) {
		
        scene_ = std::move(sceneNext_);


        // 初期化
        if(!scene_->Init()) {
            scene_.reset();   // 解放
        }
    }

    //----------------------------------------------------------
    // 更新実行
    //----------------------------------------------------------
    if(scene_) {
        scene_->Update(deltaTime);
    }
}

//--------------------------------------------------------------------------
//! 描画
//! @attention ここでは移動更新は行わないこと
//--------------------------------------------------------------------------
void onRender(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture)
{
    if(scene_) {
        scene_->Render(colorTexture, depthTexture);
        if(SystemSettingsMgr()->IsEditorMode()) {
            scene_->RenderImgui(colorTexture);
        }
    }
}

//--------------------------------------------------------------------------
//! 解放
//--------------------------------------------------------------------------
void onFinalize()
{
    scene_.reset();
    sceneNext_.reset();
}
//--------------------------------------------------------------------------
//!　シーンを切り替える
//--------------------------------------------------------------------------
void SetNextScene(eScene scene)
{
    switch(scene) {
        case eScene::Title: sceneNext_ = scene::createNewScene<scene::TitleScene>(); break;
        case eScene::Tutorial: sceneNext_ = scene::createNewScene<scene::TutorialScene>(); break;
        case eScene::Game: sceneNext_ = scene::createNewScene<scene::GameScene>(); break;
        case eScene::Clear: sceneNext_ = scene::createNewScene<scene::GameClearScene>(); break;
        case eScene::Over: sceneNext_ = scene::createNewScene<scene::GameOverScene>(); break;
    }
}
}   // namespace manager
