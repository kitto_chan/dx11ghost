﻿//---------------------------------------------------------------------------
//!	@file	EntityManager.h
//!	@brief	エンティティマネージャー
//---------------------------------------------------------------------------
#pragma once
namespace entity {
class Entity;
}
using namespace entity;

namespace manager {
class EntityManager
{
public:
    EntityManager();
    ~EntityManager();

    //---------------------------------------------------------------------------
    //! 関数
    //---------------------------------------------------------------------------
    bool Init();          //!< 初期化
    void Update();        //!< 更新
    void Render();        //!< 描画
    void RenderImgui();   //!< Imgui描画
    void Finalize();      //!< 解放

    void Regist(raw_ptr<Entity> entity);     //!< エンティティを登録
    void Unregist(raw_ptr<Entity> entity);   //!< エンティティの登録解除
    void UnregistAll();                      //!< すべてエンティティの登録解除

    std::vector<raw_ptr<Entity>> GetEntities() const;   //!< エンティティを取得

    raw_ptr<Entity> GetSelectedEntities() const;
    void            SetSelectedEntity(const raw_ptr<Entity>& selected);

private:
    //---------------------------------------------------------------------------
    //! 内部変数
    //---------------------------------------------------------------------------
    std::vector<raw_ptr<Entity>> _entities;   //!< エンティティの配列

    raw_ptr<Entity> _selectedEntity;
};

raw_ptr<Entity> FindEntityByName(std::string_view name); //!< エンティティを検索する
}   // namespace manager
