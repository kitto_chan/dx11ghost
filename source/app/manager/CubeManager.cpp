﻿//---------------------------------------------------------------------------
//!	@file	CubeManager.h
//!	@brief	ゲームカメラ管理
//---------------------------------------------------------------------------
#pragma once
#include "CubeManager.h"
#include "manager/EntityManager.h"

#include "entity/gameObject/Cube.h"
#include "entity/gameObject/Shpere.h"
#include "entity/gameObject/character/Ghost.h"
#include "entity/gameObject/character/Player.h"

#include "component/Transform.h"
#include "component/ModelRenderer.h"
#include "component/action/CubeAction.h"
#include "component/action/FloorAction.h"
#include "component/action/RollingPinAction.h"
#include "component/action/FollowAction.h"
#include "component/action/ChaseAction.h"

namespace manager {
namespace {
constexpr f32 SIDE_LENGTH = 1.0f;   //!< キューブの長さ

constexpr s32 MAP_H = settings::MAP_H;   //!< マップ長さ
constexpr s32 MAP_W = settings::MAP_W;   //!< マップ幅さ

s32 _mapData[MAP_H][MAP_W];   //!< ランダム生成したマップ
}   // namespace
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
CubeManager::CubeManager()
{
}

//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
CubeManager::~CubeManager()
{
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool CubeManager::Init(raw_ptr<EntityManager> enttMgr)
{
    raw_ptr<gameobject::GameObject> floorParentObj = new gameobject::GameObject("Floor");   // キューブたちの親
    enttMgr->Regist(floorParentObj.get());

    raw_ptr<gameobject::GameObject> dotParentObj = new gameobject::GameObject("Dots");   // ドットたちの親
    enttMgr->Regist(dotParentObj.get());

    for(int h = 0; h < MAP_H; h++) {
        for(int w = 0; w < MAP_W; w++) {
            _mapData[h][w] = 1;
            //	柱を2マス飛ばしにしたい
            if(w % 3 == 0 && h % 3 == 0) {
                _mapData[h][w] = -1;
            }

            // 枠は壁になる
            if(w == 0 || h == 0 ||
               w == MAP_W - 1 || h == MAP_H - 1)
                _mapData[h][w] = 0;
        }
    }

    for(int h = 2; h < MAP_H - 2; h++) {
        for(int w = 2; w < MAP_W - 2; w++) {
            if(_mapData[h][w] != -1) continue;
            _mapData[h][w] = 0;

            while(true) {
                int random = math::GetRandomI(3);
                //	３マス飛ばしの場合は一番上の柱は h == 4 のところ
                if(h != 3) {
                    random = math::GetRandomI(2);
                }
                //	１マス隣の場所用の変数
                int w1 = w;
                int h1 = h;
                //	２マス隣
                int w2 = w;
                int h2 = h;

                //	ランダムな値に合わせてそれぞれの場所を決めます
                if(random == 0) {
                    w1 += 1;
                    w2 += 2;
                }
                if(random == 1) {
                    h1 += 1;
                    h2 += 2;
                }

                //	壁を置いていく
                if(_mapData[h1][w1] != 0) {
                    //	１マス隣を壁にする
                    _mapData[h1][w1] = 0;
                    _mapData[h2][w2] = 0;
                    break;
                }
            }
        }
    }

    // ドットと床の生成処理
    {
        for(s32 h = 0; h < MAP_H; h++) {
            for(s32 w = 0; w < MAP_W; w++) {
                // 1 : 壁（仮）なのでスキップします
                if(_mapData[h][w] == 1) continue;

                // 床のブロック生成
                float3            initPos  = { w, 0, h };
                gameobject::Cube* newFloor = new gameobject::Cube("Floor", initPos, { 1.0, 1.0f, 1.0f });
                newFloor->SetParent(floorParentObj);
                enttMgr->Regist(newFloor);

                // ドットを生成
                initPos.y += 1.0f;
                gameobject::Shpere* newDot = new gameobject::Shpere("Dot", initPos, 0.2f);
                newDot->SetParent(dotParentObj);
                enttMgr->Regist(newDot);
                _dotList.emplace_back(newDot);
            }
        }
    }

    // プレイヤーに追跡タイプ（赤色）
    {
        gameobject::Ghost* followGhost = new gameobject::Ghost(float3(MAP_W - 1.0f, 1.0f, MAP_H - 1));
        followGhost->SetGhostColor(float4(0.8f, 0.6f, 0.6f, 0.8f));
        followGhost->AddComponent<component::FollowAction>()->SetMapData(_mapData);
        followGhost->GetComponent<component::FollowAction>()->SetToTrackingMode();
        followGhost->GetComponent<component::Transform>()->SetScale(float3(0.2f, 0.2f, 0.2f));
        _ghostList.emplace_back(followGhost);
        enttMgr->Regist(followGhost);
    }
    // 気まぐれタイプ（緑色）
    {
        gameobject::Ghost* followGhost = new gameobject::Ghost(float3(MAP_W - 1.0f, 1.0f, 0.0f));
        followGhost->SetGhostColor(float4(0.6f, 0.8f, 0.6f, 0.8f));
        followGhost->AddComponent<component::FollowAction>()->SetMapData(_mapData);
        followGhost->GetComponent<component::FollowAction>()->SetToRandomMode();
        followGhost->GetComponent<component::Transform>()->SetScale(float3(0.2f, 0.2f, 0.2f));
        _ghostList.emplace_back(followGhost);
        enttMgr->Regist(followGhost);
    }
    // 気まぐれタイプ（緑色）
    {
        gameobject::Ghost* followGhost = new gameobject::Ghost(float3(0.0f, 1.0f, MAP_H - 1.0f));
        followGhost->SetGhostColor(float4(0.6f, 0.8f, 0.6f, 0.8f));
        followGhost->AddComponent<component::FollowAction>()->SetMapData(_mapData);
        followGhost->GetComponent<component::FollowAction>()->SetToRandomMode();
        followGhost->GetComponent<component::Transform>()->SetScale(float3(0.2f, 0.2f, 0.2f));
        _ghostList.emplace_back(followGhost);
        enttMgr->Regist(followGhost);
    }
    // プレイヤーに追跡タイプ(地形無視)（青色）
    {
        gameobject::Ghost* chaseGhost = new gameobject::Ghost(float3(MAP_W / 2.0f, 1.0f, MAP_H / 2.0f));
        chaseGhost->SetGhostColor(float4(0.6, 0.6f, 0.8f, 0.8f));
        chaseGhost->AddComponent<component::ChaseAction>();
        chaseGhost->GetComponent<component::Transform>()->SetScale(float3(0.2f, 0.2f, 0.2f));
        _ghostList.emplace_back(chaseGhost);
        enttMgr->Regist(chaseGhost);
    }

    return true;
}

//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void CubeManager::Update([[maybe_unused]] raw_ptr<EntityManager> enttMgr)
{
    // プレイヤーを探す
    raw_ptr<entity::Entity> player = manager::FindEntityByName("Player");
    if(!player) return;

    const float3 playerPos = player->GetComponent<component::Transform>()->GetPosition();
    for(auto itr = _dotList.begin(); itr != _dotList.end();) {
        auto   dot    = *itr;
        float3 dotPos = dot->GetPosition();
        f32    leng   = length(playerPos.xz - dotPos.xz);
        if(leng < 0.5f) {
            dot->Release();
            _dotList.erase(itr);
        }
        else {
            ++itr;
        }
    }

    for(auto itr = _ghostList.begin(); itr != _ghostList.end();) {
        auto   ghost    = *itr;
        float3 ghostPos = ghost->GetPosition();

        f32 leng = length(playerPos.xz - ghostPos.xz);
        if(leng < 0.65f) {
            dynamic_cast<gameobject::Player*>(player.get())->SetToDead();
            break;
        }
        else {
            ++itr;
        }
    }
}

//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void CubeManager::Render()
{
}

//---------------------------------------------------------------------------
//! 解放
//---------------------------------------------------------------------------
void CubeManager::Finalize()
{
}
}   // namespace manager
