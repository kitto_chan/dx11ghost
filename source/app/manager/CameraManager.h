﻿//---------------------------------------------------------------------------
//!	@file	CameraManager.h
//!	@brief	ゲームカメラ管理
//---------------------------------------------------------------------------
#pragma once
#include "entity/gameObject/camera/FirstPersonCamera.h"
#include "entity/gameObject/camera/ThirdPersonCamera.h"
//namespace entity {
//namespace go {
//class BaseCamera;
//}
//}   // namespace entity::go
//namespace gameobject = entity::go;

namespace manager {

class CameraManager : public Singleton<CameraManager>
{
public:
    CameraManager();
    ~CameraManager();

    //---------------------------------------------------------------------------
    // 関数
    //---------------------------------------------------------------------------
    bool Init();       //!< 初期化
    void Update();     //!< 更新
    void Render();     //!< 描画
    void Finalize();   //!< 解放

    //! 使ってるカメラを取得
    raw_ptr<gameobject::BaseCamera> GetCurrentCamera() const;

    //! 追加カメラ
    void AddCamera(raw_ptr<gameobject::BaseCamera> newCamera, bool setToCurrent = false);

    //! デバッグカメラとシーンのカメラシンクロする
    void SyncDebugCameraSceneCamera();

private:
    //---------------------------------------------------------------------------
    // 内部変数
    //---------------------------------------------------------------------------
    std::vector<raw_ptr<gameobject::BaseCamera>> _cameras;   //!< シーンのカメラ

    raw_ptr<gameobject::BaseCamera> _pCurrentCamera;   //!< 現在のカメラ
    uni_ptr<gameobject::BaseCamera> _pDebugCamera;     //!< デバッグカメラ
};
//! カメラ管理クラスを取得
manager::CameraManager* CameraMgr();

}   // namespace manager
