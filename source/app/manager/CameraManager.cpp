﻿//---------------------------------------------------------------------------
//!	@file	CameraManager.h
//!	@brief	ゲームカメラ管理
//---------------------------------------------------------------------------
#pragma once
#include "CameraManager.h"
#include "entity/gameObject/Camera/FirstPersonCamera.h"
#include "entity/gameObject/Camera/ThirdPersonCamera.h"
#include "component/Transform.h"
namespace manager {
namespace {
cb::CameraCB     _cameraCB{};                          //!< カメラ用定数バッファ
sys::SystemState lastState = sys::SystemState::Play;   //!< 前フレームのステートを保存する
}   // namespace
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
CameraManager::CameraManager()
{
}

//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
CameraManager::~CameraManager()
{
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool CameraManager::Init()
{
    // デバッグ用カメラ
    // デバッグカメラ特別で作るので、
    // EntityManager と cameraリストに含まれてないです
    _pDebugCamera = std::make_unique<gameobject::FirstPersonCamera>("Debug Camera");
    _pDebugCamera->Init();

    // デフォルトカメラ
    if(!_pCurrentCamera) {
        _pCurrentCamera = _pDebugCamera;
        //ASSERT_MESSAGE(false, "カメラつけてないです");
    }

    return true;
}

//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void CameraManager::Update()
{
    if(lastState != SystemSettingsMgr()->GetSystemState()) {
        //!< デバッグカメラの位置シーンカメラ
        SyncDebugCameraSceneCamera();
    }

    // カメラバッファ更新
    if(SystemSettingsMgr()->IsPauseState()) {
        _pDebugCamera->Update();   // デバッグカメラ更新
    }

    // カメラバッファ更新
    _cameraCB.matView = GetCurrentCamera()->GetViewMatrix();
    _cameraCB.matProj = GetCurrentCamera()->GetProjMatrix();

    lastState = SystemSettingsMgr()->GetSystemState();
}

//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void CameraManager::Render()
{
    gpu::setConstantBuffer("CameraCB", _cameraCB);
}

//---------------------------------------------------------------------------
//! 解放
//---------------------------------------------------------------------------
void CameraManager::Finalize()
{
}
//---------------------------------------------------------------------------
//! 現在シーンのカメラを取得
//---------------------------------------------------------------------------
raw_ptr<gameobject::BaseCamera> CameraManager::GetCurrentCamera() const
{
    if(SystemSettingsMgr()->GetSystemState() == sys::SystemState::Pause) {
        return _pDebugCamera;
    }
    else {
        return _pCurrentCamera;
    }
}
//---------------------------------------------------------------------------
//! カメラを追加する
//---------------------------------------------------------------------------
void CameraManager::AddCamera(raw_ptr<gameobject::BaseCamera> newCamera, bool setToCurrent)
{
    _cameras.emplace_back(newCamera);
    if(setToCurrent) _pCurrentCamera = newCamera;
}
//---------------------------------------------------------------------------
//! デバッグカメラとシーンのカメラシンクロする
//---------------------------------------------------------------------------
void CameraManager::SyncDebugCameraSceneCamera()
{
    _pDebugCamera->GetComponent<component::Transform>()->Copy(*_pCurrentCamera->GetComponent<component::Transform>().get());
}

//---------------------------------------------------------------------------
//! カメラ管理クラスを取得
//---------------------------------------------------------------------------
manager::CameraManager* CameraMgr()
{
    return CameraManager::Instance();
}
}   // namespace manager
