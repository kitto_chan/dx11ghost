﻿//---------------------------------------------------------------------------
//!	@file	precompile.h
//!	@brief	プリコンパイルヘッダー
//---------------------------------------------------------------------------
#pragma once

#include "utility/GameDef.h" //!< ゲーム用の定義

#include "framework.h"

//! DirectX11実装をgpuとして名前をエイリアス
namespace gpu = dx11;

#include "GameSettings.h"
#include "utility/ImGuiGameView.h"


#include "manager/GameManager.h" //!< ゲームマネージャー
#include "manager/EntityManager.h"

#include "sample/scene.h"   //!< シーン管理
