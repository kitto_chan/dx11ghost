﻿//---------------------------------------------------------------------------
//!	@file	Entity.cpp
//!	@brief	エンティティ
//---------------------------------------------------------------------------
#include "Entity.h"
namespace entity {
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
Entity::Entity()
{
}

Entity::Entity(const std::string& name)
: _name(name)
{
}

//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
Entity::~Entity()
{
    RemoveAllComponents();
}

//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool Entity::Init()
{
    OnInit();

    for(auto& component : _components) {
        if(!_isInited) {
            component->Init();
        }
    }

    _isInited = true;

    return true;
}

//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void Entity::Update()
{
    if(!_isEnable || !_isInited)
        return;

    OnUpdate();

    for(auto& component : _components) {
        if(!component->IsEnable())
            continue;
        component->Update();
    }
}

//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void Entity::Render()
{
    if(!_isEnable || !_isInited)
        return;

    OnRender();

    for(auto& component : _components) {
        if(!component->IsEnable())
            continue;
        component->Render();
    }
}

//---------------------------------------------------------------------------
//! Imgui描画
//---------------------------------------------------------------------------
void Entity::RenderImgui()
{
    if(!_isEnable) return;

    OnRender();

    for(auto& component : _components) {
        if(ImGui::CollapsingHeader(component->GetName().c_str())) {
            if(component) {
                component->RenderImgui();
            }
        }
    }
}
//---------------------------------------------------------------------------
//! 解放
//---------------------------------------------------------------------------
void Entity::Finalize()
{
    _isRelease = true;
}
void Entity::Release()
{
    _isRelease = true;
}
//---------------------------------------------------------------------------
//! 全部持ってるコンポーネントを削除する
//---------------------------------------------------------------------------
void Entity::RemoveAllComponents()
{
    for(auto itr = _components.rbegin(); itr != _components.rend(); ++itr) {
        auto component = (*itr);
        component->Finalize();
        delete component.get();
    }
    _components.clear();
}

std::string Entity::GetName()
{
    return _name;
}

}   // namespace entity
