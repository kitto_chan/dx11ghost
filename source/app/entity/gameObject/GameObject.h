﻿//---------------------------------------------------------------------------
//!	@file	GameObject.h
//!	@brief	ゲームオブジェクトの基底クラス
//---------------------------------------------------------------------------
#pragma once
#include "entity/Entity.h"
#include "component/Transform.h"

//namespace component {
//class Transform;
//};

namespace entity::go {

class GameObject : public Entity
{
public:
    GameObject();
    GameObject(const std::string& name);
    GameObject(const float3& pos);
    GameObject(const std::string& name, const float3& pos);
    virtual ~GameObject();

    //---------------------------------------------------------------------------
    // public 関数
    //---------------------------------------------------------------------------
    bool HasChild() const;    //!< 子物件あるかどうか
    bool HasParent() const;   //!< 親物件あるかどうか

    void SetParent(raw_ptr<GameObject> childObj);   //!< 子物件を設定

    raw_ptr<GameObject>              GetParent() const;       //!< 親を取得
    std::vector<raw_ptr<GameObject>> GetChildObjs() const;    //!< 子物件を取得(GameObject形式)
    std::vector<raw_ptr<Entity>>     GetChildEntts() const;   //!< 子物件を取得(Entity形式)

    float3 GetPosition() const { return _pTransform->GetPosition(); }   //!< 座標を取得

protected:
    //---------------------------------------------------------------------------
    //! public 変数
    //---------------------------------------------------------------------------

    raw_ptr<component::Transform> _pTransform;   //!< オブジェクトの位置、回転、スケールを扱うクラス
private:
    //---------------------------------------------------------------------------
    //! private 関数
    //---------------------------------------------------------------------------
    // 継承用
    //bool OnInit()        override;  //!< 初期化
    //void OnRender()      override;  //!< 描画
    //void OnUpdate()      override;  //!< 更新

    void OnRenderImgui() override;   //!< ImGui描画
    void OnFinalize() override;      //!< 解放
};
}   // namespace entity::go
namespace gameobject = entity::go;   //省略
