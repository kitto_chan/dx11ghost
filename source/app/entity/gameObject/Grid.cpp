﻿#include "Grid.h"
#include "component/Transform.h"
#include "effect/BasicPosColEffect.h"

namespace entity::go {
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
Grid::Grid()
{
}
//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
Grid::~Grid()
{
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool Grid::OnInit()
{
    InitGPU();

    return true;
}
//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void Grid::OnRender()
{
    matrix worldMatrix = GetComponent<component::Transform>()->GetLocalToWorldMatrix();
    effect::BasicPosColEffectIns()->SetWorldMatrix(worldMatrix);

    // 頂点データー
    gpu::setVertexBuffer(0, _pVertexBuffer, sizeof(vertex::VertexPosColor));   //!< 頂点バッファ
    gpu::setIndexBuffer(_pIndexBuffer);                                        //!< インデックスバッファ

    effect::BasicPosColEffectIns()->SetRenderLine();
    effect::BasicPosColEffectIns()->Apply();

    // TODO: pass by param..
    gpu::drawIndexed(gpu::Primitive::LineList, 6);   // 6個のインデックス
}
//---------------------------------------------------------------------------
//! 初期化GPU
//---------------------------------------------------------------------------
bool Grid::InitGPU()
{
    std::vector<vertex::VertexPosColor> vertices;
    vertices.assign({ { { 30.0f, 0.0f, 0.0f }, { 1.f, 0.f, 0.f, 1.f } },
                      { { 0.0f, 0.0f, 0.0f }, { 1.f, 0.f, 0.f, 1.f } },
                      { { 0.0f, 30.0f, 0.0f }, { 0.f, 1.f, 0.f, 1.f } },
                      { { 0.0f, 0.0f, 0.0f }, { 0.f, 1.f, 0.f, 1.f } },
                      { { 0.0f, 0.0f, 30.0f }, { 0.f, 0.f, 1.f, 1.f } },
                      { { 0.0f, 0.0f, 0.0f }, { 0.f, 0.f, 1.f, 1.f } } });

    std::vector<u32> indices;
    indices.assign({ 0, 1, 2, 3, 4, 5 });

    _pVertexBuffer = gpu::createBuffer({ vertices.size() * sizeof(vertex::VertexPosColor), D3D11_BIND_VERTEX_BUFFER, D3D11_USAGE_IMMUTABLE }, vertices.data());
    _pIndexBuffer  = gpu::createBuffer({ indices.size() * sizeof(u32), D3D11_BIND_INDEX_BUFFER, D3D11_USAGE_IMMUTABLE }, indices.data());

    return true;
}

}   // namespace entity::go
