﻿//---------------------------------------------------------------------------
//!	@file	GameObject.h
//!	@brief	ゲームオブジェクトの基底クラス
//---------------------------------------------------------------------------
#include "GameObject.h"
#include "Component/Transform.h"

namespace entity::go {
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
GameObject::GameObject()
{
    _pTransform = AddComponent<component::Transform>();
    _name       = "GameObject";

    _type |= gameApp::EntityType_GameObject;
}
//---------------------------------------------------------------------------
//! コンストラクタ
//! [in] name ゲームオブジェクトの名前
//---------------------------------------------------------------------------
GameObject::GameObject(const std::string& name)
: Entity(name)
{
    _pTransform = AddComponent<component::Transform>();
    //_tags |= GameTag_GameObject;
}
//---------------------------------------------------------------------------
//! コンストラクタ
//! [in] pos ゲームオブジェクトの初期座標
//---------------------------------------------------------------------------
GameObject::GameObject(const float3& pos)
{
    _pTransform = AddComponent<component::Transform>();
    _pTransform->SetPosition(pos);

    _name = "GameObject";
}
//---------------------------------------------------------------------------
//! コンストラクタ
//! [in] name ゲームオブジェクトの名前
//! [in] pos ゲームオブジェクトの初期座標
//---------------------------------------------------------------------------
GameObject::GameObject(const std::string& name, const float3& pos)
: Entity(name)
{
    _pTransform = AddComponent<component::Transform>();
    _pTransform->SetPosition(pos);
}
//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
GameObject::~GameObject()
{
    if(GetParent()) {
        _pTransform->GetParent()->RemoveChild(_pTransform);
    }
}
//---------------------------------------------------------------------------
//!< 子物件あるかどうか
//---------------------------------------------------------------------------
bool GameObject::HasChild() const
{
    return _pTransform->HasChild();
}
//---------------------------------------------------------------------------
//!< 親物件あるかどうか
//---------------------------------------------------------------------------
bool GameObject::HasParent() const
{
    return _pTransform->HasParent();
}
//---------------------------------------------------------------------------
//!< 子物件を設定
//---------------------------------------------------------------------------
void GameObject::SetParent(raw_ptr<GameObject> childObj)
{
    _pTransform->SetParent(childObj->_pTransform);
}
//---------------------------------------------------------------------------
//!< 親を取得
//---------------------------------------------------------------------------
raw_ptr<GameObject> GameObject::GetParent() const
{
    if(_pTransform->GetParent()) {
        return dynamic_cast<GameObject*>(_pTransform->GetParent()->GetOwner());
	}
    return nullptr;
}
//---------------------------------------------------------------------------
//! 子物件を取得(Entity形式)
//---------------------------------------------------------------------------
std::vector<raw_ptr<Entity>> GameObject::GetChildEntts() const
{
    std::vector<raw_ptr<Entity>> childEntt;

    for(auto childTrans : _pTransform->GetChildTransform()) {
        childEntt.emplace_back(childTrans->GetOwner());
    }

    return childEntt;
}
//---------------------------------------------------------------------------
//!< 子物件を取得(GameObject形式)
//---------------------------------------------------------------------------
std::vector<raw_ptr<GameObject>> GameObject::GetChildObjs() const
{
    std::vector<raw_ptr<GameObject>> childObjs;

    for(auto childTrans : _pTransform->GetChildTransform()) {
        raw_ptr<GameObject> childObj = dynamic_cast<GameObject*>(childTrans->GetOwner());
        childObjs.emplace_back(childObj);
    }

    return childObjs;
}
//---------------------------------------------------------------------------
//! Imgui描画
//---------------------------------------------------------------------------
void GameObject::OnRenderImgui()
{
}
void GameObject::OnFinalize()
{
   
}
}   // namespace entity::go
