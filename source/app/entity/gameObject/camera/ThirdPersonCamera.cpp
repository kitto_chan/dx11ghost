﻿//---------------------------------------------------------------------------
//!	@file	ThirdPersonCamera.cpp
//!	@brief	三人称視点クラス
//---------------------------------------------------------------------------
#include "ThirdPersonCamera.h"
#include "component/Transform.h"

namespace entity::go {
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
ThirdPersonCamera::ThirdPersonCamera()
{
    _name = "ThirdPersonCamera";
}
//---------------------------------------------------------------------------
//! コンストラクタ
//! [in] name ゲームオブジェクトの名前
//---------------------------------------------------------------------------
ThirdPersonCamera::ThirdPersonCamera(const std::string& name)
: BaseCamera(name)
{
}
//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
ThirdPersonCamera::~ThirdPersonCamera()
{
}
bool ThirdPersonCamera::OnInit()
{
    ASSERT_MESSAGE(_pTarget, "目標が設定されてない");
    Approach(-MouseMgr()->GetScrollWheelValue() / 120.f * 1.0f);

    return true;
}
//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void ThirdPersonCamera::OnUpdate()
{
#define mode 1
#if mode == 0
    if(!MouseMgr()->IsRelativeMode()) return;
    // 後ろから映る
    float3 targetPos = GetTargetTransform()->GetPosition();
    float3 targetRot = GetTargetTransform()->GetRotation();
    float3 pos       = _pTransform->GetPosition();

    pos.f32[0] = targetPos.x + _distance * sinf(targetRot.y + math::D2R(180.0f));
    pos.f32[2] = targetPos.z + _distance * cosf(targetRot.y + math::D2R(180.0f));
    pos.f32[1] = targetPos.y + 5.0f;
    _pTransform->SetPosition(pos);
    _pTransform->LookAt(targetPos, float3(0.0, 1.0f, 0.0f));
#elif mode == 1
    if(!MouseMgr()->IsRelativeMode()) return;
    // 自由角度調整ができる
    f32 dt = timer::TimerIns()->DeltaTime();
    RotateXAroundTarget(-MouseMgr()->GetPosY() * dt * 0.3f);
    RotateYAroundTarget(-MouseMgr()->GetPosX() * dt * 0.3f);
#endif
    Approach(-MouseMgr()->GetScrollWheelValue() / 120.f * 0.5f);
}
//---------------------------------------------------------------------------
//! ターゲットを中心点して、垂直方向のラジアン角
//---------------------------------------------------------------------------
void ThirdPersonCamera::RotateXAroundTarget(f32 rad)
{
    float3 rotation  = _pTransform->GetRotation();
    f32    rotationX = rotation.x + rad;

    ClampRotateXAroundTargetRatation(rotationX);
    rotation.x = rotationX;

    RotateAroundTarget(rotation);
}
//---------------------------------------------------------------------------
//! ターゲットを中心点して、水平回転のラジアン角
//---------------------------------------------------------------------------

void ThirdPersonCamera::RotateYAroundTarget(f32 rad)
{
    float3 rotation = _pTransform->GetRotation();

    // y軸のラジアン角度は[ -XM_PI から　 XM_PI] の間にする
    rotation.y = DirectX::XMScalarModAngle(rotation.y + rad);

    RotateAroundTarget(rotation);
}
//---------------------------------------------------------------------------
//! カメラとターゲットの距離を調整する
//---------------------------------------------------------------------------
void ThirdPersonCamera::Approach(f32 dist)
{
    _distance += dist;
    //! 距離を制限する
    _distance = std::clamp(_distance, _minDist, _maxDist);

    _pTransform->SetPosition(GetTargetTransform()->GetPosition());
    _pTransform->Translate(_pTransform->GetForwardAxis(), -_distance);
}
//---------------------------------------------------------------------------
//! ターゲットを中心点して、垂直回転のラジアン角を設定する
//---------------------------------------------------------------------------
void ThirdPersonCamera::SetRotateXAroundTarget(f32 rad)
{
    float3 rotation  = _pTransform->GetRotation();
    f32    rotationX = rad;

    ClampRotateXAroundTargetRatation(rotationX);
    rotation.x += rotationX;

    RotateAroundTarget(rotation);
}
//---------------------------------------------------------------------------
//!ターゲットを中心点して、水平回転のラジアン角を設定する
//---------------------------------------------------------------------------
void ThirdPersonCamera::SetRotateYAroundTarget(f32 rad)
{
    float3 rotation = _pTransform->GetRotation();

    // y軸のラジアン角度は[ -XM_PI から XM_PI] の間にする
    rotation.y = DirectX::XMScalarModAngle(rad);

    RotateAroundTarget(rotation);
}
//---------------------------------------------------------------------------
//! 距離をセット
//---------------------------------------------------------------------------
void ThirdPersonCamera::SetDistance(f32 dist)
{
    _distance = dist;
}
//---------------------------------------------------------------------------
//! 最大距離と最小距離をセット
//---------------------------------------------------------------------------
void ThirdPersonCamera::SetDistanceMinMax(f32 minDist, f32 maxDist)
{
    _minDist = minDist;
    _maxDist = maxDist;
}
//---------------------------------------------------------------------------
//! ターゲットを中心点して、回転のラジアン角
//---------------------------------------------------------------------------
void ThirdPersonCamera::RotateAroundTarget(const float3& rotation)
{
    _pTransform->SetRotation(rotation);
    _pTransform->SetPosition(GetTargetTransform()->GetPosition());
    _pTransform->Translate(_pTransform->GetForwardAxis(), -_distance);
}
//---------------------------------------------------------------------------
//! 垂直方向のラジアン角は範囲内に収める
//---------------------------------------------------------------------------
void ThirdPersonCamera::ClampRotateXAroundTargetRatation(f32& x)
{
    // X軸のラジアン角度は[ -math::PI / 3.f から　 0] の間にする
    f32 maxRotation = 0.0f;
    f32 minRotation = -math::PI / 3.f;

    x = std::clamp(x, minRotation, maxRotation);
}
}   // namespace entity::go
