﻿//---------------------------------------------------------------------------
//!	@file	Player.cpp
//!	@brief	キャラクタープレイヤーのクラス
//---------------------------------------------------------------------------
#include "Ghost.h"
#include "component/Transform.h"
#include "component/PlayerController.h"
#include "component/Animator.h"
#include "component/ModelRenderer.h"

namespace entity::go {
cb::MeshColorCB _cbColor;
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
Ghost::Ghost()
{
    _name           = "Ghost";
    _pModelRenderer = AddComponent<component::ModelRenderer>();
    _pModelRenderer->CreateModel("model/ghost_by_dommk.fbx");
    _pModelRenderer->LoadExternalDiffuseTexture("model/ghost_by_dommk_default_BaseColor.png");
}
//---------------------------------------------------------------------------
//! コンストラクタ
//! @param	[in] name オブジェクトの名前
//---------------------------------------------------------------------------
Ghost::Ghost(const std::string& name)
: GameObject(name)
{
    _pModelRenderer = AddComponent<component::ModelRenderer>();
    _pModelRenderer->CreateModel("model/ghost_by_dommk.fbx");
    _pModelRenderer->LoadExternalDiffuseTexture("model/ghost_by_dommk_default_BaseColor.png");
}

Ghost::Ghost(const float3& pos)
{
    _name = "Ghost";
    _pTransform->SetPosition(pos);
    _pModelRenderer = AddComponent<component::ModelRenderer>();
    _pModelRenderer->CreateModel("model/ghost_by_dommk.fbx");
    _pModelRenderer->LoadExternalDiffuseTexture("model/ghost_by_dommk_default_BaseColor.png");
}

//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
Ghost::~Ghost()
{
}

//---------------------------------------------------------------------------
//! プレイヤーの死亡判定
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool Ghost::OnInit()
{
    return true;
}
//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void Ghost::OnUpdate()
{
    // ゴーストが空中浮揚の表現
    constexpr f32 posY = 1.5f;
    f32           time = timer::TimerIns()->TotalTime() * 2.f;
    _pTransform->SetPositionY(posY + sinf(time) * 0.3f);
}
//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void Ghost::OnRender()
{
    _cbColor.color = _ghostColor;
    if(_isCleanness) {
        _cbColor.color.a = (sinf(timer::TimerIns()->TotalTime()) + 1) / 2.0f; // (0-1の間)
    }
    gpu::setConstantBuffer("MeshColorCB", _cbColor);
}
}   // namespace entity::go
