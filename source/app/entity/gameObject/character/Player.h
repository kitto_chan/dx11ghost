﻿//---------------------------------------------------------------------------
//!	@file	Player.h
//!	@brief	キャラクタープレイヤーのクラス
//---------------------------------------------------------------------------
#pragma once
#include "entity/gameObject/GameObject.h"

namespace component {
class Transform;
class Animator;
class ModelRenderer;
class PlayerController;
}   // namespace component

namespace entity::go {
class Player : public GameObject
{
public:
    Player();
    Player(const std::string& name);
    virtual ~Player();

public:
    //---------------------------------------------------------------------------
    // public 関数
    //---------------------------------------------------------------------------
    bool IsDead() const;   //!< プレイヤーの死亡判定
    void SetToDead() { _isDead = true; };
    //---------------------------------------------------------------------------
    // public 変数
    //---------------------------------------------------------------------------

protected:
    //---------------------------------------------------------------------------
    // protected 変数
    //---------------------------------------------------------------------------

    //---------------------------------------------------------------------------
    // protected 関数
    //---------------------------------------------------------------------------
private:
    //---------------------------------------------------------------------------
    // private 変数
    //---------------------------------------------------------------------------

    //---------------------------------------------------------------------------
    // private 関数
    //---------------------------------------------------------------------------
    raw_ptr<component::Animator>         _pAnimator;           //!< Animator コンポーネント 参照
    raw_ptr<component::ModelRenderer>    _pModelRenderer;      //!< ModelRenderer コンポーネント 参照
    raw_ptr<component::PlayerController> _pPlayerController;   //!< PlayerController コンポーネント 参照

    bool _isDead = false;
    //-----------------
    // 継承
    bool OnInit() override;     //!< 初期化
    void OnUpdate() override;   //!< 更新
    void OnRender() override;   //!< 描画
    //void Finalize() override;   //!< 解放
};
}   // namespace entity::go
