﻿//---------------------------------------------------------------------------
//!	@file	Player.cpp
//!	@brief	キャラクタープレイヤーのクラス
//---------------------------------------------------------------------------
#include "Player.h"
#include "component/Transform.h"
#include "component/PlayerController.h"
#include "component/Animator.h"
#include "component/ModelRenderer.h"

namespace entity::go {
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
Player::Player()
{
    _name              = "Player";
    _pAnimator         = AddComponent<component::Animator>();
    _pModelRenderer    = AddComponent<component::ModelRenderer>();
    _pPlayerController = AddComponent<component::PlayerController>();
}
//---------------------------------------------------------------------------
//! コンストラクタ
//! @param	[in] name オブジェクトの名前
//---------------------------------------------------------------------------
Player::Player(const std::string& name)
: GameObject(name)
{
    _pAnimator         = AddComponent<component::Animator>();
    _pModelRenderer    = AddComponent<component::ModelRenderer>();
    _pPlayerController = AddComponent<component::PlayerController>();
}

//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
Player::~Player()
{
}
//---------------------------------------------------------------------------
//! プレイヤーの死亡判定
//---------------------------------------------------------------------------
bool Player::IsDead() const
{
    return _isDead;
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool Player::OnInit()
{
    //==============================================================
    // アニメーションを読み込み
    //==============================================================
    constexpr Animation::Desc desc[] = {
        // 名前,  アニメーションファイル名
        { "Running", "model/YBot/Running.fbx" },
        { "Jump", "model/YBot/Jump.fbx" },
        { "Idle", "model/YBot/Idle.fbx" }
    };

    {
        _pAnimator->CreateAnimation(desc, std::size(desc));
    }

    //==============================================================
    // モデルの読み込み
    //==============================================================
    _pModelRenderer->CreateModel(desc[0].path_.data());
    _pModelRenderer->SetOffset(float3(0.0f, -0.6f, 0.0f));
    _pTransform->SetPosition(float3(0.f, 1.f, 0.0f));   //　地面と少し上に
    _pTransform->SetScale(float3(0.7f, 0.7f, 0.7f));
    return true;
}
//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void Player::OnUpdate()
{
    // y 座標 -10 以下 = 落ちる,　ゲームオーバーとなります
    if(_pTransform->GetPosition().f32[1] <= -5.0f) {
        _isDead = true;
    }
}
//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void Player::OnRender()
{
}
}   // namespace entity::go
