﻿//---------------------------------------------------------------------------
//!	@file	Ghost.h
//!	@brief	キャラクターゴーストのクラス
//---------------------------------------------------------------------------
#pragma once
#include "entity/gameObject/GameObject.h"

namespace component {
class Transform;
class ModelRenderer;
}   // namespace component

namespace entity::go {

class Ghost : public GameObject
{
public:
    Ghost();
    Ghost(const std::string& name);
    Ghost(const float3& pos);
    virtual ~Ghost();

public:
    //---------------------------------------------------------------------------
    // public 関数
    //---------------------------------------------------------------------------
    void SetGhostColor(const float4& color) { _ghostColor = color; };

	void SetToCleanness() { _isCleanness = true; };
    //---------------------------------------------------------------------------
    // public 変数
    //---------------------------------------------------------------------------

protected:
    //---------------------------------------------------------------------------
    // protected 変数
    //---------------------------------------------------------------------------

    //---------------------------------------------------------------------------
    // protected 関数
    //---------------------------------------------------------------------------
private:
    //---------------------------------------------------------------------------
    // private 変数
    //---------------------------------------------------------------------------

    //---------------------------------------------------------------------------
    // private 関数
    //---------------------------------------------------------------------------
    raw_ptr<component::ModelRenderer>    _pModelRenderer;      //!< ModelRenderer コンポーネント 参照

	float4 _ghostColor = math::ONE; //!< ゴーストの色

	bool _isCleanness = false; //!< 透明化アニメション
    //-----------------
    // 継承
    bool OnInit() override;     //!< 初期化
    void OnUpdate() override;   //!< 更新
    void OnRender() override;   //!< 描画
    //void Finalize() override;   //!< 解放
};
}   // namespace entity::go
