﻿//---------------------------------------------------------------------------
//!	@file	Cube.h
//!	@brief	キューブオブジェクト
//---------------------------------------------------------------------------
#pragma once
#include "GameObject.h"

namespace entity::go {
class Shpere : public GameObject
{
public:
    Shpere();                                                                 //!< コンストラクタ
    Shpere(const std::string& name);                                          //!< コンストラクタ
    Shpere(const float3& pos, f32 radians = 1.0f);                            //!< コンストラクタ
    Shpere(const std::string& name, const float3& pos, f32 radians = 1.0f);   //!< コンストラクタ

    ~Shpere();   //!< デストラクタ

    //---------------------------------------------------------------------------
    // public 関数
    //---------------------------------------------------------------------------
private:
    //---------------------------------------------------------------------------
    // private 変数
    //---------------------------------------------------------------------------
    // GPU関連
    shr_ptr<gpu::Buffer> _pVertexBuffer;   //!< 頂点バッファ
    shr_ptr<gpu::Buffer> _pIndexBuffer;    //!< インデックスバッファ
    u32                  _indexCount;      //!< インデックス数

    shr_ptr<gpu::Texture> _pTexture;   //!< SRV

    f32 _radians = 1.0f;   //!< サイズ
    //---------------------------------------------------------------------------
    // private 関数
    //---------------------------------------------------------------------------
    bool OnInit() override;     //!< 初期化
    void OnRender() override;   //!< 描画
    void OnUpdate() override;   //!< 更新

    bool InitGPU();   //!< GPU関連の初期化
};
}   // namespace entity::go
