﻿//---------------------------------------------------------------------------
//!	@file	Entity.h
//!	@brief	エンティティ
//---------------------------------------------------------------------------
#pragma once
#include "component/Component.h"
namespace manager {
class EntityManager;
};

namespace entity {

class Entity
{
    friend class manager::EntityManager;

public:
    Entity();
    Entity(const std::string& name);
    virtual ~Entity();

    void Release();   //!< エンティティマネジャーから外す
protected:
    //---------------------------------------------------------------------------
    // 関数
    //---------------------------------------------------------------------------
    bool Init();          //!< 初期化
    void Update();        //!< 更新
    void Render();        //!< 描画
    void RenderImgui();   //!< ImGui描画
    void Finalize();      //!< 解放

public:
    //---------------------------------------------------------------------------
    // コンポーネントのAccessor
    //---------------------------------------------------------------------------

    //!追加コンポーネント
    template<typename T, typename... TArgs>
    T* AddComponent(TArgs&&... args);

    //!ゲットコンポーネント
    template<class T>
    raw_ptr<T> GetComponent();

    // ChangeTo ->raw_ptr<T>
    // //!ゲットコンポーネント
    //   template<class T>
    //   T* GetComponent();

    //template<class T>
    //const T* GetComponent();

    //! 削除コンポーネント
    template<class T>
    T* RemoveComponent();

    //! 既定のコンポーネントすべて削除
    void RemoveAllComponents();

    std::string GetName();

protected:
    //---------------------------------------------------------------------------
    // 内部変数
    //---------------------------------------------------------------------------

    // 継承用
    virtual bool OnInit() { return true; };   //!< 初期化
    virtual void OnUpdate(){};                //!< 更新
    virtual void OnRender(){};                //!< 描画
    virtual void OnRenderImgui(){};           //!< ImGui描画
    virtual void OnFinalize(){};              //!< 解放

    bool _isEnable  = true;    //!< 有効にする(Update/Render)
    bool _isInited  = false;   //!< 初期化した
    bool _isRelease = false;   //!< エンティティマネジャーから外す
    bool _isRemoved = false;   //!< 完全に削除

    std::vector<raw_ptr<component::Component>> _components;   //!< コンポーネントリスト

    std::string _name;                                //!< 名前
    s32         _type = gameApp::EntityType_Entity;   //!< タイプ
};

//===========================================================================
//!	@defgroup	コンポーネントに関して
//===========================================================================
//@{

//---------------------------------------------------------------------------
//! コンポーネントを追加する
//---------------------------------------------------------------------------
template<typename T, typename... TArgs>
inline T* Entity::AddComponent(TArgs&&... args)
{
    T* newComponent(new T(std::forward<TArgs>(args)...));
    newComponent->SetOwner(this);
    _components.push_back(newComponent);
    return newComponent;
}

//---------------------------------------------------------------------------
//! コンポーネントを取得する
//---------------------------------------------------------------------------
template<class T>
inline raw_ptr<T> Entity::GetComponent()
{
    for(auto& component : _components) {
        if(dynamic_cast<T*>(component.get())) {
            return static_cast<T*>(component.get());
        }
    }
    return nullptr;
}

// Change TO -> raw_ptr<T>
//template<class T>
//inline T* Entity::GetComponent()
//{
//    for(auto& component : _components) {
//        if(dynamic_cast<T*>(component)) {
//            return static_cast<T*>(component);
//        }
//    }
//    return nullptr;
//}
//---------------------------------------------------------------------------
//! コンポーネントを削除する
//---------------------------------------------------------------------------
template<class T>
inline T* Entity::RemoveComponent()
{
    for(auto itr = _components.begin(); itr != _components.end(); ++itr) {
        if(*itr == dynamic_cast<T*>(*itr)) {
            T* component = *itr;

            _components.erase(itr);
            break;
        }
    }
}
// @}
}   // namespace entity
