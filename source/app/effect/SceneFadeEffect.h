﻿//---------------------------------------------------------------------------
//!	@file	SceneFadeEffect.h
//!	@brief	シーンフェードのエフェクトのクラス
//---------------------------------------------------------------------------
#pragma once
#include "Effect.h"

namespace effect {
class SceneFadeEffect : public IEffect
{
public:
    SceneFadeEffect()  = default;
    ~SceneFadeEffect() = default;

    // コピー禁止
    SceneFadeEffect(const SceneFadeEffect&) = delete;
    SceneFadeEffect& operator=(const SceneFadeEffect&) = delete;

    SceneFadeEffect(SceneFadeEffect&&) = delete;
    SceneFadeEffect& operator=(SceneFadeEffect&&) = delete;

public:
    //! 実体を取得
    static SceneFadeEffect* Instance();

    //! 初期化
    bool InitAll();

    //---------------------------------------------------------------------------
    //! メトリックス / Matrix
    //---------------------------------------------------------------------------
    void SetWorldViewProjMatrix(matrix world, matrix view, matrix proj);
    void SetWorldViewProjMatrix(matrix wvp);

    //---------------------------------------------------------------------------
    //! レンダー / Render
    //---------------------------------------------------------------------------
    void SetRenderDefault();   //!< ライン描画用

    //---------------------------------------------------------------------------
    //! テクスチャー設置　/ Texture
    //---------------------------------------------------------------------------
    void SetTexture(raw_ptr<dx11::Texture> texture);

    void SetFadeAmount(f32 fadeAmount);

    void Apply() override;   //!< 設定を適用する

private:
    class SceneFadeEffectImpl* _pImpl;   //!< 実体
};

effect::SceneFadeEffect* SceneFadeEffectIns();   //!< 実体を取得

}   // namespace effect
