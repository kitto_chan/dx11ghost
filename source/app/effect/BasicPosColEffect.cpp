﻿//---------------------------------------------------------------------------
//!	@file	BasicPosColEffect.h
//!	@brief	簡単なPos+Colのエフェクトのクラス
//---------------------------------------------------------------------------
#include "BasicPosColEffect.h"

namespace effect {
class BasicPosColEffectImpl final : public BasicPosColEffect
{
public:
    struct WorldCB
    {
        matrix world;
    };

    struct CameraCB
    {
        matrix view;
        matrix proj;
    };

public:
    BasicPosColEffectImpl()  = default;
    ~BasicPosColEffectImpl() = default;

    //! 実体を取得
    static BasicPosColEffectImpl* Instance();

public:
    //---------------------------------------------------------------------------
    //! 定数バッファ
    //---------------------------------------------------------------------------
    CBuffer<0, WorldCB>  _cbWorld;    //! ワールド定数バッファ
    CBuffer<1, CameraCB> _cbCamera;   //! カメラ定数バッファ
    //---------------------------------------------------------------------------
    //! シェーダ
    //---------------------------------------------------------------------------
    shr_ptr<gpu::Shader> _pVShader;   //!< 頂点シェーダ
    shr_ptr<gpu::Shader> _pPShader;   //!< ピクセルシェーダシェーダ

    //---------------------------------------------------------------------------
    //! 入力レイアウト
    //---------------------------------------------------------------------------
    shr_ptr<gpu::InputLayout> _pILVertexPos;   //!< 入力レイアウト

    bool _isDirty = false;   //!< ダーティフラグ
};

//===========================================================================
// class BasicPosColEffect
//===========================================================================

//---------------------------------------------------------------------------
//!	初期化
//---------------------------------------------------------------------------
bool BasicPosColEffect::InitAll()
{
    pImpl = new BasicPosColEffectImpl();

    pImpl->_pVShader = dx11::createShader("gameSource/basicPosCol/BasicPosCol_VS.hlsl", "VS", "vs_5_0");
    if(!pImpl->_pVShader)
        return false;

    pImpl->_pILVertexPos = dx11::createInputLayout(vertex::VertexPosColor::inputLayout, std::size(vertex::VertexPosColor::inputLayout));

    pImpl->_pPShader = dx11::createShader("gameSource/basicPosCol/BasicPosCol_PS.hlsl", "PS", "ps_5_0");
    if(!pImpl->_pPShader)
        return false;

    // バインド
    pImpl->_cbWorld.Bind("WorldCB");

    //! 動的バッファを生成
    pImpl->_cbWorld.CreateDynamicBuffer();
    pImpl->_cbCamera.CreateDynamicBuffer();

    SetWorldMatrix(math::identity());
    return true;
}

//---------------------------------------------------------------------------
//!	ワールド行列をセット
//---------------------------------------------------------------------------
void BasicPosColEffect::SetWorldMatrix(matrix W)
{
    auto& cBuffer      = pImpl->_cbWorld;
    cBuffer.data.world = W;
    pImpl->_isDirty = cBuffer._isDirty = true;
}
//---------------------------------------------------------------------------
//!	ビュー行列をセット
//---------------------------------------------------------------------------
void BasicPosColEffect::SetViewMatrix(matrix v)
{
    auto& cBuffer     = pImpl->_cbCamera;
    cBuffer.data.view = v;
    pImpl->_isDirty = cBuffer._isDirty = true;
}
//---------------------------------------------------------------------------
//!	投影行列をセット
//---------------------------------------------------------------------------
void BasicPosColEffect::SetProjMatrix(matrix p)
{
    auto& cBuffer     = pImpl->_cbCamera;
    cBuffer.data.proj = p;
    pImpl->_isDirty = cBuffer._isDirty = true;
}
//---------------------------------------------------------------------------
//!	ライン描画用
//---------------------------------------------------------------------------
void BasicPosColEffect::SetRenderLine()
{
    dx11::vs::setShader(pImpl->_pVShader);
    dx11::ps::setShader(pImpl->_pPShader);
    dx11::setInputLayout(pImpl->_pILVertexPos);
}
//---------------------------------------------------------------------------
//!	設定を適用する
//---------------------------------------------------------------------------
void BasicPosColEffect::Apply()
{
    if(pImpl->_isDirty) {
        pImpl->_isDirty = true;
        pImpl->_cbWorld.SetnUpdateBuffer();
    }
}
//---------------------------------------------------------------------------
//! 実体を取得
//---------------------------------------------------------------------------
effect::BasicPosColEffectImpl* BasicPosColEffectImpl::Instance()
{
    static effect::BasicPosColEffectImpl Instance;   // gpu::Renderのシングルトン実体
    return &Instance;
}

//------------------------------------------------------------------------
//! BasicEffect 管理クラスを取得
//------------------------------------------------------------------------
effect::BasicPosColEffect* BasicPosColEffect::Instance()
{
    return BasicPosColEffectImpl::Instance();
}

effect::BasicPosColEffect* BasicPosColEffectIns()
{
    return BasicPosColEffect::Instance();
}
}   // namespace effect
