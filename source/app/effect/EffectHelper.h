﻿//---------------------------------------------------------------------------
//!	@file	EffectHelper.h
//!	@brief	エフェクトクラスの通用関数
//---------------------------------------------------------------------------
#pragma once
namespace effect {
struct CBufferBase
{
    //! コンストラクタ
    CBufferBase(){};
    ~CBufferBase() = default;

    //! ダーティーフラグ
    bool        _isDirty = false;
    std::string _cbName;

    //! D3D gpuバッファ
    std::shared_ptr<dx11::Buffer> _buffer;

    //!　仮想関数
    virtual bool CreateDynamicBuffer()           = 0;   //!< 動的な定数バッファを生成
    virtual void UpdateBuffer()                  = 0;   //!< バッファ更新
    virtual void SetnUpdateBuffer()              = 0;   //!< 自動的バッファを見つけて更新
    virtual void Bind(const std::string& cbName) = 0;   //!< バッファをバインド
    virtual void BindVS()                        = 0;   //!< 頂点バッファをバインド
    virtual void BindPS()                        = 0;   //!< ピクセルバッファをバインド

    /* TODO
    virtual void BindHS() = 0;
    virtual void BindDS() = 0;
    virtual void BindGS() = 0;
    virtual void BindCS() = 0;
    */
};

template<u32 startSlot, class T>
struct CBuffer : CBufferBase
{
    T data;

    CBuffer()
    : CBufferBase()
    , data()
    {
    }

    //! 動的な定数バッファを生成
    bool CreateDynamicBuffer() override
    {
        _buffer = gpu::createDynamicBuffer<T>(D3D11_BIND_CONSTANT_BUFFER);
        if(_buffer == nullptr) {
            ASSERT_MESSAGE(false, "CreateDynamicBuffer Failed");
            return false;
        }
        return true;
    };

    //! バッファ更新
    void UpdateBuffer() override
    {
        if(!_isDirty) return;

        _buffer->update(data);
        _isDirty = false;
    }

    //! 頂点バッファをバインド
    void BindVS() override
    {
        ID3D11Buffer* constantBuffers[]{ *_buffer };
        gpu::immediateContext()->VSSetConstantBuffers(startSlot, 1, constantBuffers);
    }

    //! ピクセルバッファをバインド
    void BindPS() override
    {
        ID3D11Buffer* constantBuffers[]{ *_buffer };
        dx11::immediateContext()->PSSetConstantBuffers(startSlot, 1, constantBuffers);
    }

    //! 定数バッファの名前をバインドする
    void Bind(const std::string& cbName) override
    {
        _cbName = cbName;
    }

    //! 自動的バッファを見つけて更新
    void SetnUpdateBuffer() override
    {
        if(!_isDirty) return;

        ASSERT_MESSAGE(!_cbName.empty(), "定数バッファ名前はバインドしてない");
        dx11::setConstantBuffer(_cbName.c_str(), data);
        _isDirty = false;
    }
};
}   // namespace effect
