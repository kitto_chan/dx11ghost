﻿//---------------------------------------------------------------------------
//!	@file	BasicPosColEffect.h
//!	@brief	簡単なPos+Colのエフェクトのクラス
//---------------------------------------------------------------------------
#pragma once
#include "Effect.h"
#include "EffectHelper.h"

namespace effect {
class BasicPosColEffect : public IEffect
{
public:
    BasicPosColEffect()  = default;
    ~BasicPosColEffect() = default;

    // コピー禁止
    BasicPosColEffect(const BasicPosColEffect&) = delete;
    BasicPosColEffect& operator=(const BasicPosColEffect&) = delete;

    BasicPosColEffect(BasicPosColEffect&&) = delete;
    BasicPosColEffect& operator=(BasicPosColEffect&&) = delete;

public:
    //! 実体を取得
    static BasicPosColEffect* Instance();

    //! 初期化
    bool InitAll();

    //---------------------------------------------------------------------------
    //! メトリックス / Matrix
    //---------------------------------------------------------------------------
    void SetWorldMatrix(matrix w);   //!< ワールド行列をセット
    void SetViewMatrix(matrix v);    //!< ビュー行列をセット
    void SetProjMatrix(matrix p);    //!< 投影行列をセット

    //---------------------------------------------------------------------------
    //! レンダー / Render
    //---------------------------------------------------------------------------
    void SetRenderLine();   //!< ライン描画用

    void Apply() override;   //!< 設定を適用する

private:
    class BasicPosColEffectImpl* pImpl;   //!< 実体
};

effect::BasicPosColEffect* BasicPosColEffectIns();   //!< 実体を取得

}   // namespace effect
