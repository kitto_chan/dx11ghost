﻿//---------------------------------------------------------------------------
//!	@file	StaticSkyBox.h
//!	@brief	スカイボックス
//---------------------------------------------------------------------------
#pragma once
#include "effect/Effect.h"

namespace effect {

class StaticSkyBoxEffect : public IEffect
{
public:
    StaticSkyBoxEffect()          = default;
    virtual ~StaticSkyBoxEffect() = default;

    // コピー禁止
    StaticSkyBoxEffect(const StaticSkyBoxEffect&) = delete;
    StaticSkyBoxEffect& operator=(const StaticSkyBoxEffect&) = delete;

    StaticSkyBoxEffect(StaticSkyBoxEffect&&) = default;
    StaticSkyBoxEffect& operator=(StaticSkyBoxEffect&&) = default;

    //! 実体を取得
    static StaticSkyBoxEffect* Instance();

    //! 初期化
    bool InitAll();

    //---------------------------------------------------------------------------
    //! レンダー / Render
    //---------------------------------------------------------------------------
    void SetRenderDefault();

    //---------------------------------------------------------------------------
    //! 行列 / Matrix
    //---------------------------------------------------------------------------
    void SetWorldViewProjMatrix(matrix world, matrix view, matrix proj);
    void SetWorldViewProjMatrix(matrix wvp);

    //---------------------------------------------------------------------------
    //! テクスチャー設置　/ Texture
    //---------------------------------------------------------------------------
    void SetTextureCube(std::shared_ptr<dx11::Texture> textureCube);

    void Apply() override;

private:
    class StaticSkyBoxEffectImpl* _pImpl;
};
effect::StaticSkyBoxEffect* StaticSkyBoxEffectIns();
}   // namespace effect
