﻿//---------------------------------------------------------------------------
//!	@file	SceneFadeEffect.h
//!	@brief	シーンフェードのエフェクトのクラス
//---------------------------------------------------------------------------
#include "SceneFadeEffect.h"

namespace effect {
struct WVP_CB
{
    matrix worldViewProj;
};

struct FadeCB
{
    f32    fadeAmount = 0.0f;
    float3 pad;
};
class SceneFadeEffectImpl : public SceneFadeEffect
{
private:


public:
    SceneFadeEffectImpl()  = default;
    ~SceneFadeEffectImpl() = default;

    static SceneFadeEffectImpl* Instance();

public:
    //---------------------------------------------------------------------------
    //! 定数バッファ
    //---------------------------------------------------------------------------
    CBuffer<0, WVP_CB> _wvpCB;
    CBuffer<1, FadeCB> _fadeCB;
    //---------------------------------------------------------------------------
    //! シェーダ
    //---------------------------------------------------------------------------
    shr_ptr<gpu::Shader> _pVShader;   //!< 頂点シェーダ
    shr_ptr<gpu::Shader> _pPShader;   //!< ピクセルシェーダ

    //---------------------------------------------------------------------------
    //! 入力レイアウト
    //---------------------------------------------------------------------------
    shr_ptr<gpu::InputLayout> _pILVertexPos;

    //---------------------------------------------------------------------------
    //! テクスチャー
    //---------------------------------------------------------------------------
    raw_ptr<gpu::Texture> _pTextureSRV;   //!< SRV

    //---------------------------------------------------------------------------
    //　変数
    //---------------------------------------------------------------------------
    bool _isDirty = false;
};

bool SceneFadeEffect::InitAll()
{
    _pImpl            = SceneFadeEffectImpl::Instance();
    _pImpl->_pVShader = gpu::createShader("gameSource/SceneFade/SceneFade_VS.hlsl", "VS", "vs_5_0");   // 頂点シェーダー
    if(!_pImpl->_pVShader) {
        return false;
    }
    _pImpl->_pILVertexPos = gpu::createInputLayout(vertex::VertexPosTex::inputLayout, std::size(vertex::VertexPosTex::inputLayout));

    //　シェーダー
    _pImpl->_pPShader = gpu::createShader("gameSource/SceneFade/SceneFade_PS.hlsl", "PS", "ps_5_0");   // 頂点シェーダー
    if(!_pImpl->_pPShader) {
        return false;
    }

    // バインド
    _pImpl->_wvpCB.Bind("WVP_CB");
    _pImpl->_fadeCB.Bind("FadeCB");
    // 動的バッファを生成
    _pImpl->_wvpCB.CreateDynamicBuffer();
    _pImpl->_fadeCB.CreateDynamicBuffer();

    return true;
}
void SceneFadeEffect::SetRenderDefault()
{
    gpu::setInputLayout(_pImpl->_pILVertexPos);
    gpu::vs::setShader(_pImpl->_pVShader);
    gpu::ps::setShader(_pImpl->_pPShader);
    gpu::ps::setSamplerState(0, render::RenderStatesIns()->_pSSLinearWrap);
    gpu::setRasterizerState(nullptr);
    gpu::setDepthStencilState(nullptr);
    gpu::setBlendState(nullptr);
}
void SceneFadeEffect::SetWorldViewProjMatrix(matrix world, matrix view, matrix proj)
{
    SetWorldViewProjMatrix(mul(mul(world, view), proj));
}
void SceneFadeEffect::SetWorldViewProjMatrix(matrix wvp)
{
    auto& cBuffer              = _pImpl->_wvpCB;
    cBuffer.data.worldViewProj = wvp;
    _pImpl->_isDirty = cBuffer._isDirty = true;
}
void SceneFadeEffect::SetTexture(raw_ptr<dx11::Texture> texture)
{
    _pImpl->_pTextureSRV = texture;
}

void SceneFadeEffect::SetFadeAmount(f32 fadeAmount)
{
    auto& cBuffer           = _pImpl->_fadeCB;
    cBuffer.data.fadeAmount = fadeAmount;
    _pImpl->_isDirty = cBuffer._isDirty = true;
}

void SceneFadeEffect::Apply()
{
    gpu::ps::setTexture(0, _pImpl->_pTextureSRV);

    //!　更新
    if(_pImpl->_isDirty) {
        _pImpl->_isDirty = false;

        _pImpl->_wvpCB.SetnUpdateBuffer();
        _pImpl->_fadeCB.SetnUpdateBuffer();
    }
}
//---------------------------------------------------------------------------
//! 実体を取得
//---------------------------------------------------------------------------
effect::SceneFadeEffectImpl* SceneFadeEffectImpl::Instance()
{
    static effect::SceneFadeEffectImpl Instance;   // gpu::Renderのシングルトン実体
    return &Instance;
}

//------------------------------------------------------------------------
//! CubeMapEffect 管理クラスを取得
//------------------------------------------------------------------------
effect::SceneFadeEffect* SceneFadeEffect::Instance()
{
    return SceneFadeEffectImpl::Instance();
}

effect::SceneFadeEffect* SceneFadeEffectIns()
{
    return SceneFadeEffect::Instance();
}
}   // namespace effect
