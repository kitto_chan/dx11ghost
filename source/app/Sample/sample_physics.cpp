﻿////---------------------------------------------------------------------------
////!	@file	sample_physics.cpp
////!	@brief	物理シミュレーションサンプル
////---------------------------------------------------------------------------
//#include "sample_physics.h"
//namespace scene {
//
//namespace {
//
////--------------------------------------------------------------
//// 内部変数
////--------------------------------------------------------------
//
////--------
//// モデル描画
//std::shared_ptr<Model> modelBox_;       //!< モデル(木箱)
//std::shared_ptr<Model> modelBall_;      //!< モデル(サッカーボール)
//std::shared_ptr<Model> modelOilDrum_;   //!< モデル(ドラム缶)
//CameraDebug            camera_;         //!< デバッグカメラ
//
////--------
//// 物理シミュレーション
//std::unique_ptr<physics::RigidBody>              groundRigidBody_;   //!< 地面用の剛体
//std::vector<std::unique_ptr<physics::RigidBody>> rigidBodies_;       //!< 剛体
//
////--------
//// フラグ類
//bool isDrawDebugMatrix_ = false;   //!< 行列をデバッグ描画
//bool isDrawDebug_       = false;   //!< 物理シミュレーションエンジンデバッグ描画
//
////--------------------------------------------------------------
//// カメラ用定数バッファ
//// 定数バッファ構造体はシェーダー側と同じデーター構造にする必要あり
////--------------------------------------------------------------
//struct CameraCB
//{
//    matrix matView_ = math::identity();   //!< ビュー行列
//    matrix matProj_ = math::identity();   //!< 投影行列
//};
//
//CameraCB cameraCb_{};   //!< カメラ用定数バッファ
//
//}   // namespace
//
////--------------------------------------------------------------------------
////! デストラクタ
////! デストラクタ
////--------------------------------------------------------------------------
//ScenePhysics::~ScenePhysics()
//{
//    //==========================================================
//    // 物理シミュレーション解放
//    //==========================================================
//    groundRigidBody_.reset();
//    rigidBodies_.clear();
//}
//
////--------------------------------------------------------------------------
////! 初期化
////--------------------------------------------------------------------------
//bool ScenePhysics::Init()
//{
//    //----------------------------------------------------------
//    // モデルの読み込み
//    //----------------------------------------------------------
//    if(!modelBox_) {
//        modelBox_ = createModel("wooden_box/wooden_box.fbx");
//    }
//    if(!modelBall_) {
//        modelBall_ = createModel("soccer_ball/SOCCER_Ball.FBX");
//    }
//    if(!modelOilDrum_) {
//        modelOilDrum_ = createModel("oil_drum/Oil_Drum.fbx");
//    }
//
//    //----------------------------------------------------------
//    // カメラの初期位置設定
//    //----------------------------------------------------------
//    camera_.setPosition(float3(3.0f, 2.0f, 12.0f));   // 位置座標
//    camera_.setLookAt(float3(0.0f, 3.0f, 0.0f));      // 注視点
//
//    //----------------------------------------------------------
//    // 地面の作成
//    //----------------------------------------------------------
//    {
//        const f32    mass     = 0.0f;                                         // 質量。質量0の場合は【静的オブジェクト】
//        const matrix matWorld = math::translate(float3(0.0f, -0.5f, 0.0f));   // 初期姿勢
//        const auto   shape    = shape::Box(float3(64.0f, 0.5f, 64.0f));       // 形状
//
//        // 剛体を作成
//        groundRigidBody_ = physics::createRigidBody(mass, matWorld, shape);
//    }
//
//    //-------------------------------------------------------
//    // 複数の剛体を並べて作成
//    //-------------------------------------------------------
//    auto createRigidBody = [](const matrix& matWorld) {
//        std::unique_ptr<physics::RigidBody> rigidBody;
//
//        switch(rand() % 3) {
//            case 0:
//            {
//                // 木箱
//                constexpr f32 mass     = 15.0f;   // 質量 15kg【動的オブジェクト】
//                constexpr f32 halfSize = 0.38f;   // 木箱サイズ
//
//                const auto shape = shape::Box(float3(halfSize, halfSize, halfSize));   // 形状
//
//                rigidBody = physics::createRigidBody(mass, matWorld, shape);
//                rigidBody->nativeRigidBody()->setFriction(0.5f);      // 摩擦係数
//                rigidBody->nativeRigidBody()->setRestitution(0.2f);   // 跳ね返り係数
//            } break;
//            case 1:
//            {
//                // サッカーボール
//                constexpr f32 massFactor = 4.0f;                // 安定のために意図的に重くする
//                constexpr f32 mass       = 0.5f * massFactor;   // 質量 500g【動的オブジェクト】
//
//                const auto shape = shape::Sphere(0.12f);   // 形状
//
//                rigidBody = physics::createRigidBody(mass, matWorld, shape);
//                rigidBody->nativeRigidBody()->setFriction(0.25f);           // 摩擦係数
//                rigidBody->nativeRigidBody()->setRestitution(0.9f);         // 跳ね返り係数(ボール用に少し高めに設定)
//                rigidBody->nativeRigidBody()->setRollingFriction(0.01f);    // 転がり摩擦
//                rigidBody->nativeRigidBody()->setSpinningFriction(0.01f);   // スピン摩擦
//            } break;
//            case 2:
//            {
//                // ドラム缶
//                constexpr f32 mass             = 20.0f;    // 質量 20kg【動的オブジェクト】
//                constexpr f32 radius           = 0.375f;   // 半径
//                constexpr f32 heightHalfExtent = 0.54f;    // 高さの半分
//
//                const auto shape = shape::Cylinder(radius, heightHalfExtent);   // 形状
//
//                rigidBody = physics::createRigidBody(mass, matWorld, shape);
//                rigidBody->nativeRigidBody()->setFriction(0.25f);           // 摩擦係数
//                rigidBody->nativeRigidBody()->setRestitution(0.75f);        // 跳ね返り係数
//                rigidBody->nativeRigidBody()->setRollingFriction(0.01f);    // 転がり摩擦
//                rigidBody->nativeRigidBody()->setSpinningFriction(0.01f);   // スピン摩擦
//            } break;
//        }
//        return rigidBody;
//    };
//
//    constexpr s32 ARRAY_SIZE_X = 3;
//    constexpr s32 ARRAY_SIZE_Y = 8;
//    constexpr s32 ARRAY_SIZE_Z = 3;
//    for(s32 i = 0; i < ARRAY_SIZE_X; i++) {
//        for(s32 j = 0; j < ARRAY_SIZE_Y; j++) {
//            for(s32 k = 0; k < ARRAY_SIZE_Z; k++) {
//                f32 x = static_cast<f32>(i - ARRAY_SIZE_X / 2);
//                f32 y = static_cast<f32>(j);
//                f32 z = static_cast<f32>(k - ARRAY_SIZE_Z / 2);
//
//                // 初期座標
//                float3 position;
//                position.x = 1.2f * x;
//                position.y = 1.2f * y + 2.0f;
//                position.z = 1.2f * z;
//
//                // ランダムで位置をずらす
//                position.x += static_cast<f32>(rand() & 7) * 0.02f;
//                position.z += static_cast<f32>(rand() & 7) * 0.02f;
//
//                // 剛体を作成してリストに登録
//
//                const matrix matWorld = math::translate(position);   // 初期姿勢
//
//                std::unique_ptr<physics::RigidBody> rigidBody = createRigidBody(matWorld);
//
//                rigidBodies_.emplace_back(std::move(rigidBody));
//            }
//        }
//    }
//
//    return true;
//}
//
////--------------------------------------------------------------------------
////! 更新
////! @attention ここでは描画発行は行わないこと
////--------------------------------------------------------------------------
//void ScenePhysics::Update([[maybe_unused]] f32 deltaTime)
//{
//    // カメラ行列
//    camera_.update(deltaTime);
//    cameraCb_.matView_ = camera_.view();
//    cameraCb_.matProj_ = camera_.projection();
//
//    //-----------------------------------------------------------------
//    // モデルの更新
//    //-----------------------------------------------------------------
//    modelBox_->update();
//    modelBall_->update();
//    modelOilDrum_->update();
//
//    //----------------------------------------------------------
//    // ImGui ウィンドウ表示テスト
//    //----------------------------------------------------------
//    ImGui::SetNextWindowSize(ImVec2(320, 180), ImGuiCond_Once);
//    ImGui::Begin(u8"物理シミュレーション");
//    {
//        ImGui::Text(u8"【物理シミュレーション剛体配置サンプル】");
//        ImGui::Separator();
//
//        ImGui::Checkbox(u8"行列をデバッグ描画", &isDrawDebugMatrix_);
//        ImGui::Checkbox(u8"物理シミュレーションエンジンデバッグ描画", &isDrawDebug_);
//    }
//    ImGui::End();
//}
//
////--------------------------------------------------------------------------
////! 描画
////! @attention ここでは移動更新は行わないこと
////--------------------------------------------------------------------------
//void ScenePhysics::Render(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture)
//{
//    //----------------------------------------------------------
//    // 描画バッファ設定
//    //----------------------------------------------------------
//    gpu::setRenderTarget(0, colorTexture);
//    gpu::setDepthStencil(depthTexture);
//
//    // カラーとデプスバッファをクリア
//    gpu::clearColor(colorTexture, float4(0.5f, 0.5f, 0.8f, 1.0f));   // カラーバッファ
//    gpu::clearDepth(depthTexture, 1.0f);                             // デプスバッファ
//
//    // 表裏カリングOFF
//    gpu::setRasterizerState(gpu::commonStates().CullNone());
//
//    //----------------------------------------------------------
//    // 描画準備
//    //----------------------------------------------------------
//
//    // ワールド行列を単位行列で初期化して背景として描画
//    gpu::setConstantBuffer("WorldCB", matrix::identity());   // GPUへ変更を反映
//
//    // カメラ反映
//    gpu::setConstantBuffer("CameraCB", cameraCb_);
//
//    //----------------------------------------------------------
//    // グリッドの描画
//    //----------------------------------------------------------
//    {
//        constexpr f32 gridSize = 64.0f;   // グリッドの大きさ
//
//        // 軸の描画
//        debug::drawLineF(float3(-gridSize, 0.0f, 0.0f), float3(+gridSize, 0.0f, 0.0f), Color(255, 0, 0));   // X軸
//        debug::drawLineF(float3(0.0f, -gridSize, 0.0f), float3(0.0f, +gridSize, 0.0f), Color(0, 255, 0));   // Y軸
//        debug::drawLineF(float3(0.0f, 0.0f, -gridSize), float3(0.0f, 0.0f, +gridSize), Color(0, 0, 255));   // Z軸
//
//        // グリッドの描画
//        for(s32 i = -64; i <= +64; ++i) {
//            // 中心線は軸と重なるためスキップ
//            if(i == 0)
//                continue;
//
//            debug::drawLineF(float3(i, 0.0f, -gridSize), float3(i, 0.0f, +gridSize), Color(192, 192, 192));
//            debug::drawLineF(float3(-gridSize, 0.0f, i), float3(+gridSize, 0.0f, i), Color(192, 192, 192));
//        }
//    }
//
//    //==========================================================
//    // 物理シミュレーションの結果を描画
//    //==========================================================
//    for(auto& rigidBody : rigidBodies_) {
//        //------------------------------------------------------
//        // モデルの描画
//        //------------------------------------------------------
//        const matrix matWorld = rigidBody->worldMatrix();   // 剛体からワールド行列を取得
//
//        switch(rigidBody->shapeType()) {
//            case SPHERE_SHAPE_PROXYTYPE:
//            {
//                // [Sphere] サッカーボールモデル
//                modelBall_->setWorldMatrix(matWorld);
//                modelBall_->render();
//            } break;
//            case BOX_SHAPE_PROXYTYPE:
//            {
//                // [Box] 木箱モデル
//                modelBox_->setWorldMatrix(matWorld);
//                modelBox_->render();
//            } break;
//            case CYLINDER_SHAPE_PROXYTYPE:
//            {
//                // [Cylinder] ドラム缶
//                matrix m = mul(math::translate(float3(0.0f, -0.54f, 0.0f)), matWorld);   // 表示位置をずらす調整
//                modelOilDrum_->setWorldMatrix(m);
//                modelOilDrum_->render();
//            } break;
//            default: break;
//        }
//
//        //------------------------------------------------------
//        // 行列のデバッグ描画
//        //------------------------------------------------------
//        if(isDrawDebugMatrix_) {
//            debug::drawMatrix(matWorld);
//        }
//    }
//
//    //==========================================================
//    // 物理シミュレーションエンジンのデバッグ描画
//    //==========================================================
//    if(isDrawDebug_) {
//        physics::PhysicsEngine::instance()->renderDebug();
//    }
//}
//
////---------------------------------------------------------------------------
//// シーンの生成
////! @note テンプレートの特殊化で実装します
////---------------------------------------------------------------------------
//template<>
//std::unique_ptr<IScene> createScene<ScenePhysics>()
//{
//    return std::make_unique<ScenePhysics>();
//}
//}   // namespace scene