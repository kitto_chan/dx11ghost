﻿////---------------------------------------------------------------------------
////!	@file	sample_collision.h
////!	@brief	衝突判定サンプル
////---------------------------------------------------------------------------
//#pragma once
//
////===========================================================================
////! 衝突判定サンプル用シーン
////===========================================================================
//namespace scene {
//
//class SceneCollision final : public IScene
//{
//public:
//    //! コンストラクタ
//    SceneCollision() = default;
//
//    //! デストラクタ
//    virtual ~SceneCollision();
//
//    //! 初期化
//    virtual bool Init() override;
//
//    //! 更新
//    //! @param  [in]    deltaTime   進行時間(単位:秒)
//    virtual void Update(f32 deltaTime) override;
//
//    //! 描画
//    virtual void Render(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture) override;
//};
//}   // namespace scene