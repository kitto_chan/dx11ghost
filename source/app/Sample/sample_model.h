﻿////---------------------------------------------------------------------------
////!	@file	sample_model.h
////!	@brief	モデル描画サンプル
////---------------------------------------------------------------------------
//#pragma once
//namespace scene {
//
////===========================================================================
////! モデル描画サンプル用シーン
////===========================================================================
//class SceneModel final : public IScene
//{
//public:
//    //! コンストラクタ
//    SceneModel() = default;
//
//    //! デストラクタ
//    virtual ~SceneModel() = default;
//
//    //! 初期化
//    virtual bool Init() override;
//
//    //! 更新
//    //! @param  [in]    deltaTime   進行時間(単位:秒)
//    virtual void Update(f32 deltaTime) override;
//
//    //! 描画
//    virtual void Render(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture) override;
//};
//}   // namespace scene