﻿//---------------------------------------------------------------------------
//!	@file	scene.cpp
//!	@brief	シーン管理
//---------------------------------------------------------------------------
//#include "scene.h"
//#include <bitset>
//namespace scene {

//class SceneDraw;        // 描画サンプル
//class SceneModel;       // モデル描画サンプル
//class ScenePhysics;     // 物理シミュレーションサンプル
//class SceneCollision;   // 衝突判定サンプル
//
//extern template std::unique_ptr<IScene> createScene<SceneDraw>();
//extern template std::unique_ptr<IScene> createScene<SceneModel>();
//extern template std::unique_ptr<IScene> createScene<ScenePhysics>();
//extern template std::unique_ptr<IScene> createScene<SceneCollision>();
//
//namespace {
//
//std::unique_ptr<IScene> scene_;
//std::unique_ptr<IScene> sceneNext_;
//
//}   // namespace
//
////--------------------------------------------------------------------------
//// 初期化
////--------------------------------------------------------------------------
//bool onInitialize([[maybe_unused]] u32 width, [[maybe_unused]] u32 height)
//{
//    //----------------------------------------------------------
//    // 初期シーンの作成
//    //----------------------------------------------------------
//    sceneNext_ = createScene<SceneDraw>();
//    if(!sceneNext_)
//        return false;
//
//    return true;
//}
//
////--------------------------------------------------------------------------
////! 更新
////! @attention ここでは描画発行は行わないこと
////--------------------------------------------------------------------------
//void onUpdate(f32 deltaTime)
//{
//    printf_s("%f    %f\n", deltaTime, timer::TimerIns()->DeltaTime());
//
//    //----------------------------------------------------------
//    // キー入力チェック
//    //----------------------------------------------------------
//    std::bitset<256>        keyCurrent;   // 押しているかどうか
//    std::bitset<256>        keyTrigger;   // 押した瞬間
//    static std::bitset<256> keyLast;      // 押しているかどうか(前回の値)
//
//    {
//        for(u32 i = 0; i < keyCurrent.size(); ++i) {
//            // 押しているかどうかを取得
//            if(GetKeyState(i) & 0x8000) {
//                keyCurrent.set(i);
//            }
//            else {
//                keyCurrent.reset(i);
//            }
//            // 押した瞬間を検出
//            keyTrigger[i] = (keyCurrent[i] ^ keyLast[i]) && keyCurrent[i];
//        }
//        // 次のために現在の結果を保存
//        keyLast = keyCurrent;
//    }
//
//    //----------------------------------------------------------
//    // シーン切り替え
//    //----------------------------------------------------------
//    if(application::isActive() && !keyCurrent[VK_MENU]) {   // ウィンドウがアクティブかつAltキー同時押しでない場合
//
//        if(keyTrigger[VK_F1]) {
//            // F1キー
//            sceneNext_ = createScene<SceneDraw>();
//        }
//        else if(keyTrigger[VK_F2]) {
//            // F2キー
//            sceneNext_ = createScene<SceneModel>();
//        }
//        else if(keyTrigger[VK_F3]) {
//            // F3キー
//            sceneNext_ = createScene<ScenePhysics>();
//        }
//        else if(keyTrigger[VK_F4]) {
//            // F4キー
//            sceneNext_ = createScene<SceneCollision>();
//        }
//    }
//
//    //----------------------------------------------------------
//    // シーン遷移
//    //----------------------------------------------------------
//    if(sceneNext_) {
//        scene_ = std::move(sceneNext_);
//
//        // 初期化
//        if(!scene_->Init()) {
//            scene_.reset();   // 解放
//        }
//    }
//
//    //----------------------------------------------------------
//    // 更新実行
//    //----------------------------------------------------------
//    if(scene_) {
//        scene_->Update(deltaTime);
//    }
//}
//
////--------------------------------------------------------------------------
////! 描画
////! @attention ここでは移動更新は行わないこと
////--------------------------------------------------------------------------
//void onRender(raw_ptr<gpu::Texture> colorTexture, raw_ptr<gpu::Texture> depthTexture)
//{
//    if(scene_) {
//        scene_->Render(colorTexture, depthTexture);
//    }
//}
//
////--------------------------------------------------------------------------
////! 解放
////--------------------------------------------------------------------------
//void onFinalize()
//{
//    scene_.reset();
//    sceneNext_.reset();
//}
//}   // namespace scene