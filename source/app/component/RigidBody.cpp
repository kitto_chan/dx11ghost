﻿//---------------------------------------------------------------------------
//!	@file	RigidBody.cpp
//!	@brief	ゲームオブジェクト を物理特性によって制御する事ができるようになります。\n
//!			BulletPhysics物理エンジンを通して他のオブジェクトと相互作用させるためには、\n
//!			ゲームオブジェクトにリジッドボディを加える必要があります。
//---------------------------------------------------------------------------
#include "RigidBody.h"
#include "Collider.h"
#include "Transform.h"
#include "entity/Entity.h"

namespace component {
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
RigidBody::RigidBody()
{
    _name = _NAME;
}
//---------------------------------------------------------------------------
//! コンストラクタ
//! @param	[in]	mass 質量
//---------------------------------------------------------------------------
RigidBody::RigidBody(f32 mass, bool isKinematic)
{
    _name        = _NAME;
    _mass        = mass;
    _isKinematic = isKinematic;
}

//---------------------------------------------------------------------------
//! デストラクタ
//---------------------------------------------------------------------------
RigidBody::~RigidBody()
{
    _rigidbody.reset();
}

//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool RigidBody::OnInit()
{
    //==========================================================
    // 必要なコンポーネントを取得
    //==========================================================
    _pTransform = _pOwner->GetComponent<Transform>();
    _pCollider  = _pOwner->GetComponent<Collider>();

    ASSERT_MESSAGE(_pTransform, "RigidBodyはTransformが必要です");
	ASSERT_MESSAGE(_pCollider, "RigidBodyは_pColliderが必要です");
    //----------------------------------------------------------
    // リジッドボディを生成
    //----------------------------------------------------------
    {
        // モデルメッシュから剛体を生成
        _rigidbody = physics::createRigidBody(_mass,
                                              _pTransform->GetLocalToWorldMatrix(),
                                              _pCollider->GetBtCollisionShape());

        if(_isKinematic) {
            SetKinematic();
        }
    }
    return true;
}
//---------------------------------------------------------------------------
//! Imgui描画
//---------------------------------------------------------------------------
void RigidBody::OnRenderImgui()
{
    ImGui::Text("Mass: %d", _mass);
}
//---------------------------------------------------------------------------
//!更新
//---------------------------------------------------------------------------
void RigidBody::OnUpdate()
{
    if(_isKinematic) {
        _rigidbody->setWorldMatrix(_pTransform->GetLocalToWorldMatrix());
    }
}

//---------------------------------------------------------------------------
//! 解放
//---------------------------------------------------------------------------
void RigidBody::OnFinalize()
{
    Remove();
}
//---------------------------------------------------------------------------
//! 剛体を取得
//---------------------------------------------------------------------------
raw_ptr<physics::RigidBody> RigidBody::GetRigidBody()
{
    return _rigidbody;
}
//---------------------------------------------------------------------------
//!< キネマティックを設定
//---------------------------------------------------------------------------
void RigidBody::SetKinematic()
{
    btRigidBody* nativeRigidbody = _rigidbody->nativeRigidBody();
    nativeRigidbody->setCollisionFlags(nativeRigidbody->getCollisionFlags() | btCollisionObject::CF_KINEMATIC_OBJECT);
    nativeRigidbody->setActivationState(DISABLE_DEACTIVATION);
    _isKinematic = true;
}
//---------------------------------------------------------------------------
//! 剛体を削除
//---------------------------------------------------------------------------
void RigidBody::Remove()
{
    if(_rigidbody) {
        physics::PhysicsEngine::instance()->unregisterRigidBody(_rigidbody->nativeRigidBody());
        _rigidbody.reset();
    }
}
}   // namespace component
