﻿//---------------------------------------------------------------------------
//! @file	ModelRenderer.h
//!	@brief	モデルレンダー用のコンポネント
//---------------------------------------------------------------------------
#pragma once
#include "Component.h"
#include "renderer/Model.h"
namespace component {

class Transform;
class Animator;

class ModelRenderer : public Component
{
public:
    ModelRenderer();    //!< コンストラクタ
    ~ModelRenderer();   //!< デストラクタ

private:
    //---------------------------------------------------------------------------
    //! 継承関数
    //---------------------------------------------------------------------------
    bool OnInit() override;          //!< 初期化
    void OnUpdate() override;        //!< 更新
    void OnRender() override;        //!< 描画
    void OnRenderImgui() override;   //!< ImGui描画
public:
    //---------------------------------------------------------------------------
    //! public 関数
    //---------------------------------------------------------------------------
    void CreateModel(std::string_view path, f32 scale = 1.0f);   //!< モデルを読み込む
    void SetOffset(const float3& offset);                        //!< オフセットを設定

    //! 外部（別の）DiffuseTexture設定する、fbx Fileのテクスチャ載ってない(かも)
    void LoadExternalDiffuseTexture(std::string_view path);

    //---------
    // ゲッター
    shr_ptr<Model> GetModel() const;   //!< Modelを取得
private:
    //---------------------------------------------------------------------------
    //! private関数
    //---------------------------------------------------------------------------

    //---------------------------------------------------------------------------
    //! private 変数
    //---------------------------------------------------------------------------
    raw_ptr<component::Transform> _pTransform;   //!< Transform コンポーネント参照
    raw_ptr<component::Animator>  _pAnimator;    //!< Animator  コンポーネント参照

    shr_ptr<Model> _model;   //!< モデル

    bool   _isAnimated = false;                  //!< このモデルアニメションあるかどうか
    float3 _offset     = { 0.0f, 0.0f, 0.0f };   //!< 描画原点と物理コライダー原点を合わせてのオフセット

    bool _renderDebug = false;   //!< モデルデバッグ描画するかどうか
};
}   // namespace component
