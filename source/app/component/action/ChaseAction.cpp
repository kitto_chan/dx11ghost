﻿//---------------------------------------------------------------------------
//!	@file	ChaseAction.h
//!	@brief	常にターゲットを追跡する
//---------------------------------------------------------------------------

#include "ChaseAction.h"
#include "entity/Entity.h"
#include "component/Transform.h"

namespace component {
namespace {

}   // namespace

//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
ChaseAction::ChaseAction()
{
    _name = _NAME;
}

//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool ChaseAction::OnInit()
{
	// プレイヤーを追跡する
    _pTargetTransform = manager::FindEntityByName("Player")->GetComponent<component::Transform>();
    _pTransform       = _pOwner->GetComponent<component::Transform>();
    return true;
}

//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void ChaseAction::OnUpdate()
{
    float3 targetPos = _pTargetTransform->GetPosition();
    float2 dir       = targetPos.xz - _pTransform->GetPosition().xz;
    dir = normalize(dir);

	if(timer::TimerIns()->DeltaTime() > 1) return; // 最初読み込むの時　delta time はおかしくなる TODO: Fix

    _pTransform->AddPositionX(dir.f32[0] * timer::TimerIns()->DeltaTime() * _speed);
    _pTransform->AddPositionZ(dir.f32[1] * timer::TimerIns()->DeltaTime() * _speed);
    _pTransform->LookAt({ targetPos.x, _pTransform->GetPosition().y, targetPos.z });
}

//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void ChaseAction::OnRender()
{
}

//---------------------------------------------------------------------------
//! Imgui描画
//---------------------------------------------------------------------------
void ChaseAction::OnRenderImgui()
{
}

//---------------------------------------------------------------------------
//! 解放
//---------------------------------------------------------------------------
void ChaseAction::OnFinalize()
{
}
}   // namespace component
