﻿//---------------------------------------------------------------------------
//!	@file	FollowAction.h
//!	@brief	経路探測用アクション
//---------------------------------------------------------------------------

#include "FollowAction.h"
#include "entity/Entity.h"
#include "component/Transform.h"

namespace component {
namespace {
constexpr s32 MAP_H = settings::MAP_H;   //!< マップの長さ
constexpr s32 MAP_W = settings::MAP_W;   //!< マップの幅さ

constexpr s32 WALL        = 1;     //!< 壁（通れないブロック番号）
constexpr s32 ROUTE_BLOCK = 999;   //!< ルート用（通れないブロック番号）

constexpr s32 MOV_FRAME  = 18;                                   //!< 次のマスまで移動するのにかかるフレーム数
constexpr s32 BLOCK_SIZE = 1;                                    //!< 1マスのサイズ
constexpr f32 MOVE_SPEED = ((f32)BLOCK_SIZE / (f32)MOV_FRAME);   //!< このフレーム数でちょうど１マス進むためのスピード

s32 MapData[MAP_H][MAP_W];   //!< 地図データ

//!< 渡したの座標はマップの境界かどうか
bool CheckIsBoundary(s32 posX, s32 posZ)
{
    return posX >= MAP_W || posZ >= MAP_H || posX < 0 || posZ < 0;
}
}   // namespace

//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
FollowAction::FollowAction()
{
    _name = _NAME;
}

//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool FollowAction::OnInit()
{
    for(int w = 0; w < MAP_W; w++) {
        for(int h = 0; h < MAP_H; h++) {
            _route[h][w] = ROUTE_BLOCK;
        }
    }

    _pTargetTransform = manager::FindEntityByName("Player")->GetComponent<component::Transform>();
    _pTransform       = _pOwner->GetComponent<component::Transform>();

    if(_actionMode == ActionMode::Random) {
        RandPos();
    }

    return true;
}

//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void FollowAction::OnUpdate()
{
    s32 moveToPosX = 0;
    s32 moveToPosZ = 0;
	
    float3 currentPos  = _pTransform->GetPosition();
    s32    currentPosX = static_cast<s32>(std::roundf(currentPos.f32[0]));
    s32    currentPosZ = static_cast<s32>(std::roundf(currentPos.f32[2]));

    switch(_actionMode) {
        case ActionMode::Tracking:
        {
            float3 targetPos = _pTargetTransform->GetPosition();
            moveToPosX       = static_cast<s32>(std::roundf(targetPos.f32[0]));
            moveToPosZ       = static_cast<s32>(std::roundf(targetPos.f32[2]));
        } break;
        case ActionMode::Random:
        {
            moveToPosX = _randPos.x;
            moveToPosZ = _randPos.y;
            if(currentPosX == moveToPosX && currentPosZ == moveToPosZ) {
                RandPos();
                moveToPosX = _randPos.x;
                moveToPosZ = _randPos.y;
            }
        } break;
    }

    if(!CheckIsBoundary(moveToPosX, moveToPosZ)) {
        for(s32 w = 0; w < MAP_W; w++) {
            for(s32 h = 0; h < MAP_H; h++) {
                _route[h][w] = ROUTE_BLOCK;
            }
        }
        SetRoute(moveToPosX, moveToPosZ, 0);
    }

    if(_moveCount == 0) {
        // 今いるポイントのルート番号
        s32 routeNow = _route[currentPosZ][currentPosX];

        // 比較用に上下左右のポイントのルート番号を入れる
        s32 routeRight = currentPosX + 1 >= MAP_W ? ROUTE_BLOCK : _route[currentPosZ][currentPosX + 1];
        s32 routeLeft  = currentPosX - 1 < 0 ? ROUTE_BLOCK : _route[currentPosZ][currentPosX - 1];
        s32 routeDown  = currentPosZ + 1 >= MAP_H ? ROUTE_BLOCK : _route[currentPosZ + 1][currentPosX];
        s32 routeUp    = currentPosZ - 1 < 0 ? ROUTE_BLOCK : _route[currentPosZ - 1][currentPosX];

        if(routeRight < routeNow) {
            _moveDir   = DIR::RIGHT;   //	向かいたい方向のセット
            _moveCount = MOV_FRAME;    //	フレーム数をセット
        }
        else if(routeLeft < routeNow) {
            _moveDir   = DIR::LEFT;
            _moveCount = MOV_FRAME;
        }
        else if(routeDown < routeNow) {
            _moveDir   = DIR::UP;
            _moveCount = MOV_FRAME;
        }
        else if(routeUp < routeNow) {
            _moveDir   = DIR::DOWN;
            _moveCount = MOV_FRAME;
        }
        else {
            _moveDir = DIR::NONE;
        }
    }

    float3 pos = _pTransform->GetPosition();

    //	移動フレームの数値がある間は移動させる
    if(_moveCount > 0) {
        _moveCount--;   //	時間を減らす

        //	移動方向に合わせて座標の移動
        switch(_moveDir) {
            case DIR::RIGHT:
                pos.x += MOVE_SPEED;
                _pTransform->SetRotationY(math::D2R(270));
                break;
            case DIR::LEFT:
                pos.x -= MOVE_SPEED;
                _pTransform->SetRotationY(math::D2R(90));
                break;
            case DIR::UP:
                pos.z += MOVE_SPEED;
                _pTransform->SetRotationY(math::D2R(180));
                break;
            case DIR::DOWN:
                pos.z -= MOVE_SPEED;
                _pTransform->SetRotationY(math::D2R(0));
                break;
        }

        _pTransform->SetPositionZ(pos.z);
        _pTransform->SetPositionX(pos.x);

        //	最後まで移動しきったら
        if(_moveCount == 0) {
            //	次のポイントにしたので座標もそのポイントに合わせておく
            pos.x = static_cast<f32>(currentPosX) * BLOCK_SIZE;
            pos.z = static_cast<f32>(currentPosZ) * BLOCK_SIZE;
            _pTransform->SetPositionZ(pos.z);
            _pTransform->SetPositionX(pos.x);
        }
    }
}

//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void FollowAction::OnRender()
{
}

//---------------------------------------------------------------------------
//! Imgui描画
//---------------------------------------------------------------------------
void FollowAction::OnRenderImgui()
{
}

//---------------------------------------------------------------------------
//! 解放
//---------------------------------------------------------------------------
void FollowAction::OnFinalize()
{
}
//---------------------------------------------------------------------------
//! 地図データ設置
//---------------------------------------------------------------------------
void FollowAction::SetMapData(s32 map[MAP_H][MAP_W])
{
    std::copy(&map[0][0], &map[0][0] + MAP_H * MAP_W, &MapData[0][0]);
}
//---------------------------------------------------------------------------
//! パスファインディングのルート計算
//---------------------------------------------------------------------------
void FollowAction::SetRoute(int w, int h, int num)
{
    // 通れないところまで行ったら終了
    if(MapData[h][w] == WALL) return;

    // もし境界行ったら終了
    if(CheckIsBoundary(w, h)) return;

    // 渡されてきたポイント（w,h）の場所の値を設定（num）
    _route[h][w] = num;
    // １つ右のルートの値を１大きくしたものに設定したい
    // その１つ右のルートが今から割り出したい値より大きかったら
    if(_route[h][w + 1] > num + 1) {
        // １つ右の所に１大きい値を放り込む
        SetRoute(w + 1, h, num + 1);
    }
    // 左の処理
    if(_route[h][w - 1] > num + 1) {
        // １つ左の所に１大きい値を放り込む
        SetRoute(w - 1, h, num + 1);
    }
    // 下の処理
    if(_route[h + 1][w] > num + 1) {
        // １つ下の所に１大きい値を放り込む
        SetRoute(w, h + 1, num + 1);
    }
    // 上の処理
    if(_route[h - 1][w] > num + 1) {
        // １つ上の所に１大きい値を放り込む
        SetRoute(w, h - 1, num + 1);
    }
}
void FollowAction::RandPos()
{
    while(true) {
        _randPos.x = math::GetRandomI(0, MAP_W);
        _randPos.y = math::GetRandomI(0, MAP_H);

        if(CheckIsBoundary(_randPos.x, _randPos.y)) continue;
        if(MapData[_randPos.y][_randPos.x] != WALL) {
            break;
        }
    }
}
}   // namespace component
