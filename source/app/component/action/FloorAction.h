﻿//---------------------------------------------------------------------------
//!	@file	FloorAction.h
//!	@brief	床のアクションのクラス
//---------------------------------------------------------------------------
#pragma once
#include "component/Component.h"

namespace component {
class Transform;
class RigidBody;
class FloorAction : public Component
{
    const std::string _NAME = "FloorAction";

public:
    FloorAction() = delete;      //!< コンストラクタ
    FloorAction(f32 fallTime);   //!< コンストラクタ
    ~FloorAction() = default;    //!< デストラクタ

    //---------------------------------------------------------------------------
    // public 継承関数
    //---------------------------------------------------------------------------
    virtual bool Init();          //!< 初期化
    virtual void Update();        //!< 更新
    virtual void Render();        //!< 描画
    virtual void RenderImgui();   //!< ImGui描画
    virtual void Finalize();      //!< 解放
public:
    //---------------------------------------------------------------------------
    // public 関数
    //---------------------------------------------------------------------------

    void SetFallTime(f32 fallTime);
    //---------------------------------------------------------------------------
    // public 変数
    //---------------------------------------------------------------------------

private:
    //---------------------------------------------------------------------------
    // private 関数
    //---------------------------------------------------------------------------
    f32 _fallSpeed = 10.0f;

    f32 _counter = 0.0f;
    //---------------------------------------------------------------------------
    // private 変数
    //---------------------------------------------------------------------------
    raw_ptr<Transform> _pTransform = nullptr;
    raw_ptr<RigidBody> _pRigidBody = nullptr;
};
}   // namespace component
