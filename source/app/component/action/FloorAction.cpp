﻿//---------------------------------------------------------------------------
//!	@file	FloorAction.h
//!	@brief	床のアクションのクラス
//---------------------------------------------------------------------------
#include "FloorAction.h"
#include "entity/Entity.h"
#include "component/Transform.h"
#include "component/RigidBody.h"
namespace component {
namespace {
constexpr f32 DISABLE_DISTANCE = -10.0f;   //!< Y軸はこの距離以下なったらオブジェクト削除する
}
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
FloorAction::FloorAction(f32 fallTime)
: _counter(fallTime)
{
    _name = _NAME;

    constexpr f32 minSpeed = 0.5f;   //!< 最低速度
    constexpr f32 maxSpeed = 5.0f;   //!< 最高速度

    _fallSpeed = math::GetRandomF(minSpeed, maxSpeed);
}

//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool FloorAction::Init()
{
    // 必要なコンポネントを揃えます
    _pTransform = _pOwner->GetComponent<component::Transform>();
    _pRigidBody = _pOwner->GetComponent<component::RigidBody>();

    ASSERT_MESSAGE(_pTransform, "Transform Componentが見つけません");
    ASSERT_MESSAGE(_pRigidBody, "RigidBody Componentが見つけません");

    return true;
}

//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void FloorAction::Update()
{
    _counter--;
    if(_counter <= 0.0f) {
        f32 dt = timer::TimerIns()->DeltaTime();
        _pTransform->AddPositionY(-_fallSpeed * dt);

        // rigibody解放
        if(_pRigidBody->IsEnable()) {
            _pRigidBody->Remove();
            _pRigidBody->Disable();
        }

        // 削除します
        if(_pTransform->GetPosition().y <= DISABLE_DISTANCE) {
            _pOwner->Release();
        }
    }
}

//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void FloorAction::Render()
{
}

//---------------------------------------------------------------------------
//! Imgui描画
//---------------------------------------------------------------------------
void FloorAction::RenderImgui()
{
}

//---------------------------------------------------------------------------
//! 解放
//---------------------------------------------------------------------------
void FloorAction::Finalize()
{
}
void FloorAction::SetFallTime(f32 fallTime)
{
    _counter = fallTime;
}
}   // namespace component
