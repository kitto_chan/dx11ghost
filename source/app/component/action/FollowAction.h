﻿//---------------------------------------------------------------------------
//!	@file	FollowAction.h
//!	@brief	経路探測用アクション
//---------------------------------------------------------------------------
#pragma once
#include "component/Component.h"

namespace component {
class Transform;
class FollowAction : public Component
{
    const std::string _NAME = "FollowAction";

public:
    FollowAction();              //!< コンストラクタ
    ~FollowAction() = default;   //!< デストラクタ

    enum class ActionMode
    {
        Tracking,
        Random
    };
    //---------------------------------------------------------------------------
    // 継承関数
    //---------------------------------------------------------------------------
private:
    bool OnInit();          //!< 初期化
    void OnUpdate();        //!< 更新
    void OnRender();        //!< 描画
    void OnRenderImgui();   //!< ImGui描画
    void OnFinalize();      //!< 解放
public:
    //---------------------------------------------------------------------------
    // public 関数
    //---------------------------------------------------------------------------
    void SetMapData(s32 map[settings::MAP_H][settings::MAP_W]);

    void SetToTrackingMode() { _actionMode = ActionMode::Tracking; }
    void SetToRandomMode() { _actionMode = ActionMode::Random; }
    //---------------------------------------------------------------------------
    // public 変数
    //---------------------------------------------------------------------------

private:
    enum class DIR
    {
        NONE,
        RIGHT,   //	右に移動
        LEFT,    //	左
        DOWN,    //	下
        UP,      //	上
    };
    //---------------------------------------------------------------------------
    // private 関数
    //---------------------------------------------------------------------------
    void SetRoute(int w, int h, int num);   //!< ルートを設定

    void RandPos();   //!< ランダムモードの目的地設定
    //---------------------------------------------------------------------------
    // private 変数
    //---------------------------------------------------------------------------
    raw_ptr<Transform> _pTransform = nullptr;   //!< Transform コンポネント

    raw_ptr<Transform> _pTargetTransform = nullptr;   //!< 追跡ターゲットのTransform コンポネント

    s32 _route[settings::MAP_H][settings::MAP_W];   //!< 動くパス

    DIR _moveDir   = DIR::NONE;   //!< オーナーが進んでいく方向
    s32 _moveCount = 0;           //!< 次に動くまでのカウント

    int2 _randPos = { 0, 0 };   //!< ランダムで移動のため

    ActionMode _actionMode = ActionMode::Random;   //!< モード（行為）
};
}   // namespace component
