﻿//---------------------------------------------------------------------------
//!	@file	ChaseAction.h
//!	@brief	常にターゲットを追跡する
//---------------------------------------------------------------------------
#pragma once
#include "component/Component.h"

namespace component {
class Transform;
class ChaseAction : public Component
{
    const std::string _NAME = "ChaseAction";

public:
    ChaseAction();              //!< コンストラクタ
    ~ChaseAction() = default;   //!< デストラクタ

    //---------------------------------------------------------------------------
    // 継承関数
    //---------------------------------------------------------------------------
private:
    bool OnInit();          //!< 初期化
    void OnUpdate();        //!< 更新
    void OnRender();        //!< 描画
    void OnRenderImgui();   //!< ImGui描画
    void OnFinalize();      //!< 解放
public:
    //---------------------------------------------------------------------------
    // public 関数
    //---------------------------------------------------------------------------
    //---------------------------------------------------------------------------
    // public 変数
    //---------------------------------------------------------------------------

private:
    //---------------------------------------------------------------------------
    // private 関数
    //---------------------------------------------------------------------------

    //---------------------------------------------------------------------------
    // private 変数
    //---------------------------------------------------------------------------
    raw_ptr<Transform> _pTransform = nullptr;   //!< Transform コンポネント

    raw_ptr<Transform> _pTargetTransform = nullptr; //!< 追跡ターゲットのTransform コンポネント

	f32 _speed = 1.0f; //!< 追いかけるスピード
};
}   // namespace component
