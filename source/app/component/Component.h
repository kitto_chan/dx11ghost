﻿//---------------------------------------------------------------------------
//!	@file	Conponent.h
//!	@brief	コンポーネント
//---------------------------------------------------------------------------
#pragma once
namespace entity {
class Entity;
}

namespace component {

class Component
{
    friend class entity::Entity;

public:
    Component();
    // コピー禁止
    //Component(const Component&) = delete;
    // 代入禁止
    Component& operator=(const Component&) = delete;
    virtual ~Component()                   = default;

private:
    //---------------------------------------------------------------------------
    //! メイン関数
    //---------------------------------------------------------------------------
    bool Init();          //!< 初期化
    void Update();        //!< 更新
    void Render();        //!< 描画
    void RenderImgui();   //!< ImGui描画
    void Finalize();      //!< 解放

public:
    std::string GetName() const;   //!< コンポネントの名前を取得

    //! ゲッター / セッター
    entity::Entity* GetOwner() const;                   //!< オーナーを取得
    void            SetOwner(entity::Entity* _owner);   //!< オーナーを設定

    //@{ enable

    bool IsEnable() const;        //!<　有効かどうか
    void SetEnable(bool value);   //!<　Enableフラッグ設定
    void Enable();                //!<　有効する
    void Disable();               //!<　無効する

    //@}

protected:
    //---------------------------------------------------------------------------
    //! protected 変数
    //---------------------------------------------------------------------------
    // 継承用
    virtual bool OnInit() { return true; };   //!< 初期化
    virtual void OnUpdate(){};                //!< 更新
    virtual void OnRender(){};                //!< 描画
    virtual void OnRenderImgui(){};           //!< ImGui描画
    virtual void OnFinalize(){};              //!< 解放

    bool _isEnable  = true;    //!< 有効したら(Update/Render)の関数を実行
    bool _isInited  = false;   //!< 初期化した
    bool _isRelease = false;   //!< エンティティマネジャーから外す
    bool _isRemoved = false;   //!< 完全に削除

    std::string _name;   //!< コンポネントの名前（表示用）

    entity::Entity* _pOwner = nullptr;   //!< オーナー
};
}   // namespace component
