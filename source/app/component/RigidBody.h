﻿//---------------------------------------------------------------------------
//!	@file	RigidBody.h
//!	@brief	ゲームオブジェクト を物理特性によって制御する事ができるようになります。\n
//!			BulletPhysics物理エンジンを通して他のオブジェクトと相互作用させるためには、\n
//!			ゲームオブジェクトにリジッドボディを加える必要があります。
//---------------------------------------------------------------------------
#pragma once
#include "Component.h"

namespace component {
class Collider;
class Transform;

class RigidBody : public Component
{
    const std::string _NAME = "RigidBody";

public:
    RigidBody();                                     //!< コンストラクタ
    RigidBody(f32 mass, bool isKinematic = false);   //!< コンストラクタ
    ~RigidBody();                                    //!< デストラクタ
private:
    //---------------------------------------------------------------------------
    // 継承関数
    //---------------------------------------------------------------------------
    bool OnInit();          //!< 初期化
    void OnRenderImgui();   //!< ImGui描画
    void OnUpdate();        //!< 更新
    void OnFinalize();      //!< 解放

public:
    //---------------------------------------------------------------------------
    // public 関数
    //---------------------------------------------------------------------------
    raw_ptr<physics::RigidBody> GetRigidBody();   //!< 剛体を取得

    void SetKinematic();   //!< キネマティックを設定
    void Remove();         //!< 剛体を削除
    //---------------------------------------------------------------------------
    // public 変数
    //---------------------------------------------------------------------------

private:
    //---------------------------------------------------------------------------
    // private 関数
    //---------------------------------------------------------------------------

private:
    //---------------------------------------------------------------------------
    // private 変数
    //---------------------------------------------------------------------------
    uni_ptr<physics::RigidBody> _rigidbody;   //!< 剛体

    raw_ptr<Transform> _pTransform;   //!< TransformComponent
    raw_ptr<Collider>  _pCollider;    //!< 剛体
    //===========================================================================
    //! @defgroup bullet phy 物理計算用変数
    //! bullet phyのデフォルトと同じ
    //===========================================================================
    //@{
    bool _isKinematic = false;   //!< キネマティック
    f32  _mass        = 0.0f;    //!< 物体の質量(kg)【0 = 静的オブジェクト】

    // TODO
    //f32 _friction         = 0.5f;   //!< 摩擦係数
    //f32 _restitution      = 0.0f;   //!< 跳ね返り係数
    //f32 _rollingFriction  = 0.0f;   //!< 転がり摩擦
    //f32 _spinningFriction = 0.0f;   //!< スピン摩擦

    //@}
};

}   // namespace component
