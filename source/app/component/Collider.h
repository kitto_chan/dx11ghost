﻿//---------------------------------------------------------------------------
//!	@file	Collider.h
//!	@brief	すべてのコライダーの定義
//---------------------------------------------------------------------------
#pragma once
#include "Component.h"

namespace component {
//===========================================================================
//! すべてのコライダーの基盤となるクラスです。
//===========================================================================
class Collider : public Component
{
public:
    Collider()          = default;   //!< コンストラクタ
    virtual ~Collider() = default;   //!< デストラクタ
    virtual uni_ptr<btCollisionShape> GetBtCollisionShape() { return std::move(_btShape); };

protected:
    std::unique_ptr<btCollisionShape> _btShape = nullptr;   //!< bulletシェープ
};

//===========================================================================
//! ボックスコライダーの基盤となるクラスです。
//===========================================================================
class BoxCollider : public Collider
{
public:
    BoxCollider() = delete;            //!< コンストラクタ
    BoxCollider(const float3& size);   //!< コンストラクタ
    ~BoxCollider() = default;          //!< デストラクタ
private:
    bool OnInit() override;          //!< 初期化
    void OnRenderImgui() override;   //!< ImGui描画
private:
    float3 _halfExtents = 0.0f;   //!< ボックスの辺長
};
//===========================================================================
//!　球コライダーの基盤となるクラスです。
//===========================================================================
class SphereCollider : public Collider
{
public:
    SphereCollider() = delete;     //!< コンストラクタ
    SphereCollider(f32 radius);    //!< コンストラクタ
    ~SphereCollider() = default;   //!< デストラクタ

private:
    bool OnInit() override;          //!< 初期化
    void OnRenderImgui() override;   //!< ImGui描画
private:
    f32 _radius = 0.0f;   //!< 半径
};
//===========================================================================
//! 円筒コライダーの基盤となるクラスです。
//===========================================================================
class CylinderCollider : public Collider
{
public:
    CylinderCollider() = delete;                          //!< コンストラクタ
    CylinderCollider(f32 radius, f32 heightHalfExtent);   //!< コンストラクタ
    ~CylinderCollider() = default;                        //!< デストラクタ

private:
    bool OnInit() override;          //!< 初期化
    void OnRenderImgui() override;   //!< ImGui描画
private:
    f32 _radius           = 0.0f;   //!< 半径
    f32 _heightHalfExtent = 0.0f;   //!< 高さの半分
};
//===========================================================================
//! メッシュコライダー(モデルと同じ形のコライダー)の基盤となるクラスです。
//===========================================================================
class ModelRenderer;
class MeshCollider : public Collider
{
public:
    MeshCollider();              //!< コンストラクタ
    ~MeshCollider() = default;   //!< デストラクタ

private:
    bool OnInit() override;          //!< 初期化
    void OnRenderImgui() override;   //!< ImGui描画
private:
    raw_ptr<ModelRenderer> _pModelRenderer;   //!< ModelRendererを参照
};

}   // namespace component
