﻿//---------------------------------------------------------------------------
//!	@file	Collider.h
//!	@brief	すべてのコライダーの基盤となるクラスです。
//---------------------------------------------------------------------------
#include "Collider.h"
#include "entity/Entity.h"
#include "component/ModelRenderer.h"

namespace component {
namespace {
//----
// 三角形メッシュのために使用中はメモリを保持しておく必要がある
std::unique_ptr<btTriangleIndexVertexArray> triangles_;   //!< 三角形リスト
}
//===========================================================================
// Box Collider
//===========================================================================

//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
BoxCollider::BoxCollider(const float3& size)
{
    _name        = "BoxCollider";
    _halfExtents = size / 2.0f;
}

//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool BoxCollider::OnInit()
{
    _btShape = std::make_unique<btBoxShape>(btVector3(_halfExtents.x, _halfExtents.y, _halfExtents.z));
    return true;
}
//---------------------------------------------------------------------------
//! ImGui描画
//---------------------------------------------------------------------------
void BoxCollider::OnRenderImgui()
{
    imgui::ImguiDragXYZ(u8"サイズ", _halfExtents);
    /* btVector3 bv = math::castBtVec3(_halfExtents);
        _btShape->setLocalScaling(bv);*/
}

//===========================================================================
// Sphere Collider
//===========================================================================

//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
SphereCollider::SphereCollider(f32 radius)
{
    _name   = "SphereCollider";
    _radius = radius;
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool SphereCollider::OnInit()
{
    _btShape = std::make_unique<btSphereShape>(_radius);
    return true;
}
//---------------------------------------------------------------------------
//! ImGui描画
//---------------------------------------------------------------------------
void SphereCollider::OnRenderImgui()
{
    ImGui::DragFloat(u8"半径", &_radius);
}

//===========================================================================
// Cylinder Collider
//===========================================================================
//---------------------------------------------------------------------------
//!< コンストラクタ
//---------------------------------------------------------------------------
CylinderCollider::CylinderCollider(f32 radius, f32 heightHalfExtent)
{
    _name             = "CylinderCollider";
    _radius           = radius;
    _heightHalfExtent = heightHalfExtent;
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool CylinderCollider::OnInit()
{
    _btShape = std::make_unique<btCylinderShape>(btVector3(_radius, _heightHalfExtent, _radius));
    return true;
}
//---------------------------------------------------------------------------
//! ImGui描画
//---------------------------------------------------------------------------
void CylinderCollider::OnRenderImgui()
{
    ImGui::DragFloat(u8"半径", &_radius);
    ImGui::DragFloat(u8"高さ", &_heightHalfExtent);
}

//===========================================================================
// Mesh Collider
//===========================================================================
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
MeshCollider::MeshCollider()
{
    _name = "MeshCollider";
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool MeshCollider::OnInit()
{
    _pModelRenderer = _pOwner->GetComponent<ModelRenderer>();
    if(!_pModelRenderer) {
        ASSERT_MESSAGE(false, "MeshColliderはModelRendererが必要だ!");
        return false;
    }
    raw_ptr model = _pModelRenderer->GetModel();



    std::vector<u32> triangleIndices_;   //!< TriangleMesh用インデックス配列
    triangles_ = std::make_unique<btTriangleIndexVertexArray>();
    //-----------------------------------------------------------------------
    // インデックス配列を生成
    //-----------------------------------------------------------------------
    {
        const u32 triangleCount = model->vertexCount() / 3;

        triangleIndices_.reserve(triangleCount * 3);
        for(u32 i = 0; i < triangleCount; ++i) {
            triangleIndices_.push_back(i * 3 + 0);
            triangleIndices_.push_back(i * 3 + 1);
            triangleIndices_.push_back(i * 3 + 2);
        }

        // メッシュ情報を登録
        btIndexedMesh part;

        part.m_vertexBase          = reinterpret_cast<const unsigned char*>(model->vertices());   // 頂点配列の先頭
        part.m_vertexStride        = sizeof(float3);                                              // 1頂点あたりの間隔
        part.m_numVertices         = model->vertexCount();                                        // 頂点数
        part.m_triangleIndexBase   = reinterpret_cast<const unsigned char*>(model->indices());    // 三角形インデックス配列の先頭
        part.m_triangleIndexStride = sizeof(u32) * 3;                                             // 三角形インデックス1個あたりの間隔
        part.m_numTriangles        = model->indexCount() / 3;                                     // 三角形数
        part.m_indexType           = PHY_INTEGER;                                                 // インデックスはu32

        triangles_->addIndexedMesh(part, PHY_INTEGER);
    }

    //-----------------------------------------------------------------------
    // シェイプの生成
    //-----------------------------------------------------------------------
    constexpr bool useQuantizedAabbCompression = true;

    _btShape = std::make_unique<btBvhTriangleMeshShape>(triangles_.get(), useQuantizedAabbCompression);
    return true;
}
//---------------------------------------------------------------------------
//! ImGui描画
//---------------------------------------------------------------------------
void MeshCollider::OnRenderImgui()
{
}

}   // namespace component
