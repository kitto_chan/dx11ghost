﻿//---------------------------------------------------------------------------
//! @file	Animator.h
//!	@brief	アニメーターはモデルのアニメション処理コンポネント
//---------------------------------------------------------------------------
#include "Animator.h"
#include "component/Transform.h"
#include "component/Animator.h"
#include "Entity/Entity.h"

namespace component {
namespace {
//!< TODO Assert Manager　現在playerしか使ってるので、先にこうします
std::shared_ptr<Animation> _pAnimation;   //!< メインアニメーションのロジック
}   // namespace
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
Animator::Animator()
{
    _name = "Animator";
}
//---------------------------------------------------------------------------
//! デスストラクタ
//---------------------------------------------------------------------------
Animator::~Animator()
{
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool Animator::OnInit()
{
    //==========================================================
    // 必要なコンポーネントを取得
    //==========================================================
    _pTransform = _pOwner->GetComponent<Transform>();
    ASSERT_MESSAGE(_pTransform, "AnimatorはTransformは必要です");

    //-------------------------------------------------------------
    // アニメションのチェック
    //-------------------------------------------------------------
    ASSERT_MESSAGE(_pAnimation, "先にアニメションを作ってください");

    return true;
}
//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void Animator::OnUpdate()
{
    _pAnimation->update(timer::TimerIns()->DeltaTime());
}
//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void Animator::OnRender()
{
}
//---------------------------------------------------------------------------
//! Imgui描画
//---------------------------------------------------------------------------
void Animator::OnRenderImgui()
{
}
//---------------------------------------------------------------------------
//！ アニメションを生成
//---------------------------------------------------------------------------
void Animator::CreateAnimation(const Animation::Desc* desc, u64 count, s32 defaultId)
{
    ASSERT_MESSAGE(defaultId < count, "Default Id > Size");
    if(_pAnimation)
        return;
    _pAnimation = createAnimation(desc, count);
    // デフォルト アニメーションを再生
    _pAnimation->play(desc[defaultId].name_, Animation::PlayType::Loop);
    _currentAnimationName = desc[defaultId].name_;
}
void Animator::Play(std::string name, Animation::PlayType playType)
{
    if(!std::strcmp(_currentAnimationName.c_str(), name.c_str())) return;
    _pAnimation->play(name.data(), playType);
    _currentAnimationName = name;
}
//---------------------------------------------------------------------------
//！ アニメションを取得
//---------------------------------------------------------------------------
shr_ptr<Animation> Animator::GetAnimation() const
{
    return _pAnimation;
}

std::string Animator::GetAnimationName() const
{
    return _currentAnimationName;
}

bool Animator::IsLastFrame() const
{
    return _pAnimation->IsLastFrame();
}

}   // namespace component
