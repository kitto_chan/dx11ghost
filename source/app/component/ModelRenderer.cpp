﻿//---------------------------------------------------------------------------
//! @file	ModelRenderer.cpp
//!	@brief	モデルレンダー用のコンポネント
//---------------------------------------------------------------------------
#include "ModelRenderer.h"
#include "component/Transform.h"
#include "component/Animator.h"
#include "Entity/Entity.h"
namespace component {
namespace {
//! モデル管理(仮)
//! TODO:AssertManager Class
std::map<std::string, shr_ptr<Model>> ModelMapping;
}   // namespace
//---------------------------------------------------------------------------
//! コンストラクタ
//---------------------------------------------------------------------------
ModelRenderer::ModelRenderer()
{
    _name = "ModelRenderer";
}
//---------------------------------------------------------------------------
//! デスストラクタ
//---------------------------------------------------------------------------
ModelRenderer::~ModelRenderer()
{
}
//---------------------------------------------------------------------------
//! 初期化
//---------------------------------------------------------------------------
bool ModelRenderer::OnInit()
{
    //==========================================================
    // 必要なコンポーネントを取得
    //==========================================================
    _pTransform = _pOwner->GetComponent<Transform>();
    ASSERT_MESSAGE(_pTransform, "Transformは必要です");

    _pAnimator = _pOwner->GetComponent<Animator>();
    if(_pAnimator) {
        _isAnimated = true;
        _model->bindAnimation(_pAnimator->GetAnimation());
    }
    //-------------------------------------------------------------
    // モデルのチェック
    //-------------------------------------------------------------
    ASSERT_MESSAGE(_model, "モデル読み込んでないです");

    return true;
}
//---------------------------------------------------------------------------
//! 更新
//---------------------------------------------------------------------------
void ModelRenderer::OnUpdate()
{
    _model->update();
}
//---------------------------------------------------------------------------
//! 描画
//---------------------------------------------------------------------------
void ModelRenderer::OnRender()
{
    // モデルの原点は足元から、でも物理用の原点は中心点からので、それを合わせます
    {
        component::Transform trans;

        trans.Copy(*_pTransform);
        float3 pos = _pTransform->GetLocalToWorldMatrix()._41_42_43;
        pos += _offset;
        trans.SetPosition(pos);
        _model->setWorldMatrix(trans.GetLocalToWorldMatrix());
    }
    _model->render();

    if(_renderDebug) {
        _model->renderDebug();
    }
}
//---------------------------------------------------------------------------
//! Imgui描画
//---------------------------------------------------------------------------
void ModelRenderer::OnRenderImgui()
{
    imgui::ImguiDragXYZ("Offset", _offset);
    imgui::ImguiCheckBox("RenderDebug", _renderDebug);
}
//---------------------------------------------------------------------------
//!< モデルを読み込む
//---------------------------------------------------------------------------
void ModelRenderer::CreateModel(std::string_view path, f32 scale)
{
    auto itr = ModelMapping.find(path.data());
    if(itr != ModelMapping.end()) {
        _model = itr->second;
        return;
    }

    _model = createModel(path.data(), scale);
    ModelMapping.insert(std::pair(path, _model));
}
//---------------------------------------------------------------------------
//!< オフセットを設定
//---------------------------------------------------------------------------
void ModelRenderer::SetOffset(const float3& offset)
{
    _offset = offset;
}
//---------------------------------------------------------------------------
//! 外部（別の）DiffuseTexture設定する、fbx Fileのテクスチャ載ってない(かも)
//---------------------------------------------------------------------------
void ModelRenderer::LoadExternalDiffuseTexture(std::string_view path)
{
    _model->loadExternalDiffuseTexture(path);
}
//---------------------------------------------------------------------------
//!< モデルを取得
//---------------------------------------------------------------------------
shr_ptr<Model> ModelRenderer::GetModel() const
{
    return _model;
}

}   // namespace component
