﻿//---------------------------------------------------------------------------
//! @file	Animator.h
//!	@brief	アニメーターはモデルのアニメション処理コンポネント
//---------------------------------------------------------------------------
#pragma once
#include "Component.h"
#include "renderer/animation.h"

namespace component {

class Transform;

class Animator : public Component
{
public:
    Animator();    //!< コンストラクタ
    ~Animator();   //!< デストラクタ

private:
    //---------------------------------------------------------------------------
    //! 継承関数
    //---------------------------------------------------------------------------
    bool OnInit() override;          //!< 初期化
    void OnUpdate() override;        //!< 更新
    void OnRender() override;        //!< 描画
    void OnRenderImgui() override;   //!< ImGui描画
public:
    //---------------------------------------------------------------------------
    //! public 関数
    //---------------------------------------------------------------------------

    //！ アニメションを生成
    void CreateAnimation(const Animation::Desc* desc, u64 count, s32 defaultId = 0);

    void Play(std::string name, Animation::PlayType playType);

    //! アニメションを取得
    shr_ptr<Animation> GetAnimation() const;

	std::string GetAnimationName() const;

	bool IsLastFrame() const;
private:
    //---------------------------------------------------------------------------
    //! private関数
    //---------------------------------------------------------------------------

    //---------------------------------------------------------------------------
    //! private 変数
    //---------------------------------------------------------------------------
    raw_ptr<component::Transform> _pTransform;   //!< Transform コンポーネント 参照

    std::string _currentAnimationName = "";   //!< 現在使ってるアニメション
};
}   // namespace component
