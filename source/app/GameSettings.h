﻿//---------------------------------------------------------------------------
//!	@file	precompile.h
//!	@brief	ゲーム関して設定
//---------------------------------------------------------------------------
#pragma once
namespace settings {
constexpr s32 MAP_H = 19;
constexpr s32 MAP_W = 19;
}   // namespace settings
